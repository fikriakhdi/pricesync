$(document).ready(function () {
  $('.date').datepicker({
    format: 'yyyy-mm-dd',
    startDate: 'dateToday',
    autoclose: true,
  });
  $(".datetimepicker ").timepicki({
    show_meridian: false,
    max_hour_value: 24,
    custom_classes: "myclass"
  });
});

$(document).ready(function () {
  $('.birthdate').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
  });
});

$(document).ready(function() {
  $("#deliveryDate").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
  }).change(function () {
    console.log($(this).val());
    var new_options = {
      format: 'yyyy-mm-dd',
      autoclose: true,
      startDate: $(this).val(),
    }
    $('#receiptDate').datepicker('destroy');
    $('#receiptDate').datepicker(new_options);
    $('#receiptDate').datepicker('setDates', '');
  });
});


// Date range filter
minDateFilter = "";
maxDateFilter = "";

$.fn.dataTableExt.afnFiltering.push(
  function (oSettings, aData, iDataIndex) {
    if (typeof aData._date == 'undefined') {
      aData._date = new Date(aData[0]).getTime();
    }

    if (minDateFilter && !isNaN(minDateFilter)) {
      if (aData._date < minDateFilter) {
        return false;
      }
    }

    if (maxDateFilter && !isNaN(maxDateFilter)) {
      if (aData._date > maxDateFilter) {
        return false;
      }
    }
    return true;
  }
);

$(".delete_btn").click(function (e) {
  e.preventDefault();
  var url = $(this).data("url");
  $("#delete_footer").attr("href", url);
});

$(".edit_product").click(function () {
  var url = $(this).data('url');
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data));
    if (result.status == 200) {
      var product_data = jQuery.parseJSON(JSON.stringify(result.data));
      console.log(product_data)
      var validStartDate = new Date(product_data.validStartDate);
      var month = validStartDate.getMonth() + 1;
      var date = validStartDate.getDate();
      var year = validStartDate.getFullYear();
      if (date < 10) date = "0" + date;
      if (month < 10) month = "0" + month;
      var validStartDate_in = year + "-" + month + "-" + date;
      $("#prod_id").val(product_data._id);
      $("#prod_name").val(product_data.name);
      $("#product_code").val(product_data.productCode);
      $("#description").val(product_data.description);
      $("#price").val(product_data.price);
      $("#productCategory").val(product_data.productCategory);
      // $("#productcategory option:selected").prop("selected", false);
      // for (var i = 0; i < product_data.productcategory.length; i++) {
      //   $("#productcategory option[value='" + product_data.productcategory[i] + "']").prop("selected", true);
      // }
      // $('#productcategory').select2();
      // $("#productgroup").val(product_data.productGroup);
      $("#printer_id").val(product_data.printerId);
      $("#valid_start_date").val(validStartDate_in);
      $("#brandId").val(product_data.brand._id);
    } else {
      console.log(result)
    }
  });
});


$(document).ready(function () {
  $(".datepicker").datepicker({
    format: 'yyyy-mm-dd'
  });
});

$(document).ready(function () {
  $("#brand_select").hide();
});
$("select#brand").change(function () {
  var hoid = $('#brand_select select option:selected').data('hoid');
  console.log(hoid)
  $("#hoID").val(hoid).change()
});
$("select#brand_edit").change(function () {
  var hoid = $('#brand_select_edit select option:selected').data('hoid');
  console.log(hoid)
  $("#hoID").val(hoid).change()
});
$("#role").change(function () {
  var role = $(this).val();
  if (role == 'sm_warehouse' || role == 'sm_supervisor') {
    $("#brand_select").show();
    $("#store_select").show();
    $("#ho_select").hide();
  } else if (role == 'bm') {
    $("#brand_select").show();
    $("#store_select").hide();
    $("#ho_select").hide();
  } else if (role == 'admin') {
    $("#brand_select").hide();
    $("#store_select").hide();
    $("#ho_select").hide();
  } else if (role == 'ho' || role == 'ho_staff') {
    $("#ho_select").show();
    $("#brand_select").hide();
    $("#store_select").hide();
  } else if (role == 'cashier' || role == 'waiter') {
    $("#ho_select").show();
    $("#brand_select").show();
    $("#store_select").show();
  }
});
$("#role_edit").change(function () {
  var role = $(this).val();
  if (role == 'sm_warehouse' || role == 'sm_supervisor') {
    $("#brand_select_edit").show();
    $("#store_select_edit").show();
    $("#ho_select_edit").hide();
  } else if (role == 'bm') {
    $("#brand_select_edit").show();
    $("#store_select_edit").hide();
    $("#ho_select_edit").hide();
  } else if (role == 'admin') {
    $("#brand_select_edit").hide();
    $("#store_select_edit").hide();
    $("#ho_select_edit").hide();
  } else if (role == 'ho') {
    $("#ho_select_edit").show();
    $("#brand_select_edit").hide();
    $("#store_select_edit").hide();
  } else if (role == 'cashier' || role == 'waiter') {
    $("#ho_select").show();
    $("#brand_select").show();
    $("#store_select").show();
  }
});

$("#brand_edit").change(function () {
  var url = $(this).data('url')
  var brand = $(this).val()
  $.getJSON(url + "/" + brand, function (data) {
    var items = "";
    $.each(data.data, function (key, val) {
      var data_in = "";
      items += "<option value='" + val._id + "'>" + val.name + "</option>";
    });
    $("#store_edit").html("<option>Choose Store</option>" + items);
  });
});

$("#brand").change(function () {
  var url = $(this).data('url')
  var brand = $(this).val()
  $.getJSON(url + "/" + brand, function (data) {
    var items = "";
    $.each(data.data, function (key, val) {
      var data_in = "";
      items += "<option value='" + val._id + "'>" + val.name + "</option>";
    });
    $("#store").html("<option>Choose Store</option>" + items);
  });
});

$(".brand_edit").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    $("#code_edit").val(data.data.code);
    $("#name_edit").val(data.data.name);
    $("#hoID").val(data.data.headOffice).change();
    $("#id_edit").val(data.data._id);
  });
});

$(".store_edit").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    $("#id_store").val(data.data._id);
    $("#store_name").val(data.data.name);
    $("#brandId_edit").val(data.data.brand._id).change();
    $("#outerJakarta").val(data.data.outerJakarta).change();
    $("#unitKerja_edit").val(data.data.unitKerja);
  });
});

$(".edit_unit").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    $("#unit_id").val(data.data.unit_id);
    $("#name_edit").val(data.data.name);
  });
});

$(".edit_btn").click(function () {
  var url = $(this).data('url');
  console.log(url)
  $.getJSON(url, function (data) {
    var data_user = data.data;
    var expire_date = new Date(data_user.expire);
    var month = expire_date.getMonth() + 1;
    var date = expire_date.getDate();
    var year = expire_date.getFullYear();
    if (date < 10) date = "0" + date;
    if (month < 10) month = "0" + month;
    var expire = year + "-" + month + "-" + date;
    $("#id_edit").val(data_user._id);
    $("#name_edit").val(data_user.name);
    $("#email_edit").val(data_user.email);
    $("#role_edit").val(data_user.role).change();
    $("#email_edit").val(data_user.email);
    $("#expire_edit").val(expire);
    console.log(data_user.headOffice);
    if (data_user.role == 'sm_warehouse' || data_user.role == 'sm_supervisor') {
      $("#brand_edit").val(data_user.brand._id).change();
      setTimeout(
        function () {
          $("#store_edit").val(data_user.store._id).change();
        }, 1000);
    } else if (data_user.role == 'bm') {
      $("#brand_edit").val(data_user.brand._id).change();

    } else if (data_user.role == 'ho' || data_user.role == 'ho_staff') {
      $("#hoID").val(data_user.headOffice._id).change();
    }
  });
});

$(".edit_customer_btn").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url');
  $.getJSON(url, function (data) {
    var data_edit = data.data;
    var expire_date = new Date(data_edit.custExp);
    var month = expire_date.getMonth() + 1;
    var date = expire_date.getDate();
    var year = expire_date.getFullYear();
    if (date < 10) date = "0" + date;
    if (month < 10) month = "0" + month;
    var expire = year + "-" + month + "-" + date;
    var birthdate = new Date(data_edit.custBirth);
    var month = birthdate.getMonth() + 1;
    var date = birthdate.getDate();
    var year = birthdate.getFullYear();
    if (date < 10) date = "0" + date;
    if (month < 10) month = "0" + month;
    var birth = year + "-" + month + "-" + date;
    $("#id_edit").val(data_edit._id);
    console.log(data_edit._id);
    $("#custId_edit").val(data_edit.custId);
    $("#brand_edit").val(data_edit.brand).change();
    $("#name_edit").val(data_edit.custName);
    $("#email_edit").val(data_edit.custEmail);
    $("#address_edit").val(data_edit.custAddr1);
    $("#phone_edit").val(data_edit.custTelp1);
    $("#discount_edit").val(data_edit.custDisc);
    $("#notes_edit").val(data_edit.custNotes);
    $("#expire_date_edit").val(expire);
    $("#birth_date_edit").val(birth);
    if (data_edit.isMember.toString() == "false")
      $("#type_edit").val("false").change();
    else $("#type_edit").val("true").change();
    $("#brand_edit").val(data_edit.brand).change();
    setTimeout(
      function () {
        $("#store_edit").val(data_edit.store).change();
      }, 1000);
  });
})

// edited by wiky
$(document).ready(function () {
  $('.multi-select-dd').fSelect();
})

// (function($) {
//   $(function() {
//       $('.multi-select-dd').fSelect();
//   });
// })(jQuery);

$('#add_unit input[type=radio][name=unit_type_opt]').change(function () {
  if (this.value == 'lowest') {
    $("#add_unit .lowest_unit_div").show();
    $("#add_unit .highest_unit_div").hide();
  } else if (this.value == 'highest') {
    $("#add_unit .lowest_unit_div").hide();
    $("#add_unit .highest_unit_div").show();
  }
});

$(document).ready(function () {
  $(".paymentStatus").bootstrapToggle({
      on: 'Paid',
      off: 'Unpaid'
    });
    $(".changePaymentStatus").change(function(e){
      e.preventDefault();
      var url = $(this).data('url')
      var status = $(this).val()
      var data = {
        paidStatus : status
      }
      $.post(url, data, function (result) {
        console.log(result)
      })
    })

  $('.datatable').DataTable({
    "order": [],
    "oLanguage": {
      "sSearch": "Filter Data"
    },
    "iDisplayLength": -1,
    "sPaginationType": "full_numbers",
    dom: 'Bfrtip',
    buttons: [{
      extend: 'print',
      exportOptions: {
        columns: '.view'
      }
    }],
    "lengthMenu": [10, 20, 40, 60, 80, 100],
    "pageLength": 10
  });

  var table_selection = $('.datatable-selection').DataTable({
    "order": [],
    "oLanguage": {
      "sSearch": "Filter Data"
    },
    "iDisplayLength": -1,
    "sPaginationType": "full_numbers",
    dom: 'Bfrtip',
    buttons: [{
      extend: 'print',
      exportOptions: {
        columns: '.view'
      }
    }],
    'columnDefs': [{
      'targets': 0,
      'searchable':false,
      'orderable':false,
    }],
    "lengthMenu": [10, 20, 40, 60, 80, 100],
    "pageLength": 10
  });

  // Handle click on "Select all" control
  $('#example-select-all').on('click', function(){
    var rows = table_selection.rows({ 'search': 'applied' }).nodes();
    if(this.checked) $(rows).addClass('selected')
    else $(rows).removeClass('selected')
    $('input[type="checkbox"]', rows).prop('checked', this.checked);
  });

  // Handle click on checkbox to set state of "Select all" control
  $('#example tbody').on('change', 'input[type="checkbox"]', function(){
    if(!this.checked){
      $(this).parents('tr').removeClass('selected')
      var el = $('#example-select-all').get(0);
      if(el && el.checked && ('indeterminate' in el)){
        el.indeterminate = true;
      }
    } else {
      $(this).parents('tr').addClass('selected')
    }
  });

  $("#btn-show-selected").on('click', function(){
    $.fn.dataTable.ext.search.push(
      function (settings, data, dataIndex){         
        return ($(table_selection.row(dataIndex).node()).hasClass('selected')) ? true : false;
      }
    );
    table_selection.draw();    
    $.fn.dataTable.ext.search.pop();
  });

  $("#btn-show-all").on('click', function(){
    table_selection.draw();
  });

  var dttable = $('.datatable.revenue.filter-header').DataTable();
  $(".datatable.revenue.filter-header thead th").each( function ( i ) {	
    var colfield = $(this).text()
    console.log(colfield, colfield == "store" || colfield == "shift")
    if(colfield == "Store" || colfield == "Shift") {
      var select = $('<select style="display:block; margin-bottom:10px"><option value=""></option></select>')
      .prependTo( $(this) )
      .on( 'change', function () {
        var val = $(this).val();
  
        dttable.column( i )
        .search( val ? '^'+$(this).val()+'$' : val, true, false )
        .draw();
      });
  
      dttable.column( i ).data().unique().sort().each( function ( d, j ) {  
        select.append( '<option value="'+d+'">'+d+'</option>' );
      });	
    }
  });

});




$(function () {
  console.log('here2')
  var oTable = $("#create_delivery_order_table").DataTable({
    "order": [],
    "oLanguage": {
      "sSearch": "Filter Data"
    },
    "iDisplayLength": -1,
    "sPaginationType": "full_numbers",
    dom: 'Bfrtip',
    buttons: [
      'print'
    ]
  });

  $(".close_alert").click(function(){
    $("#do_alert").hide();
  })

  // add new item
  $('#btn-add-item-submit').click(function () {
    var dotype = $('#dotype').val();
    var selected = {}
    var valname = $('#list_item_input').val();
    $("#list_item").find("option").each(function () {
      if ($(this).val() == valname) {
        selected.item_id = $(this).data('itemid')
        selected.stock_id = $(this).data('stockid')
        selected.item_code = $(this).data('itemcode')
        selected.item_name = $(this).text()
        selected.unit_id = $(this).data('unitid')
        selected.unit_name = $(this).data('unitname')

        if (dotype == "indirect_store") {
          var stock = parseInt($(this).data('stock'));
          var quantity = $('#pre_create_delivery_order_form #quantity').val();
          if(quantity<=stock){
            selected.quantity = quantity
            //decreasing stock
            var quantity = parseInt(selected.quantity);
            $(this).data('stock', stock-quantity)
          } else {
            //show warning
            $("#do_alert").show();
          }
        } else {
          // selected.quantity_sent = $('#pre_create_delivery_order_form #quantity_sent').val();
          selected.quantity_received = $('#pre_create_delivery_order_form #quantity_received').val();
          selected.weight = $('#pre_create_delivery_order_form #weight').val();
          selected.unit_price = $('#pre_create_delivery_order_form #unit_price').val(); 
        }
      }
    })

    console.log(selected)
    if (dotype == "indirect_store") {
      if(selected.quantity){
      if (selected.item_id != '' && selected.unit_price != '' && selected.quantity_received != '' && selected.quantity != '') {

        $('#ModalPreCreateDeliveryOrder').modal('toggle');
        document.getElementById("pre_create_delivery_order_form").reset();

        oTable.row.add([
          '<div class="td_hidden">' + selected.stock_id + '</div>',
          selected.item_code,
          '<div class="td_hidden">' + selected.item_id + '</div>',
          selected.item_name,
          selected.quantity,
          '<div class="td_hidden">' + selected.unit_id + '</div>',
          selected.unit_name,
          '<button class="btn btn-danger btn-block delete_btn" type="button" data-quantity="'+ selected.quantity +'" data-itemcode="'+selected.item_code+'"><span class="fa fa-trash"></span></button>'
        
        ]).draw()

        // update table data input form
        var tabledt = $('#create_delivery_order_table').tableToJSON({
          ignoreColumns: [1, 3, 6, 7]
        })

        // hide id column from table
        $("td > div.td_hidden").parent().addClass('hidden');

        console.log(tabledt)
        $("#create_delivery_order_form #items").val(JSON.stringify(tabledt))

      } else {
        alert("Please fill in all required field!")
      }
    } else {
      //show warning
      $("#do_alert").show();
    }

    } else {
      // if (selected.item_id != '' && selected.unit_price != '' && selected.quantity_received != '' && (selected.quantity_sent != '' || selected.weight != '')) {
        if (selected.item_id != '' && selected.unit_price != '' && selected.quantity_received != '') {        

        $('#ModalPreCreateDeliveryOrder').modal('toggle');
        document.getElementById("pre_create_delivery_order_form").reset();

        oTable.row.add([
          '<div class="td_hidden">' + selected.stock_id + '</div>',
          selected.item_code,
          '<div class="td_hidden">' + selected.item_id + '</div>',
          selected.item_name,
          // (selected.quantity_sent ? selected.quantity_sent : '-'),
          selected.quantity_received,
          (selected.weight ? selected.weight : '-'),
          (selected.unit_price ? selected.unit_price : '-'),
          '<div class="td_hidden">' + selected.unit_id + '</div>',
          selected.unit_name,
          '<button class="btn btn-danger btn-block delete_btn" type="button" data-itemcode="'+selected.item_code+'"><span class="fa fa-trash"></span></button>'
        ]).draw()

        // update table data input form
        var tabledt = $('#create_delivery_order_table').tableToJSON({
          ignoreColumns: [1, 3, 9, 10]
        })
        // hide id column from table
        $("td > div.td_hidden").parent().addClass('hidden');

        console.log(tabledt)
        $("#create_delivery_order_form #items").val(JSON.stringify(tabledt))

      } else {
        alert("Please fill in all required field!")
      }
    }
  })

  // delete add item table row
  $('#create_delivery_order_table tbody').on('click', '.delete_btn', function () {
    oTable
      .row($(this).parents('tr'))
      .remove()
      .draw();

    // update table data input form
    var tabledt = $('#create_delivery_order_table').tableToJSON({
      ignoreColumns: [0, 2, 5, 6]
    });
    $("#create_delivery_order_form #items").val(JSON.stringify(tabledt))
    //increase quantity
    var itemid = $(this).data('itemcode');
    var input_stock = $(this).data('quantity');
    $("#list_item").find("option").each(function () {
      if($(this).data('itemcode')==itemid){
        var stock = $(this).data('stock')
        $(this).data('stock', input_stock+stock);
      }
    });
  });
});

$(document).ready(function () {
  $('#do_table').DataTable({
    "order": [
      [6, "desc"]
    ]
  });
  $("#do_alert").hide();
});

$('#list_item_input').on('input', function () {
  var valname = $(this).val();
  $("#list_item").find("option").each(function () {
    if ($(this).val() == valname) {
      var unitname = $(this).data('unitname')
      var stock = $(this).data('stock')
      var itemid = $(this).data('id')
      console.log(itemid)
      $(".item_related #selected_item").val(itemid)
      $(".item_related #unit").val(unitname)
      $(".item_related #stock").val(stock + " " + unitname)
    }
  })
})

$("#selected_item").change(function () {
  var unitname = $(this).data('unitname')
  var stock = $(this).data('stock')
  $(".item_related #unit").val(unitname)
  $(".item_related #stock").val(stock + " " + unitname)
})

$("#brand_edit").change(function () {
  var url = $(this).data('url')
  var brand = $(this).val()
  $.getJSON(url + "/" + brand, function (data) {
    var items = "";
    $.each(data.data, function (key, val) {
      var data_in = "";
      items += "<option value='" + val._id + "'>" + val.name + "</option>";
    });
    $("#store_edit").html("<option>Choose Store</option>" + items);
  });
});

$("#create_item_category_form #category_id").change(function () {
  var prefix = $('#create_item_category_form #category_id option:selected').data('prefix');
  $('#create_item_category_form #item_code_prefix').val(prefix);
})

$("#storeId .opt").hide()
$("#brandId").change(function () {
  var val = $(this).val();
  $("#storeId .opt").hide()
  $("#storeId ." + val).show()
})

$("#create_stock_form .opt").hide()
$("#create_stock_form #selected_item").change(function () {
  $("#create_stock_form #unit_id").prop('selectedIndex', 0)
  $("#create_stock_form #quantity").val('')
  var unitid = $('#create_stock_form #selected_item option:selected').data('unitid');
  $("#create_stock_form .opt").hide()
  $("#create_stock_form ." + unitid).show()
})

$("#create_stock_form #unit_id").change(function () {
  var conversion = $('#create_stock_form #unit_id option:selected').data('conversion');
  $("#create_stock_form #cnv").val(conversion)
})

$(".edit_stock").click(function () {
  var url = $(this).data('url');
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data));
    if (result.status == 200) {
      var stock_data = jQuery.parseJSON(JSON.stringify(result.data));
      console.log(stock_data)
      $("#stock_id").val(stock_data._id);
      $("#edit_initial_stock").val(stock_data.initialStock);
      $("#edit_notes").val(stock_data.notes);
    } else {
      console.log(result)
    }
  });
});
$(".edit_table").click(function () {
  var url = $(this).data('url');
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data));
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data));
      $("#id").val(data._id);
      $("#table_number").val(data.number);
      $("#store").val(data.store);
      $("#brand").val(data.brand);
      $("#type").val(data.type).change();
      $("#status").val(data.status).change();
    } else {
      console.log(result)
    }
  });
});
$(".edit_payment").click(function () {
  var url = $(this).data('url');
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data));
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data));
      $("#id").val(data._id);
      $("#name").val(data.name);
      $("#brand").val(data.brand);
      $("#type").val(data.type);
    } else {
      console.log(result)
    }
  });
});
$(".edit_product_category").click(function () {
  var url = $(this).data('url');
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data));
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data));
      $("#id").val(data._id);
      $("#code").val(data.code);
      $("#name").val(data.name);
      $("#brand").val(data.brand);
      $("#productGroup").val(data.productGroup);
    } else {
      console.log(result)
    }
  });
});
$(".edit_discount").click(function () {
  var url = $(this).data('url');
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data));
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data));
      $("#id").val(data._id);
      $("#name").val(data.name);
      $("#brand").val(data.brand);
      $("#product").val(data.product);
      $("#productcategory").val(data.productcategory);
      $("#productgroup").val(data.productgroup);
      $("#payment").val(data.payment);
      $("#valueType").val(data.valueType);
      $("#value").val(data.value);
      $("#maxValue").val(data.maxValue);
    } else {
      console.log(result)
    }
  });
});
$(".edit_product_group").click(function () {
  var url = $(this).data('url');
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data));
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data));
      $("#id").val(data._id);
      $("#code").val(data.code);
      $("#name").val(data.name);
      $("#brand").val(data.brand);
    } else {
      console.log(result)
    }
  });
});


$('#printDO').click(function () {
  window.print();
});


$('.pop').on('click', function () {
  $('.imagepreview').attr('src', $(this).find('img').attr('src'));
  $('#imagemodal').modal('show');
});


$('.adjust-doi').click(function () {
  var doitemid = $(this).data('doitemid')
  $("#adjust-doi-" + doitemid).hide()
  $("#adjust-type-" + doitemid).show()
  $("#adjust-input-" + doitemid).show()
  $("#adjust-btn-" + doitemid).show()
})

$('.adjust-type').change(function () {
  var doitemid = $(this).data('doitemid')
  var quantity = $("#adjust-input-" + doitemid).val()
  var typeselected = $("#adjust-type-" + doitemid + " option").filter(':selected').val();
  if (quantity != '' && (typeselected != '' && typeselected != 'Type')) {
    $("#adjust-btn-" + doitemid).removeAttr("disabled");
  } else {
    $("#adjust-btn-" + doitemid).attr("disabled", true);
  }
})

$('.adjust-input').change(function () {
  var doitemid = $(this).data('doitemid')
  var quantity = $("#adjust-input-" + doitemid).val()
  var typeselected = $("#adjust-type-" + doitemid + " option").filter(':selected').val();
  if (quantity != '' && (typeselected != '' && typeselected != 'Type')) {
    $("#adjust-btn-" + doitemid).removeAttr("disabled");
  } else {
    $("#adjust-btn-" + doitemid).attr("disabled", true);
  }
})

$('.return-input').change(function () {
  var doitemid = $(this).data('doitemid')
  var quantity = $("#return-input-" + doitemid).val()
  var typeselected = $("#return-notes-" + doitemid).val()
  if (quantity != '' && (typeselected != '' && typeselected != '')) {
    $("#return-btn-" + doitemid).removeAttr("disabled");
  } else {
    $("#return-btn-" + doitemid).attr("disabled", true);
  }
})
$('.return-notes').change(function () {
  var doitemid = $(this).data('doitemid')
  var quantity = $("#return-input-" + doitemid).val()
  var typeselected = $("#return-notes-" + doitemid).val()
  if (quantity != '' && (typeselected != '' && typeselected != '')) {
    $("#return-btn-" + doitemid).removeAttr("disabled");
  } else {
    $("#return-btn-" + doitemid).attr("disabled", true);
  }
})

$('.adjust-btn').click(function () {
  var url = $(this).data('url')
  var itemid = $(this).data('itemid')
  var doid = $(this).data('doid')
  var doitemid = $(this).data('doitemid')
  var stockid = $(this).data('stockid')
  var quantity = $("#adjust-input-" + doitemid).val()
  var type = $("#adjust-type-" + doitemid).val()

  $("#adjust-loader-" + doitemid).show()
  $("#adjust-div-" + doitemid).hide()

  var data = {
    doid: doid,
    stockid: stockid,
    doitemid: doitemid,
    itemid: itemid,
    quantity: quantity,
    type: type
  }

  $.post(url, data, function (result) {
    result = JSON.parse(result)
    if (result.status == 400) {
      alert("Adjustment Process Failed")
      $("#adjust-doi-" + doitemid).show()
      $("#adjust-div-" + doitemid).show()
      $("#adjust-loader-" + doitemid).hide()
      $("#adjust-type-" + doitemid).hide()
      $("#adjust-input-" + doitemid).hide()
      $("#adjust-btn-" + doitemid).hide()
    } else {
      $("#adjust-td-" + doitemid).html(result.data.adjustmentType + " " + result.data.adjustmentVal)
      $("#adjust-doi-" + doitemid).show()
      $("#adjust-div-" + doitemid).show()
      // $("#adjust-div-" + doitemid).html("-")
      $("#adjust-loader-" + doitemid).hide()
      $("#adjust-type-" + doitemid).hide()
      $("#adjust-input-" + doitemid).hide()
      $("#adjust-btn-" + doitemid).hide()
    }
  });
})

$('.return-btn').click(function (e) {
  e.preventDefault()
  var url = $(this).data('url')
  var itemid = $(this).data('itemid')
  var stockid = $(this).data('stockid')
  var doitemid = $(this).data('doitemid')
  var notes = $("#return-notes-" + doitemid).val()
  var quantity = $("#return-input-" + doitemid).val()

  $("#return-loader-" + doitemid).show()
  $("#return-div-" + doitemid).hide()

  var data = {
    itemid: itemid,
    doitemid: doitemid,
    stockid: stockid,
    quantity: quantity,
    notes: notes
  }

  $.post(url, data, function (result) {
    result = JSON.parse(result)
    if (result.status == 400) {
      alert(result.message)
      $("#return-doi-" + doitemid).show()
      $("#return-div-" + doitemid).show()
      $("#return-loader-" + doitemid).hide()
      $("#return-notes-" + doitemid).hide()
      $("#return-input-" + doitemid).hide()
      $("#return-btn-" + doitemid).hide()
    } else {
      $("#return-td-" + doitemid).html(result.data.returnVal + "(" + result.data.returnNotes + ")")
      $("#return-doi-" + doitemid).hide()
      $("#return-div-" + doitemid).hide()
      $("#return-div-" + doitemid).html("-")
      $("#return-loader-" + doitemid).hide()
      $("#return-notes-" + doitemid).hide()
      $("#return-input-" + doitemid).hide()
      $("#return-btn-" + doitemid).hide()
    }
  });
})

$("#store_role").change(function () {
  var val = $(this).val();
  if (val == 'cashier') {
    $("#diff_head").removeClass('hidden');
    $(".diff_body").each(function (index) {
      $(this).removeClass('hidden');
    });
  } else {
    $("#diff_head").addClass('hidden');
    $(".diff_body").each(function (index) {
      $(this).addClass('hidden');
    });
  }
});

$(".add-qty").keyup(function (e) {
  e.preventDefault();
  var qty_id = $(this).data('qty');
  var val = $(this).val();
  $(qty_id).html(val);
  // update table data input form
  var tabledt = $('#receive_item_table').tableToJSON({
    ignoreColumns: [0, 4, 5, 6, 9]
  });
  $("#updateItems").val(JSON.stringify(tabledt));
  console.log(JSON.stringify(tabledt));
});


$(".edit_item").click(function () {
  var url = $(this).data('url');
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data));
    if (result.status == 200) {
      var unit_data = jQuery.parseJSON(JSON.stringify(result.data));
      console.log(unit_data)
      $("#ModalUpdateItem #item_id").val(unit_data.item_id);
      $("#ModalUpdateItem #name").val(unit_data.name);
    } else {
      console.log(result)
    }
  });
});

$(".edit_item_category").click(function () {
  var url = $(this).data('url');
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data));
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data));
      console.log(data)
      $("#ModalUpdateCategory #category_id").val(data._id);
      $("#ModalUpdateCategory #name").val(data.name);
      $("#ModalUpdateCategory #prefix").val(data.prefix);
    } else {
      console.log(result)
    }
  });
});

$(".edit_supplier").click(function () {
  var url = $(this).data('url');
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data));
    if (result.status == 200) {
      var supplier_data = jQuery.parseJSON(JSON.stringify(result.data));
      console.log(supplier_data)
      $("#ModalUpdateSupplier #supplier_id").val(supplier_data.supplier_id);
      $("#ModalUpdateSupplier #name").val(supplier_data.name);
    } else {
      console.log(result)
    }
  });
});

var table = $('#edit_delivery_order_table').DataTable({
  "order": [],
  "oLanguage": {
    "sSearch": "Filter Data"
  },
  "iDisplayLength": -1,
  "sPaginationType": "full_numbers",
  dom: 'Bfrtip',
  "pageLength": 10
})
$('#edit_delivery_order_table tbody').on('click', '.delete_btn', function () {
  table
    .row($(this).parents('tr'))
    .remove()
    .draw();
});

$(".edit_ho").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url');
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data));
    if (result.status == 200) {
      var unit_data = jQuery.parseJSON(JSON.stringify(result.data));
      console.log(unit_data);
      $("#id_ho").val(unit_data._id);
      $("#ho_name").val(unit_data.name);
    } else {
      console.log(result)
    }
  });
});

$('#history_stock_table').DataTable({
  "order": [],
  "oLanguage": {
    "sSearch": "Filter Data"
  },
  "iDisplayLength": -1,
  "sPaginationType": "full_numbers",
  dom: 'Bfrtip',
  "pageLength": 10,
  "order": [
    [6, "desc"]
  ]
})

$(".return-doi").click(function (e) {
  e.preventDefault();
  var doitemid = $(this).data('doitemid');
  $(this).hide();
  $("#return-notes-"+doitemid).show();
  $("#return-input-"+doitemid).show();
  $("#return-btn-"+doitemid).show();
});

$(".spoil-btn").click(function(e){
  e.preventDefault();
  var data_id = $(this).data('id');
  var data_item_name = $(this).data('item-name');
  $("#stock_id_spoil").val(data_id);
  $("#item_name_spoil").val(data_item_name);
})

$('#supplier_input').change(function () {
  var supplier_id = $(this).val()
  var date_from = $('#datepicker_from').val()
  var date_to = $('#datepicker_to').val()
  if(supplier_id){
    date_from = date_from ? date_from.split('/').join('-') : '-' 
    date_to = date_to ? date_to.split('/').join('-') : '-'
    var url = '/ho/restock_report/supplier/'+supplier_id+'/'+date_from+"/"+date_to
    window.location.href = url;
  }
})

$('.selectize').selectize({
  // sortField: {
  //     field: 'text',
  //     direction: 'asc'
  // },
  // dropdownParent: 'body'
});


$(function () {
  var oTable = $("#create_product_update_table").DataTable({
    "order": [],
    "oLanguage": {
      "sSearch": "Filter Data"
    },
    "iDisplayLength": -1,
    "sPaginationType": "full_numbers",
    dom: 'Bfrtip',
    buttons: [
      'print'
    ]
  });

  // add new product update
  $('#btn-add-product-submit').click(function () {
    var selected = {}
    selected.productId = $('#product option:selected').val()
    selected.productName = $('#product option:selected').text()
    selected.productPrice = $('#product option:selected').data('price')
    selected.newPrice = $('#pre_create_product_update_form #newprice').val();
    selected.newName = $('#pre_create_product_update_form #newname').val();
    selected.newDescription = $('#pre_create_product_update_form #newdescription').val();

    console.log(selected)
    if (selected.productId && selected.productName && selected.newPrice) {

      $('#ModalPreCreateProductUpdate').modal('toggle');
      document.getElementById("pre_create_product_update_form").reset();

      oTable.row.add([
        '<div class="td_hidden">' + selected.productId + '</div>',
        selected.productName,
        selected.productPrice,
        selected.newPrice,
        selected.newName ? selected.newName : '-',
        selected.newDescription ? selected.newDescription : '-' ,
        '<button class="btn btn-danger btn-block delete_btn" type="button" data-productId="'+selected.productId+'"><span class="fa fa-trash"></span></button>'
      ]).draw()

      // update table data input form
      var tabledt = $('#create_product_update_table').tableToJSON({
        ignoreColumns: [6]
      })

      // hide id column from table
      $("td > div.td_hidden").parent().addClass('hidden');

      console.log(tabledt)
      $("#create_product_update_form #product_updates").val(JSON.stringify(tabledt))

    } else {
      alert("Please fill in all required field!")
    }
  })

  // delete add item table row
  $('#create_product_update_table tbody').on('click', '.delete_btn', function () {
    oTable
      .row($(this).parents('tr'))
      .remove()
      .draw();

    // update table data input form
    var tabledt = $('#create_product_update_table').tableToJSON({
      ignoreColumns: [6]
    });
    $("#create_product_update_form #product_updates").val(JSON.stringify(tabledt))
  });
});


$(function () {
  var pTable = $("#product_update_detail_table table").DataTable({
    "order": [],
    "oLanguage": {
      "sSearch": "Filter Data"
    },
    "iDisplayLength": -1,
    "sPaginationType": "full_numbers",
    dom: 'Bfrtip',
    buttons: [
      'print'
    ]
  });

  $(".view_product_update").click(function (e) {
    e.preventDefault();
    var url = $(this).data('url');
    $.getJSON(url, function (data) {
      var result = jQuery.parseJSON(JSON.stringify(data));
      if (result.status == 200) {
        console.log(result)
        pTable
          .clear()
          .draw()

        result.data.productChanges.map(pc=>{
          pTable.row.add([
            pc.product.name,
            pc.product.price,
            pc.price,
            pc.name ? pc.name : '-',
            pc.description ? pc.description : '-'
          ]).draw()   
        })

        var validDate = new Date(result.data.validDate);
        var month = validDate.getMonth() + 1;
        var date = validDate.getDate();
        var year = validDate.getFullYear();
        if (date < 10) date = "0" + date;
        if (month < 10) month = "0" + month;
        validDate = year + "-" + month + "-" + date;
        $('#valid_date_field').html('<b>Valid Date:</b> '+validDate)
        $('#brand_field').html('<b>Brand:</b> '+result.data.brand.name)
      } else {
        console.log(result)
      }
    });
  });
});


$(function () {
  var oTable = $("#edit_product_update_table").DataTable({
    "order": [],
    "oLanguage": {
      "sSearch": "Filter Data"
    },
    "iDisplayLength": -1,
    "sPaginationType": "full_numbers",
    dom: 'Bfrtip',
    buttons: [
      'print'
    ]
  });

  // add new product update
  $('#btn-add-editproduct-submit').click(function () {
    var selected = {}
    selected.productId = $('#product option:selected').val()
    selected.productName = $('#product option:selected').text()
    selected.productPrice = $('#product option:selected').data('price')
    selected.newPrice = $('#pre_edit_product_update_form #newprice').val();
    selected.newName = $('#pre_edit_product_update_form #newname').val();
    selected.newDescription = $('#pre_edit_product_update_form #newdescription').val();

    console.log(selected)
    if (selected.productId && selected.productName != '' && selected.newPrice) {

      $('#ModalPreEditProductUpdate').modal('toggle');
      document.getElementById("pre_edit_product_update_form").reset();

      oTable.row.add([
        '<div class="td_hidden">' + selected.productId + '</div>',
        selected.productName,
        selected.productPrice,
        selected.newPrice,
        selected.newName ? selected.newName : '-',
        selected.newDescription ? selected.newDescription : '-' ,
        '<button class="btn btn-danger btn-block delete_btn" type="button" data-productId="'+selected.productId+'"><span class="fa fa-trash"></span></button>'
      ]).draw()

      // update table data input form
      var tabledt = $('#edit_product_update_table').tableToJSON({
        ignoreColumns: [6]
      })

      // hide id column from table
      $("td > div.td_hidden").parent().addClass('hidden');

      console.log(tabledt)
      $("#edit_product_update_form #product_updates").val(JSON.stringify(tabledt))

    } else {
      alert("Please fill in all required field!")
    }
  })

  // delete add item table row
  $('#edit_product_update_table tbody').on('click', '.delete_btn', function () {
    oTable
      .row($(this).parents('tr'))
      .remove()
      .draw();

    // update table data input form
    var tabledt = $('#edit_product_update_table').tableToJSON({
      ignoreColumns: [6]
    });
    $("#edit_product_update_form #product_updates").val(JSON.stringify(tabledt))
  });
});