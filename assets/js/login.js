$(".login-btn").click(function(e){
    e.preventDefault()
    var form_id           = $(this).data("id")
    var form              = $("#"+form_id)
    console.log(form_id)
    var submit_url        = form.attr('action')
    var refresh_url       = form.attr('refresh')
    var datastring        = form.serialize()
    var verifyURL         = form.attr('verifyURL')
    var email             = $("#email").val()
    $.ajax({
      type: "POST",
      url: submit_url,
      data: datastring,
      success: function(response) {
        var result =jQuery.parseJSON( JSON.stringify(response ))
        if(result.status=="200"){
            swal({
                 title: "Information",
                 text: "Login Successfull",
                 icon: "success",
                 });
          window.location = refresh_url
        }
        else if(result.status=="300"){
            swal({
                text: 'No Token Found, Please enter your token',
                content: "input",
                icon: "error",
                button: {
                  text: "Submit",
                  closeModal: false,
                },
              })
              .then(name => {
                if (!name) throw null;
                console.log(name, verifyURL)
                $.ajax({
                  type: "POST",
                  url: verifyURL,
                  data: { 'token': name, email:email},
                  cache: false,
                  success: function(response) {
                    var result = JSON.parse(response)
                    if(result.status=="200"){
                      swal(
                      "Success!",
                      "Your token has been submitted!",
                      "success"
                      )
                    } else {
                      swal(
                        "Failed!",
                        result.message,
                        "error"
                        )
                    }
                  },
                  failure: function (response) {
                      swal(
                      "Internal Error",
                      "Oops, failed to submit your token.", // had a missing comma
                      "error"
                      )
                  }
              });
              })
              .catch(err => {
                if (err) {
                  swal("Oh noes!", "The AJAX request failed!", "error");
                } else {
                  swal.stopLoading();
                  swal.close();
                }
              });
        } else {
          swal(
            "Information!",
            result.message,
            "error"
            )
        }
      },
      dataType: "json"
    })
  })

  
$(document).ajaxStart(function(){
  $("#wait").show();
});

$(document).ajaxComplete(function(){
  $("#wait").hide();
});
$(document).ready(function () {
  $("#wait").hide();
})