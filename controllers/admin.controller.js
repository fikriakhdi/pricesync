const User = require('../models/user.model')
const Brand = require('../models/brand.model')
const Store = require('../models/store.model')
const News = require('../models/news.model')
const Product = require('../models/product.model')
const Revenue = require('../models/revenue.model')
const Session = require('../models/session.model')
const HO = require('../models/head_office.model')
const multer = require("multer")
var fs = require('fs')
var formidable = require('formidable')
var mv = require('mv')
var dateFormat = require('dateformat')
var async = require('async')
var crypto = require('crypto')
var request = require('request')
var hbs = require('hbs')
const moment = require('moment')

const upload = multer({
  dest: "/assets/img/image_profile"
})


//Dashboard
exports.dashboard = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var user_list = []
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load User Data
    function (callback) {
      User.find({}).sort({
        createdDate: -1
      }).populate("headOffice brand store createdBy updatedBy").exec(function (err, user) {
        if (err) return callback(err)
        var total_expired_user = 0
        var total_active_user = 0
        locals.user_list = user
        user.forEach(function (item) {
          var date_expire = (new Date(item.expire))
          var date_now = (new Date())
          if (date_expire < date_now) total_expired_user += 1
          if (date_expire >= date_now) total_active_user += 1
          locals.total_expired_user = total_expired_user
          locals.total_active_user = total_active_user
        })
        callback()
      })
    },
    function (callback) {
      Brand.find({}, function (err, result) {
        if (err) return callback(err)
        locals.brand_list = result
        var count = 0
        result.forEach(function (item) {
          count++
        })
        locals.total_brand = count
        locals.brand_list = result
        // console.log(locals.brand_list)
        callback()
      })
    },
    //Load total Store Data
    function (callback) {
      Store.count({}, function (err, count) {
        if (err) return callback(err)
        locals.total_stores = count
        callback()
      })
    },
    //Load Data HO
    function (callback) {
      HO.find({}, function (err, result) {
        if (err) return callback(err)
        locals.HO = result
        callback()
      })
    }

  ], function (err) { //This function gets called after the two tasks have called their "task callbacks"
    if (err) return next(err) //If an error occurred, we let express handle it by calling the `next` function
    res.render('main', {
      title: appName + " | " + "Dashboard",
      user_data: user_data,
      user_list: locals.user_list,
      brand_list: locals.brand_list,
      HO: locals.HO,
      total_stores: locals.total_stores,
      total_brand: locals.total_brand,
      total_expired_user: locals.total_expired_user,
      total_active_user: locals.total_active_user,
      messages: messages,
      messages_type: messages_type
    })
    // console.log(locals.brand_list)
  })
}


//function auth redirect
function auth_redirect(req, res) {
  sess = req.session
  if (sess.user_login) return true
  else res.redirect('/signin')
}


// POST admin change_password
exports.change_password = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.password != '' && req.body.conf_password != '' && req.body.password == req.body.conf_password) {
        var userData = {
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          updatedDate: Date.now(),
          upeatedBy: user._id,
        }
        User.findByIdAndUpdate({
          _id: user._id
        }, userData, function (error, user) {
          if (error || user == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Update profile success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

//product
exports.product = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var user_list = []
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load Products Data
    function (callback) {
      Product.find({}).populate('brand createdBy updatedBy').sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.products = result
        callback()
      })
    },
    //Load Products Data
    function (callback) {
      Brand.find({}, function (err, result) {
        if (err) return callback(err)
        locals.brands = result
        callback()
      })
    }
  ], function (err) { //This function gets called after the two tasks have called their "task callbacks"
    if (err) return next(err) //If an error occurred, we let express handle it by calling the `next` function
    res.render('product', {
      title: appName + " | " + "Product",
      user_data: user_data,
      products: locals.products,
      brands: locals.brands,
      messages: messages,
      messages_type: messages_type
    })
  })
}


//profile
exports.profile = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load Brand Data
    function (callback) {
      Brand.find({
        _id: user_data.brandId
      }, function (err, brand) {
        if (err) return callback(err)
        locals.brand = brand[0]
        callback()
      })
    },
    //Load Store Data
    function (callback) {
      Store.find({
        _id: user_data.storeId
      }, function (err, store) {
        if (err) return callback(err)
        locals.store = store[0]
        callback()
      })
    }
  ], function (err) { //This function gets called after the two tasks have called their "task callbacks"
    if (err) return next(err) //If an error occurred, we let express handle it by calling the `next` function
    //Here `locals` will be an object with `user` and `posts` keys
    //Example: `locals = {user: ..., posts: [...]}`
    res.render('profile', {
      title: appName + " | " + "Profile",
      user_data: user_data,
      brand: locals.brand,
      store: locals.store,
      messages: messages,
      messages_type: messages_type
    })
  })
}

// Product Report
exports.revenue_report = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Get Revenue This Week
    function (callback) {
      var start = moment().format("YYYY-MM-01")
      var end = moment().format("YYYY-MM-") + moment().daysInMonth()
      Revenue.find({
        createdDate: {
          $gte: new Date((new Date()) - 7 * 60 * 60 * 24 * 1000)
        }
      }, null).populate('store').sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.this_week = result
        callback()
      })
    },
    //Get Product This Month
    function (callback) {
      Revenue.find({}).populate('store').exec(function (err, result) {
        if (err) return callback(err)
        locals.all_time = result
        callback()
      })
    }
  ], function (err) { //This function gets called after the two tasks have called their "task callbacks"
    if (err) return next(err) //If an error occurred, we let express handle it by calling the `next` function
    //Here `locals` will be an object with `user` and `posts` keys
    //Example: `locals = {user: ..., posts: [...]}`
    res.render('revenue_report', {
      title: appName + " | " + "Revenue Report",
      user_data: user_data,
      this_week: locals.this_week,
      all_time: locals.all_time,
      messages: messages,
      messages_type: messages_type
    })
  })
}

//change password
exports.change_password_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.password != '' && req.body.conf_password != '' && req.body.password == req.body.conf_password) {
    var userData = {
      password: crypto.createHash('md5').update(req.body.password).digest('hex'),
      updatedDate: Date.now(),
      upeatedBy: user_data._id,
    }
    User.findByIdAndUpdate({
      _id: user_data._id
    }, userData, function (error, user) {
      if (error || user == null) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Change password success')
        req.flash('info_type', 'success')
        res.redirect('/admin/profile')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/profile')
  }
}

// GET admin delete user
exports.delete_user_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var redirect = ""
  redirect = req.params.redirect
  if (!redirect) redirect = ""
  if (req.params.userId) {
    if (user_data._id != req.params.userId) {
      User.findByIdAndRemove(req.params.userId, function (error, revenue) {
        if (error) {
          var err = {
            status: 400,
            message: error
          }
          return res.send(JSON.stringify(err))
        }
        if (revenue == null) {
          req.flash('info', 'User not found')
          req.flash('info_type', 'danger')
          res.redirect('/admin/' + redirect)
        } else {
          req.flash('info', 'Delete User success')
          req.flash('info_type', 'success')
          res.redirect('/admin/' + redirect)
        }
      })
    } else {
      req.flash('info', 'You can\'t delete your own account')
      req.flash('info_type', 'danger')
      res.redirect('/admin')
    }
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/admin')
  }
}


// POST admin update_profile
exports.update_profile = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.name != '' && req.body.email != '') {
        var userData = {
          name: req.body.name,
          email: req.body.email,
          imageProfile: "none",
          updatedDate: Date.now(),
          upeatedBy: user._id,
        }
        User.findOneAndUpdate({
          _id: user._id
        }, userData, function (error, user) {
          if (error || user == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Update profile success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}


//Dashboard
exports.user = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var user_list = []
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load User Data
    function (callback) {
      User.find({}).populate("headOffice brand store createdBy updatedBy").sort({
        createdDate: -1
      }).exec(function (err, user) {
        if (err) return callback(err)
        var total_expired_user = 0
        var total_active_user = 0
        locals.user_list = user
        callback()
      })
    },
    //Load Brand Data
    function (callback) {
      Brand.find({}, function (err, result) {
        if (err) return callback(err)
        locals.brands = result
        callback()
      })
    },
    //Load Data HO
    function (callback) {
      HO.find({}, function (err, result) {
        if (err) return callback(err)
        locals.HO = result
        callback()
      })
    }

  ], function (err) { //This function gets called after the two tasks have called their "task callbacks"
    if (err) return next(err) //If an error occurred, we let express handle it by calling the `next` function
    res.render('user', {
      title: appName + " | " + "User",
      user_data: user_data,
      brands: locals.brands,
      user_list: locals.user_list,
      HO: locals.HO,
      total_expired_user: locals.total_expired_user,
      total_active_user: locals.total_active_user,
      messages: messages,
      messages_type: messages_type
    })
    // console.log(locals.brand_list)
  })
}

// POST admin create user
exports.create_user = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.name != '' && req.body.email != '' && req.body.password != '' &&
        req.body.role != '') {
        var userData = {
          name: req.body.name,
          email: req.body.email,
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          role: req.body.role,
          expire: req.body.expire,
          brand: req.body.brandId,
          store: req.body.storeId,
          headOffice: req.body.hoID,
          imageProfile: "none",
          createdDate: Date.now(),
          createdBy: user._id,
        }
        User.create(userData, function (error, user) {
          if (error || user == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Create user success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}



//Post Create User
exports.create_user_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var redirect = req.params.redirect
  if (!redirect) redirect = ""
  if (req.body.name != '' && req.body.email != '' && req.body.password != '' && req.body.confirm_password &&
    req.body.role != '' && req.body.expire != '') {
    if (req.body.password == req.body.confirm_password) {
      var file_url = ""
      //Upload Profile Picture
      upload.single("file" /* name attribute of <file> element in your form */ ),
        (req, res) => {
          const tempPath = req.file.path
          const random_name = Math.random().toString(36).substring(7)
          const extension = path.extname(req.file.originalname).toLowerCase()
          const targetPath = path.join(__dirname, "./assets/img/" + random_name + "." + extension)
          fs.rename(tempPath, targetPath, err => {
            if (err) return handleError(err, res)
            file_url = targetPath
          })
        }
      //If Role
      var file_url = req.file ? ('/image/' + res.req.file.filename) : ''
      if (req.body.role == 'sm_warehouse' || req.body.role == 'sm_supervisor' || req.body.role=='ho_staff') {
        var userData = {
          name: req.body.name,
          email: req.body.email.toLowerCase(),
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          role: req.body.role,
          expire: req.body.expire,
          brand: req.body.brand,
          store: req.body.store,
          headOffice: req.body.hoID,
          imageProfile: file_url,
          createdDate: Date.now(),
          createdBy: user_data._id,
        }
      } else if (req.body.role == 'bm') {
        var userData = {
          name: req.body.name,
          email: req.body.email.toLowerCase(),
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          role: req.body.role,
          expire: req.body.expire,
          headOffice: req.body.hoID,
          brand: req.body.brand,
          store: null,
          imageProfile: file_url,
          createdDate: Date.now(),
          createdBy: user_data._id,
        }
      } else if (req.body.role == 'admin') {
        var userData = {
          name: req.body.name,
          email: req.body.email.toLowerCase(),
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          role: req.body.role,
          expire: req.body.expire,
          headOffice: req.body.hoID,
          brand: null,
          store: null,
          imageProfile: file_url,
          createdDate: Date.now(),
          createdBy: user_data._id,
        }
      } else if (req.body.role == 'ho') {
        var userData = {
          name: req.body.name,
          email: req.body.email.toLowerCase(),
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          role: req.body.role,
          expire: req.body.expire,
          brand: null,
          store: null,
          headOffice: req.body.hoID,
          imageProfile: file_url,
          createdDate: Date.now(),
          createdBy: user_data._id,
        }
      } else if (req.body.role == 'ho_staff') {
        var userData = {
          name: req.body.name,
          email: req.body.email.toLowerCase(),
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          role: req.body.role,
          expire: req.body.expire,
          brand: null,
          store: null,
          headOffice: req.body.hoID,
          imageProfile: file_url,
          createdDate: Date.now(),
          createdBy: user_data._id,
        }
      } else if (req.body.role == "cashier" || req.body.role == "waiter") {
        var userData = {
          name: req.body.name,
          email: req.body.email.toLowerCase(),
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          role: req.body.role,
          expire: req.body.expire,
          brand: req.body.brand,
          store: req.body.store,
          headOffice: req.body.hoID,
          imageProfile: file_url,
          createdDate: Date.now(),
          createdBy: user_data._id,
        }
      }
      User.findOne({
        email: req.body.email.toLowerCase()
      }, function (error, result) {
        if (!result) {
          User.create(userData, function (error, user) {
            if (error || user == null) {
              console.log(error, user)
              req.flash('info', 'Create User Failed! Something went wrong.')
              req.flash('info_type', 'danger')
              res.redirect('/admin/' + redirect)
            } else {
              req.flash('info', 'Create User success')
              req.flash('info_type', 'success')
              res.redirect('/admin/' + redirect)
            }
          })
        } else {
          req.flash('info', 'Email is already used')
          req.flash('info_type', 'danger')
          res.redirect('/admin/' + redirect)
        }
      })
    } else {
      req.flash('info', 'Password isn\'t match')
      req.flash('info_type', 'danger')
      res.redirect('/admin/' + redirect)
    }
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/' + redirect)
  }
}

//POST admin update user non API

exports.edit_user_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var redirect = req.params.redirect
  if (!redirect) redirect = ""
  if (req.body.name != '' && req.body.email != '' &&
    req.body.role != '' && req.body.expire != '') {
    var file_url = ""
    //Upload Profile Picture
    // upload.single("file" /* name attribute of <file> element in your form */),
    //   (req, res) => {
    //     const tempPath = req.file.path
    //     const random_name = Math.random().toString(36).substring(7)
    //     const extension = path.extname(req.file.originalname).toLowerCase()
    //     const targetPath = path.join(__dirname, "./assets/img/"+random_name+"."+extension)
    //     fs.rename(tempPath, targetPath, err => {
    //       if (err) return handleError(err, res)
    //       file_url = targetPath
    //     })
    //   }
    //You can't edit your own account
    if (user_data._id != req.body.id) {
      //If Role
      var file_url = req.file ? ('/image/' + res.req.file.filename) : ''
      console.log(file_url)
      if (req.body.role == 'sm_supervisor' || req.body.role == 'sm_warehouse') {
        var userData = {
          name: req.body.name,
          email: req.body.email.toLowerCase(),
          role: req.body.role,
          expire: req.body.expire,
          brand: req.body.brand,
          store: req.body.store,
          updatedDate: Date.now(),
          updatedBy: user_data._id,
        }
        if (file_url != '') userData.imageProfile = file_url
      } else if (req.body.role == 'bm') {
        var userData = {
          name: req.body.name,
          email: req.body.email.toLowerCase(),
          role: req.body.role,
          expire: req.body.expire,
          brand: req.body.brand,
          store: null,
          updatedDate: Date.now(),
          updatedBy: user_data._id,
        }
        if (file_url != '') userData.imageProfile = file_url
      } else if (req.body.role == 'admin') {
        var userData = {
          name: req.body.name,
          email: req.body.email.toLowerCase(),
          role: req.body.role,
          expire: req.body.expire,
          brand: null,
          store: null,
          imageProfile: file_url,
          updatedDate: Date.now(),
          updatedBy: user_data._id,
        }
        if (file_url != '') userData.imageProfile = file_url
      } else if (req.body.role == 'ho') {
        var userData = {
          name: req.body.name,
          email: req.body.email.toLowerCase(),
          role: req.body.role,
          expire: req.body.expire,
          brand: null,
          store: null,
          headOffice: req.body.hoID,
          imageProfile: file_url,
          updatedDate: Date.now(),
          updatedBy: user_data._id,
        }
        if (file_url != '') userData.imageProfile = file_url
      }
      User.findOneAndUpdate({
        _id: req.body.id
      }, userData, function (error, user) {
        if (error || user == null) {
          req.flash('info', 'Edit User Failed')
          req.flash('info_type', 'danger')
          res.redirect('/admin/' + redirect)
        } else {
          console.log(file_url)
          req.flash('info', 'Edit User success')
          req.flash('info_type', 'success')
          res.redirect('/admin/' + redirect)
        }
      })
    } else {
      req.flash('info', 'You can\'t edit your own account')
      req.flash('info_type', 'danger')
      res.redirect('/admin/' + redirect)
    }
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/' + redirect)
  }
}
// POST admin update user
exports.update_user = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.name != '' && req.body.email != '' && req.body.password != '' &&
        req.body.role != '') {
        var userData = {
          name: req.body.name,
          email: req.body.email,
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          role: req.body.role,
          expire: req.body.expire,
          brandId: req.body.brandId,
          storeId: req.body.storeId,
          imageProfile: "none",
          updatedDate: Date.now(),
          updatedBy: user._id,
        }
        User.findOneAndUpdate({
          _id: req.body.id
        }, userData, function (error, user) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          }
          if (user == null) {
            var err = {
              status: 400,
              message: 'User not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Update user success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// GET admin user list
exports.user_list = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      User.find({}, {
          '__v': 0
        })
        .exec(function (error, user) {
          if (err) {
            var err = {
              status: 400,
              message: err
            }
            return res.send(JSON.stringify(err))
          }
          if (!user || user.length == 0) {
            var err = {
              status: 400,
              message: 'User list not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              data: user
            }
            return res.send(JSON.stringify(ret))
          }
        })
    }
  })
}

// GET admin single user
exports.single_user = function (req, res, next) {
  if (req.params.userId) {
    User.findOne({
      _id: req.params.userId
    }, {
      '__v': 0
    }).populate("headOffice brand store").exec(function (err, user) {
      if (err) {
        var err = {
          status: 400,
          message: err
        }
        return res.send(JSON.stringify(err))
      }
      if (user == null) {
        var err = {
          status: 400,
          message: 'User not found'
        }
        return res.send(JSON.stringify(err))
      } else {
        var ret = {
          status: 200,
          data: user,
          date: dateFormat(user.datecreated, "yyyy-mm-dd")
        }
        return res.send(JSON.stringify(ret))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'User id required'
    }
    return res.send(JSON.stringify(err))
  }
}

// GET admin delete user
exports.delete_user = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.userId) {
        User.findByIdAndRemove(req.params.userId, function (error, user) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          }
          if (user == null) {
            var err = {
              status: 400,
              message: 'User not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Delete user success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'User id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// POST admin create brand
exports.create_brand = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.name) {
        var brandData = {
          name: req.body.name,
          createdDate: Date.now(),
          createdBy: user._id,
        }
        Brand.create(brandData, function (error, brand) {
          if (error || brand == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Create brand success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// POST admin update brand
exports.update_brand = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.id && req.body.name) {
        var brandData = {
          name: req.body.name,
          updatedDate: Date.now(),
          updatedBy: user._id,
        }
        Brand.findOneAndUpdate({
          _id: req.body.id
        }, brandData, function (error, brand) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          }
          if (brand == null) {
            var err = {
              status: 400,
              message: 'Brand not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Update brand success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// GET admin brand list
exports.brand_list = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      Brand.find({}, {
          '__v': 0
        })
        .exec(function (error, brand) {
          if (err) {
            var err = {
              status: 400,
              message: err
            }
            return res.send(JSON.stringify(err))
          }
          if (!brand || brand.length == 0) {
            var err = {
              status: 400,
              message: 'Brand list not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              data: brand
            }
            return res.send(JSON.stringify(ret))
          }
        })
    }
  })
}

// GET admin single brand
exports.single_brand = function (req, res, next) {
  if (req.params.brandId) {
    Brand.findOne({
        _id: req.params.brandId
      }).populate('stores')
      .exec(function (error, brand) {
        if (err) {
          var err = {
            status: 400,
            message: err
          }
          return res.send(JSON.stringify(err))
        }
        if (brand == null) {
          var err = {
            status: 400,
            message: 'Brand not found'
          }
          return res.send(JSON.stringify(err))
        } else {
          var ret = {
            status: 200,
            data: brand
          }
          return res.send(JSON.stringify(ret))
        }
      })
  } else {
    var err = {
      status: 400,
      message: 'Brand id required'
    }
    return res.send(JSON.stringify(err))
  }
}

// GET admin delete brand
exports.delete_brand = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.brandId) {
        Brand.findByIdAndRemove(req.params.brandId, function (error, brand) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          }
          if (brand == null) {
            var err = {
              status: 400,
              message: 'Brand not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Delete brand success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Brand id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// POST admin create store
exports.create_store = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.name && req.body.brandId) {
        var storeData = {
          name: req.body.name,
          brandId: req.body.brandId,
          createdDate: Date.now(),
          createdBy: user._id,
        }
        Store.create(storeData, function (error, store) {
          if (error || store == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            Brand.updateOne({
              _id: req.body.brandId
            }, {
              '$push': {
                stores: store._id.valueOf()
              }
            }, function (error, val) {
              if (error) {
                var err = {
                  status: 400,
                  message: error
                }
                return res.send(JSON.stringify(err))
              } else {
                var ret = {
                  status: 200,
                  message: 'Create store success.'
                }
                return res.send(JSON.stringify(ret))
              }
            })
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// POST admin update store
exports.update_store = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.id && req.body.name && req.body.brandId) {
        var storeData = {
          name: req.body.name,
          brandId: req.body.brandId,
          updatedDate: Date.now(),
          updatedBy: user._id,
        }
        Store.findOneAndUpdate({
          _id: req.body.id
        }, storeData, function (error, store) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          }
          if (store == null) {
            var err = {
              status: 400,
              message: 'Store not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Update store success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// GET admin store list
exports.store_list = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      Store.find({}, {
          '__v': 0
        })
        .exec(function (error, store) {
          if (err) {
            var err = {
              status: 400,
              message: err
            }
            return res.send(JSON.stringify(err))
          }
          if (!store || store.length == 0) {
            var err = {
              status: 400,
              message: 'Store list not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              data: store
            }
            return res.send(JSON.stringify(ret))
          }
        })
    }
  })
}

// GET admin single store
exports.single_store = function (req, res, next) {
  if (req.params.storeId) {
    Store.findOne({
        _id: req.params.storeId
      }, {
        '__v': 0
      }).populate("brand")
      .exec(function (error, store) {
        if (err) {
          var err = {
            status: 400,
            message: err
          }
          return res.send(JSON.stringify(err))
        }
        if (store == null) {
          var err = {
            status: 400,
            message: 'Store not found'
          }
          return res.send(JSON.stringify(err))
        } else {
          var ret = {
            status: 200,
            data: store
          }
          return res.send(JSON.stringify(ret))
        }
      })
  } else {
    var err = {
      status: 400,
      message: 'Store id required'
    }
    return res.send(JSON.stringify(err))
  }
}

// GET admin delete store
exports.delete_store = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.storeId) {
        Store.findByIdAndRemove(req.params.storeId, function (error, store) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          }
          if (store == null) {
            var err = {
              status: 400,
              message: 'Store not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Delete store success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Store id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// POST admin create product
exports.create_product = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.name && req.body.description && req.body.PLUId && req.body.price &&
        req.body.group && req.body.printerId && req.body.validStartDate && req.body.brandId) {
        var productData = {
          name: req.body.name,
          description: req.body.description,
          PLUId: req.body.PLUId,
          price: req.body.price,
          group: req.body.group,
          printerId: req.body.printerId,
          validStartDate: req.body.validStartDate,
          brandId: req.body.brandId,
          createdDate: Date.now(),
          createdBy: user._id,
        }
        Product.create(productData, function (error, product) {
          if (error || product == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Create product success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// POST admin update product
exports.update_product = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.id && req.body.name && req.body.description && req.body.PLUId && req.body.price &&
        req.body.group && req.body.printerId && req.body.validStartDate && req.body.brandId) {
        var productData = {
          name: req.body.name,
          description: req.body.description,
          PLUId: req.body.PLUId,
          price: req.body.price,
          group: req.body.group,
          printerId: req.body.printerId,
          validStartDate: req.body.validStartDate,
          brandId: req.body.brandId,
          updatedDate: Date.now(),
          updatedBy: user._id,
        }
        Product.findOneAndUpdate({
          _id: req.body.id
        }, productData, function (error, product) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          }
          if (product == null) {
            var err = {
              status: 400,
              message: 'Product not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Update product success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// GET admin product list
exports.product_list = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      Product.find({}, {
          '__v': 0
        })
        .exec(function (error, products) {
          if (err) {
            var err = {
              status: 400,
              message: err
            }
            return res.send(JSON.stringify(err))
          }
          if (!products || products.length == 0) {
            var err = {
              status: 400,
              message: 'Product list not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              data: products
            }
            return res.send(JSON.stringify(ret))
          }
        })
    }
  })
}

// GET admin single product
exports.single_product = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.productId) {
        Product.findOne({
            _id: req.params.productId
          }, {
            '__v': 0
          })
          .exec(function (error, product) {
            if (err) {
              var err = {
                status: 400,
                message: err
              }
              return res.send(JSON.stringify(err))
            }
            if (product == null) {
              var err = {
                status: 400,
                message: 'Product not found'
              }
              return res.send(JSON.stringify(err))
            } else {
              var ret = {
                status: 200,
                data: product
              }
              return res.send(JSON.stringify(ret))
            }
          })
      } else {
        var err = {
          status: 400,
          message: 'Product id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// GET admin delete product
exports.delete_product = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.productId) {
        Product.findByIdAndRemove(req.params.productId, function (error, product) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          }
          if (product == null) {
            var err = {
              status: 400,
              message: 'Product not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Delete product success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Product id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// POST admin create news
exports.create_news = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.title && req.body.description && req.body.validStartDate) {
        var newsData = {
          title: req.body.title,
          description: req.body.description,
          validStartDate: req.body.validStartDate,
          brandId: 'brandId',
          createdDate: Date.now(),
          createdBy: user._id,
        }
        News.create(newsData, function (error, news) {
          if (error || news == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Create news success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// POST admin update news
exports.update_news = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.id && req.body.title && req.body.description && req.body.validStartDate) {
        var newsData = {
          title: req.body.title,
          description: req.body.description,
          validStartDate: req.body.validStartDate,
          brandId: 'brandId',
          updatedDate: Date.now(),
          updatedBy: user._id,
        }
        News.findOneAndUpdate({
          _id: req.body.id
        }, newsData, function (error, news) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          }
          if (news == null) {
            var err = {
              status: 400,
              message: 'News not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Update news success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// GET admin news list
exports.news_list = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      News.find({}, {
          '__v': 0
        })
        .exec(function (error, news) {
          if (err) {
            var err = {
              status: 400,
              message: err
            }
            return res.send(JSON.stringify(err))
          }
          if (!news || news.length == 0) {
            var err = {
              status: 400,
              message: 'News list not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              data: news
            }
            return res.send(JSON.stringify(ret))
          }
        })
    }
  })
}

// GET admin single news
exports.single_news = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.newsId) {
        News.findOne({
            _id: req.params.newsId
          }, {
            '__v': 0
          })
          .exec(function (error, news) {
            if (err) {
              var err = {
                status: 400,
                message: err
              }
              return res.send(JSON.stringify(err))
            }
            if (news == null) {
              var err = {
                status: 400,
                message: 'News not found'
              }
              return res.send(JSON.stringify(err))
            } else {
              var ret = {
                status: 200,
                data: news
              }
              return res.send(JSON.stringify(ret))
            }
          })
      } else {
        var err = {
          status: 400,
          message: 'News id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// GET admin delete news
exports.delete_news = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.newsId) {
        News.findByIdAndRemove(req.params.newsId, function (error, news) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          }
          if (news == null) {
            var err = {
              status: 400,
              message: 'News not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Delete news success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'News id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// POST admin create revenue
exports.create_revenue = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.date && req.body.shift && req.body.cash && req.body.card &&
        req.body.debit && req.body.other && req.body.discount && req.body.tax && req.body.brandId && req.body.storeId && req.body.user_id) {
        var total = (parseInt(req.body.cash) + parseInt(req.body.card) + parseInt(req.body.debit) + parseInt(req.body.other)) - (parseInt(req.body.tax) + parseInt(req.body.discount))
        var revenueData = {
          date: req.body.date,
          shift: req.body.shift,
          cash: req.body.cash,
          card: req.body.card,
          debit: req.body.debit,
          other: req.body.other,
          discount: req.body.discount,
          tax: req.body.tax,
          total: total,
          brand: req.body.brandId,
          store: req.body.storeId,
          createdDate: Date.now(),
          createdBy: req.body.user_id,
        }
        Revenue.create(revenueData, function (error, revenue) {
          if (error || revenue == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Create revenue success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// POST admin update revenue
exports.update_revenue = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.id && req.body.date && req.body.shift && req.body.cash && req.body.card &&
        req.body.debit && req.body.other && req.body.discount && req.body.tax) {
        var revenueData = {
          date: req.body.date,
          shift: req.body.shift,
          cash: req.body.cash,
          card: req.body.card,
          debit: req.body.debit,
          other: req.body.other,
          discount: req.body.discount,
          tax: req.body.tax,
          brandId: 'admin',
          storeId: 'admin',
          updatedDate: Date.now(),
          updatedBy: user._id,
        }
        Revenue.findOneAndUpdate({
          _id: req.body.id
        }, revenueData, function (error, revenue) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          }
          if (revenue == null) {
            var err = {
              status: 400,
              message: 'Revenue not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Update revenue success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// GET admin revenue list
exports.revenue_list = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      Revenue.find({}, {
          '__v': 0
        })
        .exec(function (error, revenues) {
          if (err) {
            var err = {
              status: 400,
              message: err
            }
            return res.send(JSON.stringify(err))
          }
          if (!revenues || revenues.length == 0) {
            var err = {
              status: 400,
              message: 'Revenue list not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              data: revenues
            }
            return res.send(JSON.stringify(ret))
          }
        })
    }
  })
}

// POST admin single revenue
exports.single_revenue = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.revenueId) {
        Revenue.findOne({
            _id: req.params.revenueId
          }, {
            '__v': 0
          })
          .exec(function (error, revenue) {
            if (err) {
              var err = {
                status: 400,
                message: err
              }
              return res.send(JSON.stringify(err))
            }
            if (revenue == null) {
              var err = {
                status: 400,
                message: 'Revenue not found'
              }
              return res.send(JSON.stringify(err))
            } else {
              var ret = {
                status: 200,
                data: revenue
              }
              return res.send(JSON.stringify(ret))
            }
          })
      } else {
        var err = {
          status: 400,
          message: 'Revenue id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// GET admin delete revenue
exports.delete_revenue = function (req, res, next) {
  Session.validate(req.params.key, 'admin', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.revenueId) {
        Revenue.findByIdAndRemove(req.params.revenueId, function (error, revenue) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          }
          if (revenue == null) {
            var err = {
              status: 400,
              message: 'Revenue not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Delete revenue success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Revenue id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

//Get Store list
exports.get_store_list = function (req, res, next) {
  auth_redirect(req, res)
  if (req.params.brandId) {
    Store.find({
        brand: req.params.brandId
      }, {
        '__v': 0
      })
      .exec(function (error, result) {
        if (err) {
          var err = {
            status: 400,
            message: err
          }
          return res.send(JSON.stringify(err))
        }
        if (result == null) {
          var err = {
            status: 400,
            message: 'No Store'
          }
          return res.send(JSON.stringify(err))
        } else {
          var ret = {
            status: 200,
            data: result
          }
          return res.send(JSON.stringify(ret))
        }
      })
  } else {
    var err = {
      status: 400,
      message: 'Brand id required'
    }
    return res.send(JSON.stringify(err))
  }
}

// POST sm create product
exports.create_product_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name && req.body.description && req.body.PLUId && req.body.price && req.body.productcategory &&
    req.body.productgroup && req.body.printerId && req.body.validStartDate && req.body.brandId) {
    var productData = {
      name: req.body.name,
      description: req.body.description,
      PLUId: req.body.PLUId,
      price: req.body.price,
      productcategory: req.body.productcategory,
      productgroup: req.body.productgroup,
      imageProduct: file_url,
      group: req.body.group,
      printerId: req.body.printerId,
      validStartDate: req.body.validStartDate,
      brand: req.body.brandId,
      createdDate: Date.now(),
      createdBy: user_data._id,
    }
    Product.create(productData, function (error, product) {
      if (error || product == null) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Create product success')
        req.flash('info_type', 'success')
        res.redirect('/admin/product')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/product')
  }
}

//POST admin Update Product
exports.update_product_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  console.log(req.body)
  if (req.body.id && req.body.name && req.body.description && req.body.price &&
    req.body.productgroup && req.body.brandId) {
    var productData = {
      name: req.body.name,
      description: req.body.description,
      PLUId: req.body.PLUId,
      price: req.body.price,
      group: req.body.group,
      printerId: req.body.printerId,
      productcategory: req.body.productcategory,
      productgroup: req.body.productgroup,
      validStartDate: req.body.validStartDate,
      brand: req.body.brandId,
      updatedDate: Date.now(),
      updatedBy: user_data._id,
    }
    Product.findOneAndUpdate({
      _id: req.body.id
    }, productData, function (error, product) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (product == null) {
        req.flash('info', 'Product update failed')
        req.flash('info_type', 'danger')
        res.redirect('/admin/product')
      } else {
        req.flash('info', 'Product update success')
        req.flash('info_type', 'success')
        res.redirect('/admin/product')
      }
    })
  } else {
    req.flash('info', 'All fields Required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/product')
  }
}

//GET admin Data Product
exports.get_single_product = function (req, res, next) {
  if (req.params.productId) {
    Product.findOne({
        _id: req.params.productId
      }, {
        '__v': 0
      }).populate('brand createdBy updatedBy')
      .exec(function (error, product) {
        if (err) {
          var err = {
            status: 400,
            message: err
          }
          return res.send(JSON.stringify(err))
        }
        if (product == null) {
          var err = {
            status: 400,
            message: 'Product not found'
          }
          return res.send(JSON.stringify(err))
        } else {
          var ret = {
            status: 200,
            data: product
          }
          return res.send(JSON.stringify(ret))
        }
      })
  } else {
    var err = {
      status: 400,
      message: 'Product id required'
    }
    return res.send(JSON.stringify(err))
  }
}

//GET admin Delete Product
exports.delete_product_process = function (req, res, next) {
  auth_redirect(req, res)
  if (req.params.productId) {
    Product.findByIdAndRemove(req.params.productId, function (error, product) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (product == null) {
        req.flash('info', 'Product not found')
        req.flash('info_type', 'danger')
        res.redirect('/admin/product')
      } else {
        req.flash('info', 'Delete product success')
        req.flash('info_type', 'success')
        res.redirect('/admin/product')
      }
    })
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/product')
  }
}

//news
exports.news = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load News Data
    function (callback) {
      News.find({}).populate('brand createdBy updatedBy').sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.news = result
        callback()
      })
    },
    function (callback) {
      Brand.find({}, function (err, result) {
        if (err) return callback(err)
        locals.brands = result
        callback()
      })
    }
  ], function (err) {
    if (err) return next(err)
    res.render('news', {
      title: appName + " | " + "News",
      user_data: user_data,
      news: locals.news,
      brands: locals.brands,
      messages: messages,
      messages_type: messages_type
    })
  })
}

// POST sm create news
exports.create_news_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.title && req.body.description && req.body.valid_start_date && req.body.brandId) {
    var newsData = {
      title: req.body.title,
      description: req.body.description,
      validStartDate: req.body.valid_start_date,
      brand: req.body.brandId,
      createdDate: Date.now(),
      createdBy: user_data._id,
      updatedDate: Date.now(),
      updatedBy: user_data._id,
    }
    News.create(newsData, function (error, news) {
      if (error || news == null) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Create news success')
        req.flash('info_type', 'success')
        res.redirect('/admin/news')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/news')
  }
}

// POST sm delete news
exports.delete_news_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.newsId) {
    News.findByIdAndRemove(req.params.newsId, function (error, news) {
      if (error || news == null) {
        console.log(error)
        req.flash('info', 'Delete news failed')
        req.flash('info_type', 'danger')
        res.redirect('/admin/news')
      } else {
        req.flash('info', 'Delete news success')
        req.flash('info_type', 'success')
        res.redirect('/admin/news')
      }
    })
  } else {
    req.flash('info', 'News id required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/news')
  }
}

//brand
exports.brand = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    // Load HO Data
    function (callback) {
      HO.find({}, function (err, result) {
        if (err) return callback(err)
        locals.HO = result
        callback()
      })
    },
    // Load Brand Data
    function (callback) {
      Brand.find({}).populate('headOffice createdBy updatedBy').sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.brands = result
        callback()
      })
    },
    function (callback) {
      Store.find({}).populate("brand createdBy updatedBy").sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.stores = result
        callback()
      })
    }
  ], function (err) { //This function gets called after the two tasks have called their "task callbacks"
    if (err) return next(err) //If an error occurred, we let express handle it by calling the `next` function
    //Here `locals` will be an object with `user` and `posts` keys
    res.render('brand', {
      title: appName + " | " + "Brands",
      user_data: user_data,
      brands: locals.brands,
      stores: locals.stores,
      HO: locals.HO,
      messages: messages,
      messages_type: messages_type
    })
  })
}

// POST create brand
exports.create_brand_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name && req.body.code) {
    var brandData = {
      code: req.body.code,
      name: req.body.name,
      headOffice: req.body.hoID,
      createdDate: Date.now(),
      createdBy: user_data._id,
    }
    Brand.create(brandData, function (error, result) {
      if (error || result == null) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      } else {
        HO.updateOne({
          _id: req.body.hoID
        }, {
          '$push': {
            brands: result._id.valueOf()
          }
        }, function (error, val) {
          if (error) {
            req.flash('info', 'Create Brand failed')
            req.flash('info_type', 'danger')
            res.redirect('/admin/brand')
          } else {
            req.flash('info', 'Create Brand success')
            req.flash('info_type', 'success')
            res.redirect('/admin/brand')
          }
        })
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/brand')
  }
}

//POST update brand
exports.update_brand_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name && req.body.id && req.body.code ) {
    var brandData = {
      _id: req.body.id,
      code: req.body.code,
      headOffice: req.body.hoID,
      name: req.body.name,
      updatedDate: Date.now(),
      updatedBy: user_data._id,
    }
    Brand.findOne({
      _id: brandData._id
    }, function (error, result) {
      if (error || result == null) {
        req.flash('info', 'Update Brand Failed')
        req.flash('info_type', 'danger')
        res.redirect('/admin/brand')
      } else {
        HO.updateOne({
          _id: result.headOffice
        }, {
          '$pull': {
            brands: req.body.id
          }
        }, function (error, val) {
          if (error) {
            req.flash('info', 'Update Brand Failed')
            req.flash('info_type', 'danger')
            res.redirect('/admin/brand')
          } else {
            Brand.findOneAndUpdate({
              _id: brandData._id
            }, brandData, function (error, result) {
              if (error || result == null) {
                req.flash('info', 'Update Brand Failed')
                req.flash('info_type', 'danger')
                res.redirect('/admin/brand')
              } else {
                HO.updateOne({
                  _id: req.body.hoID
                }, {
                  '$push': {
                    brands: req.body.id
                  }
                }, function (error, val) {
                  if (error) {
                    req.flash('info', 'Update Brand Failed')
                    req.flash('info_type', 'danger')
                    res.redirect('/admin/brand')
                  } else {
                    req.flash('info', 'Update Brand Success')
                    req.flash('info_type', 'success')
                    res.redirect('/admin/brand')
                  }
                })
              }
            })
          }
        })
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/brand')
  }
}

//GET delete brand
exports.delete_brand_process = function (req, res, next) {
  auth_redirect(req, res)
  if (req.params.brandId) {

    Brand.findOne({
      _id: req.params.brandId
    }, function (error, result) {
      if (error || result == null) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      } else {
        HO.updateOne({
          _id: result.headOffice
        }, {
          '$pull': {
            brands: result._id.valueOf()
          }
        }, function (error, val) {
          if (error || val == null) {
            req.flash('info', 'Delete Brand Failed')
            req.flash('info_type', 'danger')
            res.redirect('/admin/brand')
          } else {
            return true
          }
        })
      }
    })

    Brand.findByIdAndRemove(req.params.brandId, function (error, result) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (result == null) {
        req.flash('info', 'Brand not found')
        req.flash('info_type', 'danger')
        res.redirect('/admin/brand')
      } else {
        req.flash('info', 'Delete Brand Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/brand')
      }
    })

  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/brand')
  }
}

// POST create store
exports.create_store_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name) {
    var brandData = {
      brand: req.body.brand,
      name: req.body.name,
      outerJakarta: req.body.outerJakarta,
      unitKerja: req.body.unitKerja,
      createdDate: Date.now(),
      createdBy: user_data._id,
    }
    Store.create(brandData, function (error, result) {
      if (error || result == null) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      } else {
        Brand.updateOne({
          _id: req.body.brand
        }, {
          '$push': {
            stores: result._id.valueOf()
          }
        }, function (error, val) {
          if (error) {
            req.flash('info', 'Create Store failed')
            req.flash('info_type', 'danger')
            res.redirect('/admin/brand')
          } else {
            req.flash('info', 'Create Store success')
            req.flash('info_type', 'success')
            res.redirect('/admin/brand')
          }
        })
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/brand')
  }
}

//POST update store
exports.update_store_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name != "" && req.body.id != "") {
    var StoreData = {
      _id: req.body.id,
      brand: req.body.brandId,
      name: req.body.name,
      outerJakarta: req.body.outerJakarta,
      unitKerja: req.body.unitKerja,
      updatedDate: Date.now(),
      updatedBy: user_data._id,
    }

    Store.findOne({
      _id: req.body.id
    }, function (error, result) {
      if (error || result == null) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      } else {
        Brand.updateOne({
          _id: result.brand
        }, {
          '$pull': {
            stores: req.body.id
          }
        }, function (error, val) {
          if (error) {
            req.flash('info', 'Update Store Failed')
            req.flash('info_type', 'danger')
            res.redirect('/admin/brand')
          } else {
            Store.findOneAndUpdate({
              _id: StoreData._id
            }, StoreData, function (error, result) {
              if (error || result == null) {
                var err = {
                  status: 400,
                  message: error
                }
                return res.send(JSON.stringify(err))
              } else {
                Brand.updateOne({
                  _id: req.body.brandId
                }, {
                  '$push': {
                    stores: req.body.id
                  }
                }, function (error, val) {
                  if (error) {
                    req.flash('info', 'Update Store Failed')
                    req.flash('info_type', 'danger')
                    res.redirect('/admin/brand')
                    console.log(error)
                  } else {
                    req.flash('info', 'Update Store Success')
                    req.flash('info_type', 'success')
                    res.redirect('/admin/brand')
                  }
                })
              }
            })
          }
        })
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/brand')
  }
}

//GET delete store
exports.delete_store_process = function (req, res, next) {
  auth_redirect(req, res)
  if (req.params.storeId) {
    Store.findOne({
      _id: req.params.storeId
    }, function (error, result) {
      if (error || result == null) {
        req.flash('info', 'Store Not Found')
        req.flash('info_type', 'danger')
        res.redirect('/admin/brand')
      } else {
        Brand.updateOne({
          _id: result.brand
        }, {
          '$pull': {
            stores: req.params.storeId
          }
        }, function (error2, result2) {
          if (error2) {
            req.flash('info', 'Update Store Failed')
            req.flash('info_type', 'danger')
            res.redirect('/admin/brand')
          } else {
            Store.findByIdAndRemove(req.params.storeId, function (error3, result3) {
              if (error || result == null) {
                req.flash('info', 'Store not found')
                req.flash('info_type', 'danger')
                res.redirect('/admin/brand')
              } else {
                req.flash('info', 'Delete Store success')
                req.flash('info_type', 'success')
                res.redirect('/admin/brand')
              }
            })
          }
        })
      }
    })
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/brand')
  }
}

//revenue
exports.revenue = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load Revenue Data
    function (callback) {
      Revenue.find({}).populate('store createdBy updatedBy').sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.revenue = result
        callback()
      })
    },
    // Load Brand Data
    function (callback) {
      Brand.find({}, function (err, result) {
        if (err) return callback(err)
        locals.brands = result
        callback()
      })
    }
  ], function (err) { //This function gets called after the two tasks have called their "task callbacks"
    if (err) return next(err) //If an error occurred, we let express handle it by calling the `next` function
    //Here `locals` will be an object with `user` and `posts` keys
    res.render('revenue', {
      title: appName + " | " + "Revenue",
      user_data: user_data,
      revenue: locals.revenue,
      brands: locals.brands,
      messages: messages,
      messages_type: messages_type
    })
  })
}

// POST sm create revenue
exports.create_revenue_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.date && req.body.shift && req.body.cash && req.body.card &&
    req.body.debit && req.body.other && req.body.discount && req.body.tax && req.body.brandId && req.body.storeId) {
    var total = (parseInt(req.body.cash) + parseInt(req.body.card) + parseInt(req.body.debit) + parseInt(req.body.other)) - (parseInt(req.body.tax) + parseInt(req.body.discount))
    var revenueData = {
      date: req.body.date,
      shift: req.body.shift,
      cash: req.body.cash,
      card: req.body.card,
      debit: req.body.debit,
      other: req.body.other,
      discount: req.body.discount,
      tax: req.body.tax,
      total: total,
      brand: req.body.brandId,
      store: req.body.storeId,
      createdDate: Date.now(),
      createdBy: user_data._id,
      updatedDate: Date.now(),
      updatedBy: user_data._id,
    }
    Revenue.create(revenueData, function (error, revenue) {
      if (error || revenue == null) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Create revenue success')
        req.flash('info_type', 'success')
        res.redirect('/admin/revenue')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/revenue')
  }
}

// GET admin delete revenue
exports.delete_revenue_process = function (req, res, next) {
  auth_redirect(req, res)
  if (req.params.revenueId) {
    Revenue.findByIdAndRemove(req.params.revenueId, function (error, revenue) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (revenue == null) {
        req.flash('info', 'Revenue not found')
        req.flash('info_type', 'danger')
        res.redirect('/admin/revenue')
      } else {
        req.flash('info', 'Delete revenue success')
        req.flash('info_type', 'success')
        res.redirect('/admin/revenue')
      }
    })
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/revenue')
  }
}


//GET HO
exports.ho = function (req, res, nex) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var user_list = []
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load HO Data
    function (callback) {
      HO.find({}).populate('brands createdBy updatedBy').sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.HO = result
        callback()
      })
    },
    //Load Brand Data
    function (callback) {
      Brand.find({}, function (err, result) {
        if (err) return callback(err)
        locals.brands = result
        callback()
      })
    }
  ], function (err) { //This function gets called after the two tasks have called their "task callbacks"
    if (err) return next(err) //If an error occurred, we let express handle it by calling the `next` function
    res.render('ho', {
      title: appName + " | " + "Head Office",
      user_data: user_data,
      HO: locals.HO,
      brands: locals.brands,
      messages: messages,
      messages_type: messages_type
    })
  })
}

//POST HO
exports.create_ho = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name != '' && req.body.brandId != '') {
    var HOData = {
      name: req.body.name,
      createdDate: Date.now(),
      createdBy: user_data._id,
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    HO.create(HOData, function (error, result) {
      if (error || result == null) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Create Head Office success')
        req.flash('info_type', 'success')
        res.redirect('/admin/ho')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/ho')
  }
}

//GET Data HO
exports.get_single_ho = function (req, res, next) {
  if (req.params.hoID) {
    HO.findOne({
        _id: req.params.hoID
      }).populate('brands')
      .exec(function (error, result) {
        if (err) {
          var err = {
            status: 400,
            message: err
          }
          return res.send(JSON.stringify(err))
        }
        if (result == null) {
          var err = {
            status: 400,
            message: 'Head Office not found'
          }
          return res.send(JSON.stringify(err))
        } else {
          var ret = {
            status: 200,
            data: result
          }
          return res.send(JSON.stringify(ret))
        }
      })
  } else {
    var err = {
      status: 400,
      message: 'Head Office id required'
    }
    return res.send(JSON.stringify(err))
  }
}

//POST Edit HO
exports.edit_ho_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name != '') {
    var HOData = {
      name: req.body.name,
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    HO.findOneAndUpdate({
      _id: req.body.id
    }, HOData, function (error, result) {
      if (error || result == null) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Update Head Office success')
        req.flash('info_type', 'success')
        res.redirect('/admin/ho')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/admin/ho')
  }
}

// GET Delete HO
exports.delete_ho_process = function (req, res, next) {
  auth_redirect(req, res)
  var redirect = ""
  redirect = req.params.redirect
  if (req.params.hoID) {
    HO.findByIdAndRemove(req.params.hoID, function (error, result) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (result == null) {
        req.flash('info', 'Head Office not found')
        req.flash('info_type', 'danger')
        res.redirect('/admin/ho')
      } else {
        req.flash('info', 'Head Office  User success')
        req.flash('info_type', 'success')
        res.redirect('/admin/ho')
      }
    })
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/admin')
  }
}

//verifyURL
exports.verifyURL = function (req, res) {
  var verifyURL = billingAPI + "update_token_data/"
  var token = req.body.token
  var email = req.body.email.toLowerCase()
  User.findOne({
    email: email
  }, function (err, user) {
    if (err || user == null) {
      var ret = {
        status: 400,
        message: "Invalid Email"
      }
      res.send(JSON.stringify(ret))
    } else {
      request(verifyURL + token + "/" + user.role, {
        headers: {
          'secretCode': secretCode
        }
      }, function (error, response, body) {
        var result = JSON.parse(body)
        if (result.status == 200) {
          if (user.role == "ho") {
            //update ho
            var data_update = {
              token: token
            }
            HO.findOneAndUpdate({
              _id: user.headOffice
            }, data_update, function (err, ho) {
              if (err || ho == null) {
                var ret = {
                  status: 400,
                  message: "Invalid Email"
                }
                res.send(JSON.stringify(ret))
              } else {
                var ret = {
                  status: 200,
                  message: "Token valid, loging in.."
                }
                res.send(JSON.stringify(ret))
              }
            })
          } else if (user.role == "sm_warehouse" || user.role == "sm_supervisor") {
            //update store
            var data_update = {
              token: token
            }
            Store.findOneAndUpdate({
              _id: user.store
            }, data_update, function (err, user) {
              if (err || user == null) {
                var ret = {
                  status: 400,
                  message: "Invalid Email"
                }
                res.send(JSON.stringify(ret))
              } else {
                var ret = {
                  status: 200,
                  message: "Token valid, loging in.."
                }
                res.send(JSON.stringify(ret))
              }
            })
          } 
        } else {
          var ret = {
            status : 400,
            message : result.message
          }
          res.send(JSON.stringify(ret))
        }
      })
    }
  })
}