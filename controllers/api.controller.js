const User = require('../models/user.model')
const News = require('../models/news.model')
const Session = require('../models/session.model')
const Product = require('../models/product.model')
const DeliveryOrder = require('../models/delivery_order.model')
const DOItem = require('../models/do_item.model')
const Brand = require('../models/brand.model')
const Store = require('../models/store.model')
const Stock = require('../models/stock.model')
const Payment = require('../models/payment.model')
const Table = require('../models/table.model')
const ProductCategory = require('../models/product_category.model')
const ProductGroup = require('../models/product_group.model')
const ProductUpdate = require('../models/product_update.model')
const Discount = require('../models/discount.model')
const Unit = require('../models/unit.model')
const Item = require('../models/item.model')
const Revenue = require('../models/revenue.model')
const HeadOffice = require('../models/head_office.model')
const Supplier = require('../models/supplier.model')

const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId
const Schema = mongoose.Schema

var fs = require('fs')
const StockTransaction = require('../models/stock_transaction.model')
const moment = require('moment')
var crypto = require('crypto')
var dateFormat = require('dateformat')
var async = require('async')
var QRCode = require('qrcode')
var request = require('request')

var nodemailer = require('nodemailer')
var axios = require('axios')
var transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: '587',
  auth: {
    user: "maningcorp@gmail.com",
    pass: "maning123!"
  },
  secureConnection: 'false',
  tls: {
    ciphers: 'SSLv3',
    rejectUnauthorized: false

  }
})

function ID() {
  return Math.random().toString(36).substr(2, 6).toUpperCase()
}

//--------- PRODUCT API ---------///
//CREATE
exports.create_product = function (req, res, next) {
  if (req.body.name != '' && req.body.description != '' && req.body.PLUId != '' && req.body.price != '' &&
    req.body.group != '' && req.body.printerId != '' && req.body.validStartDate != '' && req.body.brandId != '' && req.body.user_id != '') {
    var productData = {
      name: req.body.name,
      description: req.body.description,
      PLUId: req.body.PLUId,
      price: req.body.price,
      group: req.body.group,
      printerId: req.body.printerId,
      validStartDate: req.body.validStartDate,
      brand: req.body.brandId,
      createdDate: Date.now(),
      createdBy: req.body.user_id
    }
    Product.create(productData, function (error, result) {
      if (error || result == null) {
        var err = {
          status: 400,
          message: "Create Product Error"
        }
        return res.send(JSON.stringify(err))
      } else {
        var ret = {
          status: 200,
          data: productData
        }
        return res.send(JSON.stringify(ret))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'All fields is required'
    }
    return res.send(JSON.stringify(err))
  }
}


//--------- BRAND API ---------///
//READ
exports.read_brand = function (req, res, next) {
  Brand.find({}, {
      '__v': 0
    })
    .exec(function (error, result) {
      if (err) {
        var err = {
          status: 400,
          messsage: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      }
      if (!result || result.length == 0) {
        var err = {
          status: 400,
          message: 'Brand list not found'
        }
        return res.send(JSON.stringify(err))
      } else {
        var ret = {
          status: 200,
          data: result
        }
        return res.send(JSON.stringify(ret))
      }
    })
}

//--------- STORE API ---------///
//READ
exports.read_store = function (req, res, next) {
  Store.find({}, {
      '__v': 0
    })
    .exec(function (error, result) {
      if (err) {
        var err = {
          status: 400,
          message: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      }
      if (!result || result.length == 0) {
        var err = {
          status: 400,
          message: 'Store list not found'
        }
        return res.send(JSON.stringify(err))
      } else {
        var ret = {
          status: 200,
          data: result
        }
        return res.send(JSON.stringify(ret))
      }
    })
}

//--------- USER API ---------///
//READ
exports.read_user = function (req, res, next) {
  User.find({}, {
      '__v': 0
    })
    .exec(function (error, result) {
      if (err) {
        var err = {
          status: 400,
          messsage: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      }
      if (!result || result.length == 0) {
        var err = {
          status: 400,
          message: 'Store list not found'
        }
        return res.send(JSON.stringify(err))
      } else {
        var ret = {
          status: 200,
          data: result
        }
        return res.send(JSON.stringify(ret))
      }
    })
}

//--------- REVENUE API ---------///
exports.create_revenue = function (req, res, next) {
  console.log(req.body)

  var keys = Object.keys(req.body)
  var vals =  Object.keys(Object.values(req.body)[0])
  var tempdt = keys+"["+vals+"]}"
  console.log("------------")
  console.log('tempdt',tempdt)

  var postdata = JSON.parse(tempdt)  
  console.log("------------")
  console.log('postdata',postdata)

  if (req.body) {
    var unitkerja = postdata.header.kd_unitkerja    
    Store.findOne({
      unitKerja: unitkerja
    }, function (err, store) {
      if (err || store == null) {
        var err = {
          status: 400,
          message: 'Invalid Unit Kerja'
        }
        return res.send(JSON.stringify(err))
      } else {
        request(billingAPI + "checkToken/" + store.token, {
          headers: {
            'secretCode': secretCode
          }
        }, function (error, response, body) {
          var result = JSON.parse(body)
          if (result.status == "200") {
            let promises = []
            postdata.data.map((val, i) => {
              promises.push(new Promise((resolve, reject) => {
                var total = (parseInt(val.cash) + parseInt(val.cc) + parseInt(val.debit) + (val.other != '' ? parseInt(val.other) : 0)) - (parseInt(val.tax) + parseInt(val.disc))
                var revenueData = {
                  date: postdata.header.tgl_trans,
                  shift: parseInt(val.shift),
                  cash: parseInt(val.cash),
                  card: parseInt(val.cc),
                  debit: parseInt(val.debit),
                  other: (val.other != '' ? parseInt(val.other) : 0),
                  discount: parseInt(val.disc),
                  tax: parseInt(val.tax),
                  total: total,
                  store: store._id,
                  brand: store.brand,
                  createdDate: Date.now()
                }
                Revenue.create(revenueData, function (error, revenue) {
                  if (error || revenue == null) {
                    reject(error)
                  } else {
                    resolve(revenue)
                  }
                })
              }))
            })
            Promise.all(promises).then(function (revenues){
              var ret = {
                status: 200,
                message: 'Create revenue success.',
                data: postdata
              }
              return res.send(JSON.stringify(ret))          
            }).catch(err => {
              console.log(err)
              var err = {
                status: 400,
                messsage: "Something went wrong"
              }
              return res.send(JSON.stringify(err))
            })  
          } else {
            var ret = {
              status: result.status,
              message: result.message
            }
            res.send(JSON.stringify(ret))
          }
        })      
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'All fields required'
    }
    return res.send(JSON.stringify(err))
  }
}


//-------- POS RELATED API -------//
exports.product_update_checker = function (req, res) {
  Brand.findOne({code: req.params.brandcode}, function (err, brand) {
    if (err || brand == null) {
      var err = {
        status: 400,
        message: 'Invalid Brand Code'
      }
      return res.send(JSON.stringify(err))
    } else {
      Store.findOne({
        brand: brand._id,
        unitKerja: req.params.unitkerja
      }, function (err, store) {
        if (err || store == null) {
          var err = {
            status: 400,
            message: 'Invalid Unit Kerja'
          }
          return res.send(JSON.stringify(err))
        } else {
          console.log('brand',brand._id, brand.name)
          console.log('store',store._id, store.name)          
          request(billingAPI + "checkToken/" + store.token, {
            headers: {
              'secretCode': secretCode
            }
          }, function (error, response, body) {
            var result = JSON.parse(body)
            console.log(result)
            if (result.status == "200") {
              var brandId = brand._id
              var locals = {}
              async.parallel([
                function (callback) {
                  var start = moment(req.params.updatedate).startOf('day');
                  var end = moment(req.params.updatedate).endOf('day');
                  ProductUpdate.findOne({brand:brandId, updateToPOSDate:{ $gte:start, $lte:end }})
                    .populate({path: 'productChanges.product', populate: {path: 'productCategory'}})
                    .exec(function (err, product_updates) {
                      if (err) return callback(err)
                      locals.product_update = {validDate:product_updates.validDate,updateToPOSDate:product_updates.updateToPOSDate}
                      locals.product_change_list = product_updates != null ? product_updates.productChanges : null
                      callback()       
                    })
                },            
              ], function (err) {
                if (err) {
                  var err = {
                    status: 400,
                    messsage: "Something went wrong"
                  }
                  return res.send(JSON.stringify(err))
                } else {
                  if(locals.product_change_list != null){
                    var ret = {
                      status: 200,
                      brand: brand.code,
                      kd_unitkerja: store.unitKerja,
                      validDate: moment(locals.product_update.validDate).format('YYYY-MM-DD'),
                      updateToPOSDate: moment(locals.product_update.updateToPOSDate).format('YYYY-MM-DD'),                        
                      item: locals.product_change_list.map(p=>{
                        return {
                          id: p._id,
                          prod_no: p.product.productCode,
                          prod_group: p.product.productCategory.code,
                          prod_name: p.name != p.product.name && p.name != null ? p.name : p.product.name,
                          prod_desc: p.description != p.product.description && p.description != null ? p.description : p.product.description,
                          prod_price: p.price != p.product.price && p.price != null ? p.price : p.product.price,
                          kd_printer: p.product.printerId
                        }
                      })
                    }
                    return res.send(JSON.stringify(ret))     
                  } else {
                    var ret = {
                      status: 200,
                      brand: brand.code,                      
                      kd_unitkerja: store.unitKerja,
                      validDate: moment(locals.product_update.validDate).format('YYYY-MM-DD'),
                      updateToPOSDate: moment(locals.product_update.updateToPOSDate).format('YYYY-MM-DD'),                      
                      item: []
                    }
                    return res.send(JSON.stringify(ret))     
                  }
                }
              })
            } else {
              var ret = {
                status: result.status,
                message: result.message
              }
              res.send(JSON.stringify(ret))
            }
          })      
        }
      })
    }
  })
}
exports.push_product_category = function (req, res, next) {
  console.log(req.body)
  var keys = Object.keys(req.body)
  var vals =  Object.keys(Object.values(req.body)[0])
  var tempdt = keys+"["+vals+"]}"
  // console.log("------------")
  // console.log('tempdt',tempdt)

  var postdata = JSON.parse(tempdt)  
  // console.log("------------")
  // console.log('postdata',postdata) 

  // var postdata = req.body
  
  if (req.body) {
    Brand.findOne({code: postdata.brand}, function (err, brand) {
      if (err || brand == null) {
        var err = {
          status: 400,
          message: 'Invalid Brand Code'
        }
        return res.send(JSON.stringify(err))
      } else {    
        var unitkerja = postdata.kd_unitkerja 
        Store.findOne({
          brand: brand._id,
          unitKerja: unitkerja
        }, function (err, store) {
          if (err || store == null) {
            var err = {
              status: 400,
              message: 'Invalid Unit Kerja'
            }
            return res.send(JSON.stringify(err))
          } else {
            console.log('brand',brand._id, brand.name)
            console.log('store',store._id, store.name)          
            request(billingAPI + "checkToken/" + store.token, {
              headers: {
                'secretCode': secretCode
              }
            }, function (error, response, body) {
              var result = JSON.parse(body)
              console.log(result)
              if (result.status == "200") {
                ProductGroup.find({brand: brand._id}, {code:1, name:1}, function (err, productGroupList) {
                  if (err || productGroupList == null) {
                    var err = {
                      status: 400,
                      message: 'Error, cannot find product group list for this brand id'
                    }
                    return res.send(JSON.stringify(err))
                  } else {
                    ProductCategory.deleteMany({brand: brand._id}, function (err, removedProductCategories) {
                      if (err || removedProductCategories == null) {
                        var err = {
                          status: 400,
                          message: 'Error, cannot delete product categories for this brand id'
                        }
                        return res.send(JSON.stringify(err))
                      } else {
                        let promises = []
                        var productCategoriesPostData = postdata.item
                        productCategoriesPostData.map((dt) => {
                          promises.push(new Promise((resolve, reject) => {
                            var productGroup = productGroupList.find(pg=>pg.code == dt.group_jenis)
                            if (typeof productGroup == 'undefined'){
                              reject("Group not found for product category "+dt.prod_group)
                            } else {
                              var createData = {
                                code: dt.prod_group,                  //prod_group
                                name: dt.group_desc,                  //group_desc  
                                productGroupCode: productGroup.code,  //group_jenis
                                groupDisc: dt.group_disc,             //group_disc
                                groupParent: dt.group_parent,         //group_parent
                                groupList: dt.group_list,             //group_list
                                groupMultidisc: dt.group_multidisc,   //group_multidisc       
                                productGroup: productGroup.id,
                                brand: brand._id,
                                createdDate: Date.now(),
                              }
                              ProductCategory.create(createData, function (error, productCategory) {
                                if (error || productCategory == null) {
                                  reject(error)
                                } else {
                                  resolve(productCategory)
                                }
                              })
                            }
                          }))
                        })
                        Promise.all(promises).then(function (productCategories){
                          var ret = {
                            status: 200,
                            message: 'Push products category success.',
                            data: postdata
                          }
                          return res.send(JSON.stringify(ret))          
                        }).catch(err => {
                          console.log(err)
                          var err = {
                            status: 400,
                            messsage: "Something went wrong"
                          }
                          return res.send(JSON.stringify(err))
                        })  
                      }
                    })
                  }
                })
              } else {
                var ret = {
                  status: result.status,
                  message: result.message
                }
                res.send(JSON.stringify(ret))
              }
            })      
          }
        })
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'All fields required'
    }
    return res.send(JSON.stringify(err))
  }
}
exports.push_product_list = function (req, res, next) {
  console.log(req.body)
  var keys = Object.keys(req.body)
  var vals =  Object.keys(Object.values(req.body)[0])
  var tempdt = keys+"["+vals+"]}"
  // console.log("------------")
  // console.log('tempdt',tempdt)

  var postdata = JSON.parse(tempdt)  
  // console.log("------------")
  // console.log('postdata',postdata) 

  // var postdata = req.body

  // var err = {
  //   status: 555,
  //   message: 'test aja',
  //   data: postdata
  // }
  // return res.send(JSON.stringify(err))
  
  if (req.body) {
    Brand.findOne({code: postdata.brand}, function (err, brand) {
      if (err || brand == null) {
        var err = {
          status: 400,
          message: 'Invalid Brand Code'
        }
        return res.send(JSON.stringify(err))
      } else {    
        var unitkerja = postdata.kd_unitkerja 
        Store.findOne({
          brand: brand._id,
          unitKerja: unitkerja
        }, function (err, store) {
          if (err || store == null) {
            var err = {
              status: 400,
              message: 'Invalid Unit Kerja'
            }
            return res.send(JSON.stringify(err))
          } else {
            console.log('brand',brand._id, brand.name)
            console.log('store',store._id, store.name)          
            request(billingAPI + "checkToken/" + store.token, {
              headers: {
                'secretCode': secretCode
              }
            }, function (error, response, body) {
              var result = JSON.parse(body)
              console.log(result)
              if (result.status == "200") {
                ProductCategory.find({brand: brand._id}, {code:1, name:1, productGroup:1}).exec(function (err, productCategoryList) {
                  if (err || productCategoryList == null) {
                    var err = {
                      status: 400,
                      message: 'Error, cannot find product categories list for this brand id'
                    }
                    return res.send(JSON.stringify(err))
                  } else {
                    // console.log('productCategoryList',productCategoryList)
                    Product.deleteMany({brand: brand._id}, function (err, removedProducts) {
                      if (err || removedProducts == null) {
                        var err = {
                          status: 400,
                          message: 'Error, cannot delete products for this brand id'
                        }
                        return res.send(JSON.stringify(err))
                      } else {
                        let promises = []
                        var productsPostData = postdata.item
                        productsPostData.map((dt) => {
                          promises.push(new Promise((resolve, reject) => {
                            var productCategory = productCategoryList.find(pc=>pc.code == dt.prod_group)       
                            if (typeof productCategory == 'undefined'){
                              reject("Category not found for product "+dt.prod_no)
                            } else {
                              var createData = {
                                productCode: dt.prod_no,                          //prod_no
                                name: dt.prod_desc,                               //prod_desc
                                price: parseInt(dt.prod_price),                   //prod_price
                                printerId: dt.kd_printer,                          //kd_printer
                                productCategoryCode: dt.prod_group,                //prod_group  
                                productCategory: productCategory._id,
                                productGroup: productCategory.productGroup,
                                brand: brand._id,
                                createdDate: Date.now(),
                              }
                              Product.create(createData, function (error, product) {
                                if (error || product == null) {
                                  reject(error)
                                } else {
                                  resolve(product)
                                }
                              })                              
                            }
                          }))
                        })
                        Promise.all(promises).then(function (products){
                          var ret = {
                            status: 200,
                            message: 'Push products list success.',
                            data: postdata
                          }
                          return res.send(JSON.stringify(ret))          
                        }).catch(err => {
                          console.log(err)
                          var err = {
                            status: 400,
                            messsage: "Something went wrong",
                            detail: err
                          }
                          return res.send(JSON.stringify(err))
                        })  
                      }
                    })
                  }
                })
              } else {
                var ret = {
                  status: result.status,
                  message: result.message
                }
                res.send(JSON.stringify(ret))
              }
            })      
          }
        })
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'All fields required'
    }
    return res.send(JSON.stringify(err))
  }
}



//login
exports.login_process = function (req, res) {
  res.header('Access-Control-Allow-Origin', "*")
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  var email = req.body.email.toLowerCase()
  var password = req.body.password
  User.authenticate(email, password, function (error, user) {
    if (error || !user || user.length <= 0) {
      var err = {
        status: 400,
        message: 'Invalid Email/Password '
      }
      return res.send(JSON.stringify(err))
    } else {
      var sessionData = {
        token: crypto.createHash('md5').update(req.sessionID + Date.now()).digest('hex'),
        user: user._id,
        createdDate: Date.now(),
      }
      var options = {
        upsert: true,
        new: true,
        setDefaultsOnInsert: true
      }
      Session.findOneAndUpdate({
        user: user._id
      }, sessionData, options, function (error, session) {
        if (!error || session != null) {
          var date_expire = Date.now()
          var date_now = (new Date())
          console.log('passed')
          if (date_expire >= date_now) {
            var data = {
              status: 200,
              message: 'Login Success, please wait..',
              data: session,
              user_data: user
            }
            console.log(data)
            return res.send(JSON.stringify(data))
          } else {
            var err = {
              status: 400,
              message: 'Login Failed, your account is expired..'
            }
            return res.send(JSON.stringify(err))
          }
        }
      })
    }
  })
}

exports.login_pos = function (req, res) {
  res.header('Access-Control-Allow-Origin', "*")
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  var email = req.body.email.toLowerCase()
  var password = req.body.password
  User.authenticate(email, password, function (error, user) {
    if (error || !user || user.length <= 0) {
      var err = {
        status: 400,
        message: 'Invalid Email/Password '
      }
      return res.send(JSON.stringify(err))
    } else {
      var sessionData = {
        token: crypto.createHash('md5').update(req.sessionID + Date.now()).digest('hex'),
        user: user._id,
        createdDate: Date.now(),
      }
      var options = {
        upsert: true,
        new: true,
        setDefaultsOnInsert: true
      }
      Session.findOneAndUpdate({
        user: user._id
      }, sessionData, options, function (error, session) {
        if (!error || session != null) {
          var date_expire = Date.now()
          var date_now = (new Date())
          console.log('passed')
          if (date_expire >= date_now) {
            var data = {
              status: 200,
              message: 'Login Success, please wait..',
              data: session,
              user_data: user
            }
            return res.send(JSON.stringify(data))
          } else {
            var err = {
              status: 400,
              message: 'Login Failed, your account is expired..'
            }
            return res.send(err)
          }
        }
      })
    }
  })
}

//checkToken
exports.checkToken = function (req, res) {
  res.header('Access-Control-Allow-Origin', "*")
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  Session.findOne({
    token: req.body.token
  }).populate('user').exec(function (err, session) {
    if (err) {
      var err = {
        status: 400,
        message: 'Something went wrong',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else if (!session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      var err = {
        status: 200,
        message: 'Token Valid',
        data: session
      }
      return res.send(JSON.stringify(err))
    }
  })
}

//checkStore
exports.checkStore = function (req, res) {
  res.header('Access-Control-Allow-Origin', "*")
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  Store.findOne({
    _id: req.params.storeId
  }).exec(function (err, store) {
    if (err) {
      var err = {
        status: 400,
        message: 'Something went wrong'
      }
      return res.send(JSON.stringify(err))
    } else if (!store) {
      var err = {
        status: 400,
        message: 'Invalid ID'
      }
      return res.send(JSON.stringify(err))
    } else {
      var ret = {
        status: 200,
        message: 'Data Found',
        data: store
      }
      return res.send(JSON.stringify(ret))
    }
  })
}


//dashboard
exports.dashboard = function (req, res) {
  // auth_redirect(req, res)
  Session.findOne({
      token: req.body.token
    }).populate({
      path: 'user',
      populate: {
        path: 'brand store headOffice'
      }
    })
    .exec(function (err, session) {
      if (err) {
        var err = {
          status: 400,
          message: 'Something went wrong',
          data: err
        }
        return res.send(JSON.stringify(err))
      } else if (!session) {
        var err = {
          status: 400,
          message: 'Token Invalid',
          data: session
        }
        return res.send(JSON.stringify(err))
      } else {
        user_data = session.user
        News.findOne({
          brand: session.user.brand
        }).sort({
          createdDate: -1
        }).exec(function (err, news) {
          if (err) {
            var err = {
              status: 400,
              messsage: "Something went wrong"
            }
            return res.send(JSON.stringify(err))
          } else {
            Product.find({
              brand: session.user.brand
            }).populate('createdBy updatedBy').exec(function (err, products) {
              if (err) {
                var err = {
                  status: 400,
                  messsage: "Something went wrong"
                }
                return res.send(JSON.stringify(err))
              } else {
                DeliveryOrder.find({
                    headOffice: user_data.headOffice,
                    isDifference: true
                  })
                  .count(function (err, numofnotif) {
                    if (err) {
                      var err = {
                        status: 400,
                        messsage: "Something went wrong"
                      }
                      return res.send(JSON.stringify(err))
                    } else {
                      var data_in = {
                        status: 200,
                        session: session,
                        news: news,
                        products: products,
                        numofnotif: numofnotif
                      }
                      return res.send(JSON.stringify(data_in))
                    }
                  })
              }
            })
          }
        })
      }
    })
}

//my_profile
exports.my_profile = function (req, res) {
  // auth_redirect(req, res)
  Session.findOne({
    token: req.body.token
  }).populate({
    path: 'user',
    populate: {
      path: 'brand store headOffice'
    }
  }).exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          isDifference: true
        })
        .count(function (err, numofnotif) {
          if (err) {
            var err = {
              status: 400,
              messsage: "Something went wrong"
            }
            return res.send(JSON.stringify(err))
          } else {
            var data_in = {
              status: 200,
              session: session,
              numofnotif: numofnotif
            }
            return res.send(JSON.stringify(data_in))
          }
        })
    }
  })
}


//product
exports.product = function (req, res) {
  // auth_redirect(req, res)

  Product.find({
    brand: req.params.brand
    })
    .exec(function (err, product) {
      if (err) {
        var err = {
          status: 400,
          messsage: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      } else {
        var data_in = {
          status: 200,
          data: product
        }
        return res.send(JSON.stringify(data_in))
      }
    })
}

//product group
exports.product_group = function (req, res) {
  // auth_redirect(req, res)

  ProductGroup.find({
      brand: req.params.brand
    })
    .exec(function (err, product_group) {
      if (err) {
        var err = {
          status: 400,
          messsage: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      } else {
        var data_in = {
          status: 200,
          data: product_group
        }
        return res.send(JSON.stringify(data_in))
      }
    })
}

//product category
exports.product_category = function (req, res) {
  // auth_redirect(req, res)

  ProductCategory.find({
      brand: req.params.brand
    })
    .exec(function (err, product_category) {
      if (err) {
        var err = {
          status: 400,
          messsage: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      } else {
        var data_in = {
          status: 200,
          data: product_category
        }
        return res.send(JSON.stringify(data_in))
      }
    })
}

//brand
exports.brand = function (req, res) {
  // auth_redirect(req, res)

  Brand.find({
    })
    .exec(function (err, brand) {
      if (err) {
        var err = {
          status: 400,
          messsage: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      } else {
        var data_in = {
          status: 200,
          data: brand
        }
        return res.send(JSON.stringify(data_in))
      }
    })
}

//user
exports.user = function (req, res) {
  // auth_redirect(req, res)

  User.find({})
    .exec(function (err, user) {
      if (err) {
        var err = {
          status: 400,
          messsage: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      } else {
        var data_in = {
          status: 200,
          data: user
        }
        return res.send(JSON.stringify(data_in))
      }
    })
}

//store
exports.store = function (req, res) {
  // auth_redirect(req, res)

  Store.find({brand:req.params.brand})
    .exec(function (err, store) {
      if (err) {
        var err = {
          status: 400,
          messsage: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      } else {
        var data_in = {
          status: 200,
          data: store
        }
        return res.send(JSON.stringify(data_in))
      }
    })
}


//edit_password
exports.edit_password = function (req, res) {
  var userData = {
    password: crypto.createHash('md5').update(req.body.password).digest('hex'),
    updatedDate: Date.now(),
    upeatedBy: req.body.id,
  }
  User.findByIdAndUpdate({
    _id: req.body.id
  }, userData, function (error, user) {
    if (error || user == null) {
      var err = {
        status: 400,
        messsage: "Something went wrong"
      }
      return res.send(JSON.stringify(err))
    } else {
      var err = {
        status: 200,
        message: "Change password success"
      }
      return res.send(JSON.stringify(err))
    }
  })
}

exports.change_paid_status = function (req, res) {
  console.log(req.body)
  Session.findOne({
    token: req.body.token
  }).populate({
    path: 'user',
    populate: {
      path: 'brand store headOffice'
    }
  }).exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {

      var doId = req.body.doId
      var paidStatus = req.body.paidStatus
      if (doId) {
        var data_update = {
          paidStatus: (paidStatus == 'paid' ? 'paid' : 'unpaid')
        }
        DeliveryOrder.findByIdAndUpdate({
          _id: doId
        }, data_update, function (error, result) {
          if (error || result == null) {
            var err = {
              status: 400,
              messsage: "Something went wrong"
            }
            return res.send(JSON.stringify(err))
          } else {
            var err = {
              status: 200,
              message: 'Change Paid Status Success'
            }
            return res.send(JSON.stringify(err))
          }
        })
      } else {
        var err = {
          status: 400,
          messsage: "No DO id"
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

//signout
exports.signout = function (req, res) {
  res.header('Access-Control-Allow-Origin', "*")
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  Session.findOneAndRemove({
    token: req.body.key
  }, function (error) {
    if (error) {
      console.log('Login Berhasil')
    } else {
      req.session.destroy(function (err) {
        if (err) {
          var err = {
            status: 400,
            message: 'Logout Failed, please try again..'
          }
          return res.send(JSON.stringify(err))
        } else {
          var err = {
            status: 200,
            message: 'Logout Success, please wait..'
          }
          return res.send(JSON.stringify(err))
        }
      })
    }
  })
}


// POST forgot password
exports.forgot_password_process = function (req, res, next) {
  res.header('Access-Control-Allow-Origin', "*")
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  if (req.body.email) {
    User.findOne({
      email: req.body.email
    }, {}, function (err, user) {
      if (err || !user) {
        var err = {
          status: 200,
          message: 'Forgot password Failed, User not found..'
        }
        return res.send(JSON.stringify(err))
      } else {
        var newpass = random_password()
        User.findOneAndUpdate({
          _id: user._id
        }, {
          password: newpass
        }, function (error, usr) {
          if (error) {
            var err = {
              status: 400,
              messsage: "Something went wrong"
            }
            return res.send(JSON.stringify(err))
          } else {
            var baseUrl = req.headers.host
            var mailOptions = {
              from: 'noreply@isipos.online',
              to: req.body.email,
              subject: 'ISIPOS Online Account Forgot Password',
              html: `Please click this link if you want to reset your password <br>
                <a href="https://isipos.online/forgot_password/reset_password/` + req.body.email + `/` + newpass + `">
                https://isipos.online/forgot_password/reset_password/` + req.body.email + `/` + newpass + `
                </a>
              `
            }
            transporter.sendMail(mailOptions, function (error, info) {
              if (error) {
                var err = {
                  status: 400,
                  messsage: "Something went wrong"
                }
                return res.send(JSON.stringify(err))
              } else {
                var ret = {
                  status: 200,
                  message: 'Email Sent. Please open your email to reset password'
                }
                return res.send(JSON.stringify(ret))
              }
            })
          }
        })
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/forgot_password')
  }
}

//api get delivery order
exports.delivery_order = function (req, res, next) {
  Session.findOne({
    token: req.body.token
  }).populate('user').exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      Brand.findOne({
        _id: session.user.brand
      }, function (err, brand) {
        if (err) {
          var err = {
            status: 400,
            messsage: "Something went wrong"
          }
          return res.send(JSON.stringify(err))
        } else {
          if (session.user.role == 'ho' || session.user.role == 'ho_staff') {
            DeliveryOrder.find({
              headOffice: session.user.headOffice,
              type: 'indirect_store'
            }).sort({
              createdDate: -1
            }).populate({
              path: 'deliveryItem store recipientUser supplier headOffice storeWarehouse storeCasheer createdBy updatedBy',
              populate: {
                path: 'item brand',
                populate: 'unit itemCategory',
              }
            }).exec(function (err, DOData) {
              if (err) {
                var err = {
                  status: 400,
                  messsage: "Something went wrong"
                }
                return res.send(JSON.stringify(err))
              } else {
                DeliveryOrder.find({
                    headOffice: user_data.headOffice,
                    isDifference: true
                  })
                  .count(function (err, numofnotif) {
                    if (err) {
                      var err = {
                        status: 400,
                        messsage: "Something went wrong"
                      }
                      return res.send(JSON.stringify(err))
                    } else {
                      var data_in = {
                        status: 200,
                        session: session,
                        brand: brand,
                        DOData: DOData,
                        numofnotif: numofnotif
                      }
                      return res.send(JSON.stringify(data_in))
                    }
                  })
              }
            })
          }
          if (session.user.role == 'sm_supervisor' || session.user.role == 'sm_warehouse') {
            DeliveryOrder.find({
              store: session.user.store
            }).sort({
              createdDate: -1
            }).populate({
              path: 'deliveryItem store recipientUser supplier headOffice storeWarehouse storeCasheer createdBy updatedBy',
              populate: {
                path: 'item unit brand'
              }
            }).exec(function (err, DOData) {
              if (err) {
                var err = {
                  status: 400,
                  messsage: "Something went wrong"
                }
                return res.send(JSON.stringify(err))
              } else {
                var data_in = {
                  status: 200,
                  session: session,
                  brand: brand,
                  DOData: DOData
                }
                return res.send(JSON.stringify(data_in))
              }
            })
          }
        }
      })
    }
  })
}
// create do
exports.create_do = function (req, res) {
  Session.findOne({
    token: req.body.token
  }).populate({
    path: 'user',
    populate: {
      path: 'brand store headOffice'
    }
  }).exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      user_data = session.user
      var locals = {}

      async.parallel([
        function (callback) {
          Stock.find({
              headOffice: user_data.headOffice._id
            }).populate({
              path: 'item',
              populate: {
                path: 'itemCategory unit'
              }
            }).sort({
              createdDate: -1
            })
            .exec(function (err, stocks) {
              if (err) return callback(err)
              locals.stocks = stocks
              // var activeStock = stocks.map(st => st.item)
              // locals.items = activeStock
              callback()
            })
        },
        function (callback) {
          HeadOffice.findById(user_data.headOffice._id).populate({
              path: 'brands',
              populate: {
                path: 'stores'
              }
            })
            .exec(function (err, HOData) {
              if (err) return callback(err)
              locals.brands = HOData.brands
              callback()
            })
        },
        function (callback) {
          Unit.find({
            type: "lowest"
          }, function (err, units) {
            if (err) return callback(err)
            locals.lowest_unit = units
            callback()
          })
        },
        function (callback) {
          Supplier.find({
              headOffice: user_data.headOffice._id
            })
            .exec(function (err, suppliers) {
              if (err) return callback(err)
              locals.suppliers = suppliers
              callback()
            })
        },
        function (callback) {
          DeliveryOrder.find({
              headOffice: user_data.headOffice,
              isDifference: true
            }).populate({
              path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
              populate: {
                path: 'item stockRef unit brand',
                populate: {
                  path: 'itemCategory'
                }
              }
            }).sort({
              createdDate: -1
            })
            .exec(function (err, DOData) {
              if (err) return callback(err)
              locals.numofnotif = DOData.length
              callback()
            })
        }        
      ], function (err) {
        if (err) {
          var err = {
            status: 400,
            messsage: "Something went wrong"
          }
          return res.send(JSON.stringify(err))
        } else {
          var data_in = {
            status: 200,
            session: session,
            dotype: "indirect_store",
            stocks: locals.stocks,
            brands: locals.brands,
            stores: locals.stores,
            suppliers: locals.suppliers,
            lowest_unit: locals.lowest_unit,
            numofnotif: locals.numofnotif
          }
          return res.send(JSON.stringify(data_in))
        }
      })
    }
  })
}
// create do process
exports.create_do_process = function (req, res, next) {
  Session.findOne({
    token: req.body.token
  }).populate({
    path: 'user',
    populate: {
      path: 'brand store headOffice'
    }
  }).exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      user_data = session.user
      if (req.body.deliveryDate && req.body.driverName && req.body.items){ // && req.body.receiptDate) {
        var isDifference = false
        var itemIds = []
        let promises = []
        var itemsData = JSON.parse(req.body.items)

        itemsData.map((itm, i) => {
          promises.push(new Promise((resolve, reject) => {
            var DOIData = {
              item: itm.ItemId,
              stock: itm.StockId,
              quantitySend: req.body.dotype == 'indirect_store' ? itm.Quantity: (itm.QuantitySent != '-'? itm.QuantitySent : ''),
              unit: itm.UnitId,
              createdDate: Date.now(),
              createdBy: user_data._id,
            }

            var fileName = random_password() + ".png"
            var fileUrl = "./assets/image/" + fileName
            var fullFileURL = "/image/" + fileName
            var buff = decodeBase64Image(itm.ImgBase64)
            fs.writeFile(fileUrl, buff.data, function (err) {
              DOIData.image = fullFileURL
              
              // check if there are any differences
              // if (req.body.dotype != 'indirect_store' && DOIData.quantitySend!=DOIData.quantityReceive && DOIData.quantitySend != '') isDifference=true
              DOItem.create(DOIData, function (error, DOI) {
                if (error || DOI == null) {
                  reject(error)
                } else {
                  var transData = {
                    hoId: user_data.headOffice._id,
                    doItemId: DOI._id,
                    stock: itm.StockId,
                    quantity: itm.Quantity,
                    type: 'out',
                    description: 'Delivery Order',
                    createdDate: Date.now(),
                    createdBy: user_data._id,
                  }
                  StockTransaction.create(transData, function (err, newstock) {
                    if (err || newstock == null) {
                      reject(err)
                    } else {
                      StockTransaction.aggregate([{
                          $match: {
                            hoId: user_data.headOffice._id,
                            stock: ObjectId(itm.StockId)
                          }
                        },
                        {
                          $group: {
                            _id: "$type",
                            sum: {
                              $sum: "$quantity"
                            }
                          }
                        }
                      ], function (err, result) {
                        if (err) {
                          reject(err)
                        } else {
                          var currentStock = result.reduce(function (prev, cur) {
                            return prev + cur.sum
                          }, 0)
                          if (currentStock == 0) {
                            var stockData = {
                              "$inc": {
                                currentStock: parseFloat(itm.Quantity) * -1
                              },
                              updatedDate: Date.now(),
                              updatedBy: user_data._id,
                            }
                          } else {
                            var stockData = {
                              currentStock: currentStock,
                              updatedDate: Date.now(),
                              updatedBy: user_data._id,
                            }
                          }
                          Stock.findByIdAndUpdate(itm.StockId, stockData, function (err, stock) {
                            if (err || stock == null) {
                              reject(err)
                            } else {
                              itemIds.push(DOI._id.valueOf())
                              resolve()
                            }
                          })
                        }
                      })
                    }
                  })
                }
              })
            })
          }))
        })
        Promise.all(promises).then(function (results) {
          //convert base64 to file
          var DOData = {
            doCode: "DO" + ID(),
            headOffice: user_data.headOffice._id,
            store: req.body.storeId,
            deliveryDate: req.body.deliveryDate,
            driverName: req.body.driverName,
            deliveryItem: itemIds,
            type: req.body.dotype,
            isDifference: isDifference,
            recipientUser: user_data._id,
            createdDate: Date.now(),
            createdBy: user_data._id,
          }

          DeliveryOrder.create(DOData, function (err, DO) {
            if (err || DO == null) {
              console.log('wrong')
              var err = {
                status: 400,
                messsage: "Something went wrong"
              }
              return res.send(JSON.stringify(err))
            } else {
              console.log(200)
              var data_in = {
                status: 200,
                session: session,
                message: 'Create restock success'
              }
              return res.send(JSON.stringify(data_in))
            }
          })
        }).catch(err => {
          console.log(err)
          var err = {
            status: 400,
            messsage: "Something went wrong"
          }
          return res.send(JSON.stringify(err))
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}


//api get restock list
exports.restock = function (req, res, next) {
  Session.findOne({
    token: req.body.token
  }).populate('user').exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      user_data = session.user
      Brand.findOne({
        _id: session.user.brand
      }, function (err, brand) {
        if (err) {
          var err = {
            status: 400,
            messsage: "Something went wrong"
          }
          return res.send(JSON.stringify(err))
        } else {
          if (session.user.role == 'ho' || session.user.role == 'ho_staff') {
            DeliveryOrder.find({
              headOffice: session.user.headOffice,
              type: 'direct_ho'
            }).sort({
              createdDate: -1
            }).populate({
              path: 'deliveryItem store recipientUser supplier headOffice storeWarehouse storeCasheer createdBy updatedBy',
              populate: {
                path: 'item unit brand',
                populate: {
                  path: 'itemCategory',
                }
              }
            }).exec(function (err, DOData) {
              if (err) {
                var err = {
                  status: 400,
                  messsage: "Something went wrong"
                }
                return res.send(JSON.stringify(err))
              } else {
                DeliveryOrder.find({
                    headOffice: user_data.headOffice,
                    isDifference: true
                  })
                  .count(function (err, numofnotif) {
                    if (err) {
                      var err = {
                        status: 400,
                        messsage: "Something went wrong"
                      }
                      return res.send(JSON.stringify(err))
                    } else {
                      var data_in = {
                        status: 200,
                        session: session,
                        brand: brand,
                        DOData: DOData,
                        numofnotif: numofnotif
                      }
                      return res.send(JSON.stringify(data_in))
                    }
                  })
              }
            })
          }
          if (session.user.role == 'sm_supervisor' || session.user.role == 'sm_warehouse') {
            DeliveryOrder.find({
              store: session.user.store
            }).sort({
              createdDate: -1
            }).populate({
              path: 'deliveryItem store recipientUser supplier headOffice storeWarehouse storeCasheer createdBy updatedBy',
              populate: {
                path: 'item unit brand'
              }
            }).exec(function (err, DOData) {
              if (err) {
                var err = {
                  status: 400,
                  messsage: "Something went wrong"
                }
                return res.send(JSON.stringify(err))
              } else {
                var data_in = {
                  status: 200,
                  session: session,
                  brand: brand,
                  DOData: DOData
                }
                return res.send(JSON.stringify(data_in))
              }
            })
          }
        }
      })
    }
  })
}


// create restock
exports.create_restock = function (req, res) {
  Session.findOne({
    token: req.body.token
  }).populate({
    path: 'user',
    populate: {
      path: 'brand store headOffice'
    }
  }).exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      user_data = session.user
      var locals = {}

      async.parallel([
        function (callback) {
          Stock.find({
              headOffice: user_data.headOffice
            }).populate({
              path: 'item',
              populate: {
                path: 'itemCategory unit'
              }
            }).sort({
              createdDate: -1
            })
            .exec(function (err, stocks) {
              if (err) return callback(err)
              locals.stocks = stocks
              callback()
            })
        },
        function (callback) {
          HeadOffice.findById(user_data.headOffice).populate({
              path: 'brands',
              populate: {
                path: 'stores'
              }
            })
            .exec(function (err, HOData) {
              if (err) return callback(err)
              locals.brands = HOData.brands
              callback()
            })
        },
        function (callback) {
          Unit.find({
            type: "lowest"
          }, function (err, units) {
            if (err) return callback(err)
            locals.lowest_unit = units
            callback()
          })
        },
        function (callback) {
          Supplier.find({
              headOffice: user_data.headOffice
            })
            .exec(function (err, suppliers) {
              if (err) return callback(err)
              locals.suppliers = suppliers
              callback()
            })
        },
        function (callback) {
          DeliveryOrder.find({
              headOffice: user_data.headOffice,
              isDifference: true
            }).populate({
              path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
              populate: {
                path: 'item stockRef unit brand',
                populate: {
                  path: 'itemCategory'
                }
              }
            }).sort({
              createdDate: -1
            })
            .exec(function (err, DOData) {
              if (err) return callback(err)
              locals.numofnotif = DOData.length
              callback()
            })
        }
      ], function (err) {
        if (err) {
          var err = {
            status: 400,
            messsage: "Something went wrong"
          }
          return res.send(JSON.stringify(err))
        } else {
          var data_in = {
            status: 200,
            session: session,
            dotype: "direct_ho",
            stocks: locals.stocks,
            brands: locals.brands,
            stores: locals.stores,
            suppliers: locals.suppliers,
            lowest_unit: locals.lowest_unit,
            numofnotif: locals.numofnotif
          }
          return res.send(JSON.stringify(data_in))
        }
      })
    }
  })
}


// create restock process
exports.create_restock_process = function (req, res, next) {
  Session.findOne({
    token: req.body.token
  }).populate({
    path: 'user',
    populate: {
      path: 'brand store headOffice'
    }
  }).exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      user_data = session.user
      if (req.body.deliveryDate && req.body.driverName && req.body.items &&
        req.body.supplierId){ // && req.body.receiptDate) {
        var isDifference = false
        var itemIds = []
        let promises = []
        var itemsData = JSON.parse(req.body.items)

        itemsData.map((itm, i) => {
          promises.push(new Promise((resolve, reject) => {
            var DOIData = {
              item: itm.ItemId,
              stock: itm.StockId,
              // quantitySend: (itm.QuantitySent != '-'? itm.QuantitySent : ''),
              quantityReceive: itm.QuantityReceived,
              totalWeight: (itm.TotalWeight != '-' ? itm.TotalWeight : ''),
              unitPrice: (itm.UnitPrice != '-' ? itm.UnitPrice : ''),
              totalPrice: (itm.UnitPrice != '-' ? (itm.TotalWeight != '-' ? itm.UnitPrice * itm.TotalWeight : itm.UnitPrice * itm.QuantityReceived) : ''),
              unit: itm.UnitId,
              createdDate: Date.now(),
              createdBy: user_data._id,
            }


            var fileName = random_password() + ".png"
            var fileUrl = "./assets/image/" + fileName
            var fullFileURL = "/image/" + fileName
            var buff = decodeBase64Image(itm.ImgBase64)
            fs.writeFile(fileUrl, buff.data, function (err) {
              DOIData.image = fullFileURL
              // check if there are any differences
              // if (req.body.dotype != 'indirect_store' && DOIData.quantitySend!=DOIData.quantityReceive && DOIData.quantitySend != '') isDifference=true
              DOItem.create(DOIData, function (error, DOI) {
                if (error || DOI == null) {
                  reject(error)
                } else {
                  var transData = {
                    hoId: user_data.headOffice._id,
                    doItemId: DOI._id,
                    stock: itm.StockId,
                    quantity: itm.QuantityReceived,
                    type: 'in',
                    description: 'Restock items',
                    createdDate: Date.now(),
                    createdBy: user_data._id,
                  }
                  StockTransaction.create(transData, function (err, newstock) {
                    if (err || newstock == null) {
                      reject(err)
                    } else {
                      StockTransaction.aggregate([{
                          $match: {
                            hoId: user_data.headOffice._id,
                            stock: ObjectId(itm.StockId)
                          }
                        },
                        {
                          $group: {
                            _id: "$type",
                            sum: {
                              $sum: "$quantity"
                            }
                          }
                        }
                      ], function (err, result) {
                        if (err) {
                          reject(err)
                        } else {
                          console.log(user_data.headOffice._id, itm.StockId, result)
                          var currentStock = result.reduce(function (prev, cur) {
                            return prev + cur.sum
                          }, 0)
                          if (currentStock == 0) {
                            var stockData = {
                              "$inc": {
                                currentStock: parseFloat(itm.QuantityReceived)
                              },
                              updatedDate: Date.now(),
                              updatedBy: user_data._id,
                            }
                          } else {
                            var stockData = {
                              currentStock: currentStock,
                              updatedDate: Date.now(),
                              updatedBy: user_data._id,
                            }
                          }
                          console.log(stockData)
                          Stock.findByIdAndUpdate(itm.StockId, stockData, function (err, stock) {
                            if (err || stock == null) {
                              reject(err)
                            } else {
                              itemIds.push(DOI._id.valueOf())
                              resolve()
                            }
                          })
                        }
                      })
                    }
                  })
                }
              })
            })
          }))
        })
        Promise.all(promises).then(function (results) {
          //convert base64 to file
          var DOData = {
            doCode: "DO" + ID(),
            headOffice: user_data.headOffice._id,
            deliveryDate: req.body.deliveryDate,
            driverName: req.body.driverName,
            deliveryItem: itemIds,
            type: req.body.dotype,
            isDifference: isDifference,
            recipientUser: user_data._id,
            supplier: req.body.supplierId,
            // receiptDate: req.body.receiptDate,
            imageUrl: "",
            createdDate: Date.now(),
            createdBy: user_data._id,
          }
          DeliveryOrder.create(DOData, function (err, DO) {
            if (err || DO == null) {
              console.log('wrong')
              var err = {
                status: 400,
                messsage: "Something went wrong"
              }
              return res.send(JSON.stringify(err))
            } else {
              console.log(200)
              var data_in = {
                status: 200,
                session: session,
                message: 'Create restock success'
              }
              return res.send(JSON.stringify(data_in))
            }
          })
        }).catch(err => {
          console.log(err)
          var err = {
            status: 400,
            messsage: "Something went wrong"
          }
          return res.send(JSON.stringify(err))
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}



//notification
exports.notification = function (req, res) {
  Session.findOne({
    token: req.body.token
  }).populate({
    path: 'user',
    populate: {
      path: 'brand store headOffice'
    }
  }).exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      user_data = session.user
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          isDifference: true
        }).populate({
          path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) {
            var err = {
              status: 400,
              messsage: "Something went wrong"
            }
            return res.send(JSON.stringify(err))
          } else {
            var data_in = {
              status: 200,
              session: session,
              DOData: DOData,
              numofnotif: DOData.length
            }
            return res.send(JSON.stringify(data_in))
          }
        })
    }
  })
}

//delivery_order_detail
exports.delivery_order_detail = function (req, res) {
  Session.findOne({
    token: req.body.token
  }).populate({
    path: 'user',
    populate: {
      path: 'brand store headOffice'
    }
  }).exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      user_data = session.user
      DeliveryOrder.findOne({
          _id: req.body.doId
        }).populate({
          path: 'headOffice supplier deliveryItem recipientUser store storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) {
            var err = {
              status: 400,
              messsage: "Something went wrong"
            }
            return res.send(JSON.stringify(err))
          } else {
            DeliveryOrder.find({
                headOffice: user_data.headOffice,
                isDifference: true
              })
              .count(function (err, numofnotif) {
                if (err) {
                  var err = {
                    status: 400,
                    messsage: "Something went wrong"
                  }
                  return res.send(JSON.stringify(err))
                } else {
                  var data_in = {
                    status: 200,
                    session: session,
                    DOData: DOData,
                    numofnotif: numofnotif
                  }
                  return res.send(JSON.stringify(data_in))
                }
              })
          }
        })
    }
  })
}

// resolve_difference DO
exports.resolve_difference = function (req, res) {
  Session.findOne({
    token: req.body.token
  }).populate({
    path: 'user',
    populate: {
      path: 'brand store headOffice'
    }
  }).exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      user_data = session.user
      if (req.body.doId) {
        DeliveryOrder.findByIdAndUpdate(req.body.doId, {
          isDifference: false
        }, function (error, DO) {
          if (error) {
            var err = {
              status: 400,
              message: 'Resolve difference delivery order failed'
            }
            return res.send(JSON.stringify(err))
          }
          if (DO == null) {
            var err = {
              status: 400,
              message: 'Delivery order not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var data_in = {
              status: 200,
              message: 'resolve difference delivery order successful',
            }
            return res.send(JSON.stringify(data_in))
          }
        })
      }
    }
  })
}

//stock
exports.stock = function (req, res) {
  Session.findOne({
    token: req.body.token
  }).populate('user').exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      var query = {
        headOffice: session.user.headOffice
      }
      if(session.user.role != "ho" && session.user.role != "ho_staff") query.store = session.user.store
      Stock.find(query).populate({
          path: 'item',
          populate: {
            path: 'itemCategory unit'
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, stocks) {
          console.log(stocks)
          if (err || !stocks) {
            var err = {
              status: 400,
              messsage: "Something went wrong"
            }
            return res.send(JSON.stringify(err))
          } else {
            var activeStockIds = stocks.map(st => st.item)
            Item.find({
                _id: {
                  $nin: activeStockIds
                }
              }).populate({
                path: 'itemCategory unit',
                populate: {
                  path: 'highestUnit'
                }
              })
              .exec(function (err, items) {
                if (err || !items) {
                  var err = {
                    status: 400,
                    messsage: "Something went wrong"
                  }
                  return res.send(JSON.stringify(err))
                } else {
                  Unit.find({
                      type: "lowest"
                    }).populate('highestUnit')
                    .exec(function (err, units) {
                      if (err || !units) {
                        var err = {
                          status: 400,
                          messsage: "Something went wrong"
                        }
                        return res.send(JSON.stringify(err))
                      } else {
                        DeliveryOrder.find({
                            headOffice: session.user.headOffice,
                            isDifference: true
                          })
                          .count(function (err, numofnotif) {
                            if (err) {
                              var err = {
                                status: 400,
                                messsage: "Something went wrong"
                              }
                              return res.send(JSON.stringify(err))
                            } else {
                              var data_in = {
                                status: 200,
                                session: session,
                                stocks: stocks,
                                items: items,
                                units: units,
                                numofnotif: numofnotif
                              }
                              return res.send(JSON.stringify(data_in))
                            }
                          })
                      }
                    })
                }
              })
          }
        })
    }
  })
}

//receive items delivery order manual
exports.receive_do_manual = function (req, res) {
  Session.findOne({
    token: req.body.token
  }).populate('user').exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      var code = req.body.do_code
      DeliveryOrder.findOne({
          doCode: code,
          store: session.user.store
        })
        .populate({
          path: 'deliveryItem headOffice supplier store ',
          populate: {
            path: 'item unit brand'
          }
        }).exec(function (err, DOData) {
          if (err || DOData == null) {
            var err = {
              status: 400,
              message: 'Cannot find Delivery Code'
            }
            console.log(err)
            return res.send(JSON.stringify(err))
          } else {
            if (DOData.type == 'indirect_store') {
              if (session.user.store.toString() == DOData.store._id.toString()) {
                //only Store Warehouse can see this page
                if (session.user.role == 'sm_warehouse') {
                  DeliveryOrder.find({
                      headOffice: user_data.headOffice,
                      isDifference: true
                    })
                    .count(function (err, numofnotif) {
                      if (err) {
                        var err = {
                          status: 400,
                          messsage: "Something went wrong"
                        }
                        return res.send(JSON.stringify(err))
                      } else {
                        var data_in = {
                          status: 200,
                          session: session,
                          DOData: DOData,
                          store: DOData.store,
                          numofnotif: numofnotif
                        }
                        return res.send(JSON.stringify(data_in))
                      }
                    })
                } else {
                  var err = {
                    status: 400,
                    message: 'You are not allowed to access this page'
                  }
                  return res.send(JSON.stringify(err))
                }
              } else {
                var err = {
                  status: 400,
                  message: 'This Delivery Order isn\'t for your store'
                }
                return res.send(JSON.stringify(err))
              }
            } else {
              var err = {
                status: 400,
                message: 'This not indirect Delivery Order'
              }
              return res.send(JSON.stringify(err))
            }
          }
        })
    }
  })
}

//spoil
exports.spoil_stock_process = function (req, res) {
  Session.findOne({
    token: req.body.token
  }).populate('user').exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      var user_data = session.user
      var StockData = {
        "$inc": {
          currentStock: parseFloat(req.body.quantity) * -1
        }
      }
      Stock.findOne({
        _id: req.body.stock_id
      }, function (err, stock) {
        if (err || stock == null) {
          var err = {
            status: 400,
            message: "Spoil Item Failed"
          }
          return res.send(JSON.stringify(err))
        } else {
          if (req.body.quantity <= stock.currentStock) {
            Stock.findByIdAndUpdate(req.body.stock_id, StockData, function (err, stock) {
              if (err || stock == null) {
                var err = {
                  status: 400,
                  message: "Spoil Item Failed"
                }
                return res.send(JSON.stringify(err))
              } else {
                console.log(user_data)
                var transData = {
                  hoId: user_data.headOffice,
                  stock: stock._id,
                  quantity: parseFloat(req.body.quantity) * -1,
                  type: 'out',
                  description: 'Spoil Item : ' + req.body.reason,
                  createdDate: Date.now(),
                  createdBy: user_data._id,
                }
                StockTransaction.create(transData, function (error, trans) {
                  if (error || trans == null) {
                    var err = {
                      status: 400,
                      message: "Spoil Item Failed"
                    }
                    return res.send(JSON.stringify(err))
                  } else {
                    var data_in = {
                      status: 200,
                      message: 'Spoil item successful',
                    }
                    return res.send(JSON.stringify(data_in))
                  }
                })
              }
            })
          } else {
            var err = {
              status: 400,
              message: "Spoil quantity can't exceed the current quantity"
            }
            return res.send(JSON.stringify(err))
          }
        }
      })

    }
  })
}


// POST API SM return doitem process
exports.return_doitem_process = function (req, res, next) {
  Session.findOne({
    token: req.body.token
  }).populate('user').exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      var user_data = session.user
      if (req.body.doi_id && req.body.quantity && req.body.notes) {
        var DOItemData = {
          quantityReturn: req.body.quantity,
          notesReturn: req.body.notes
        }
        DOItem.findOneAndUpdate({
            _id: req.body.doi_id
          }, DOItemData).populate('unit item')
          .exec(function (error, doitem) {
            if (err) {
              var err = {
                status: 400,
                message: err
              }
              return res.send(JSON.stringify(err))
            }
            if (doitem == null) {
              var err = {
                status: 400,
                message: 'DO Item not found'
              }
              return res.send(JSON.stringify(err))
            } else {
              //Increasing Stock on HO
              var StockData = {
                "$inc": {
                  currentStock: parseFloat(req.body.quantity)
                }
              }
              Stock.findByIdAndUpdate(doitem.stock, StockData, function (error, stock) {
                if (error || stock == null) {
                  var err = {
                    status: 400,
                    message: 'Return DO item failed'
                  }
                  return res.send(JSON.stringify(err))
                } else {
                  var stockData = {
                    hoId: null,
                    doItemId: doitem.stock,
                    quantity: req.body.quantity,
                    stock: stock._id,
                    type: "in",
                    description: 'Return DO item',
                    createdDate: Date.now(),
                    createdBy: user_data._id,
                  }
                  StockTransaction.create(stockData, function (err, stock) {
                    if (error || stock == null) {
                      console.log(error)
                      var err = {
                        status: 400,
                        message: 'Return DO item failed'
                      }
                      return res.send(JSON.stringify(err))
                    } else {
                      //Search Stock to Increasing Current Stock on Store
                      Stock.findOne({
                        item: doitem.item._id,
                        store: user_data.store
                      }, function (err, result_stock) {
                        if (err || result_stock == null) {
                          var err = {
                            status: 400,
                            message: 'Return DO item failed'
                          }
                          return res.send(JSON.stringify(err))
                        } else {
                          //Increasing Stock
                          var StockData = {
                            "$inc": {
                              currentStock: parseFloat(req.body.quantity * -1)
                            }
                          }
                          Stock.findByIdAndUpdate(result_stock._id, StockData, function (error, update_stock) {
                            if (error || update_stock == null) {
                              var err = {
                                status: 400,
                                message: 'Return DO item failed'
                              }
                              return res.send(JSON.stringify(err))
                            } else {
                              var stockData = {
                                hoId: null,
                                doItemId: doitem.stock,
                                quantity: req.body.quantity,
                                stock: result_stock._id,
                                type: "out",
                                description: 'Return DO item',
                                createdDate: Date.now(),
                                createdBy: user_data._id,
                              }
                              StockTransaction.create(stockData, function (err, trans_stock) {
                                if (err || trans_stock == null) {
                                  return res.send(JSON.stringify(err))
                                } else {
                                  var ret = {
                                    status: 200,
                                    message: 'Return DO item success'
                                  }
                                  return res.send(JSON.stringify(ret))
                                }
                              })
                            }
                          })
                        }
                      })
                    }
                  })
                }
              })
            }
          })
      } else {
        var err = {
          status: 400,
          message: 'All data required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}



// GET API HO adjust doitem process
exports.adjust_doitem_process = function (req, res, next) {
  Session.findOne({
    token: req.body.token
  }).populate('user').exec(function (err, session) {
    if (err || !session) {
      var err = {
        status: 400,
        message: 'Token Invalid',
        data: session
      }
      return res.send(JSON.stringify(err))
    } else {
      var user_data = session.user
      if (req.body.doitemid && req.body.quantity && req.body.type) {
        //find Adjust before (if do was adjusted before)
        var transData = {
          hoId: user_data.headOffice,
          doItemId: req.body.doitemid,
          stock: req.body.stockid,
          quantity: req.body.quantity,
          type: (req.body.type == "IN" ? "in" : "out"),
          description: 'Re-adjustment DO item',
          createdDate: Date.now(),
          createdBy: user_data._id,
        }
        console.log(transData)
        StockTransaction.create(transData, function (error, newstock) {
          if (error || newstock == null) {
            console.log(error)
            var err = {
              status: 400,
              message: "Adjust Failed"
            }
            return res.send(JSON.stringify(err))
          } else {
            DOItem.findById(req.body.doitemid, function (error, DO) {
              if (error) {
                console.log(error)
                var err = {
                  status: 400,
                  message: "Adjust Failed"
                }
                return res.send(JSON.stringify(err))
              } else if (DO.quantityAdjustment != null) {
                var stockData = {
                  "$inc": {
                    currentStock: parseFloat(DO.adjustmentType == 'Plus' ? DO.quantityAdjustment * -1 : DO.quantityAdjustment)
                  }
                }
                // console.log(stockData)
                Stock.findByIdAndUpdate(req.body.stockid, stockData, function (error, item) {
                  if (error || item == null) {
                    console.log(error)
                    var err = {
                      status: 400,
                      message: "Adjust Failed"
                    }
                    return res.send(JSON.stringify(err))
                  } else {
                    var DOItemData = {
                      quantityAdjustment: req.body.quantity,
                      adjustmentType: req.body.type == 'out' ? 'Minus' : 'Plus'
                    }
                    DOItem.findOneAndUpdate({
                        _id: req.body.doitemid
                      }, DOItemData).populate('unit item')
                      .exec(function (error, item) {
                        if (error) {
                          console.log(error)
                          var err = {
                            status: 400,
                            message: "Adjust Failed"
                          }
                          console.log('error disini 1')
                          return res.send(JSON.stringify(err))
                        }
                        if (item == null) {
                          var err = {
                            status: 400,
                            message: 'DO Item not found'
                          }
                          console.log('error disini 2')
                          return res.send(JSON.stringify(err))
                        } else {
                          var stockData = {
                            "$inc": {
                              currentStock: parseFloat(req.body.type == 'out' ? req.body.quantity * -1 : req.body.quantity)
                            }
                          }
                          Stock.findByIdAndUpdate(req.body.stockid, stockData, function (error, item) {
                            if (error || item == null) {
                              console.log(error)
                              var err = {
                                status: 400,
                                message: "Adjust Failed"
                              }
                              console.log('error disini 3')
                              return res.send(JSON.stringify(err))
                            } else {
                              var ret = {
                                status: 200,
                                message: "Adjust Succesful"
                              }
                              return res.send(JSON.stringify(ret))
                            }
                          })
                        }
                      })
                  }
                })
              } else {
                var transData = {
                  hoId: user_data.headOffice,
                  doItemId: req.body.doitemid,
                  stock: req.body.stockid,
                  quantity: req.body.quantity,
                  type: (req.body.type == "IN" ? "in" : "out"),
                  description: 'Adjustment DO item',
                  createdDate: Date.now(),
                  createdBy: user_data._id,
                }
                console.log(transData)
                StockTransaction.create(transData, function (err, newstock) {
                  if (err || newstock == null) {
                    console.log(error)
                    var err = {
                      status: 400,
                      message: "Adjust Failed"
                    }
                    return res.send(JSON.stringify(err))
                  } else {
                    var DOItemData = {
                      quantityAdjustment: req.body.quantity,
                      adjustmentType: req.body.type == 'out' ? 'Minus' : 'Plus'
                    }
                    DOItem.findOneAndUpdate({
                        _id: req.body.doitemid
                      }, DOItemData).populate('unit item')
                      .exec(function (error, item) {
                        if (err) {
                          var err = {
                            status: 400,
                            message: "Adjust Failed"
                          }
                          return res.send(JSON.stringify(err))
                        }
                        if (item == null) {
                          var err = {
                            status: 400,
                            message: 'DO Item not found'
                          }
                          return res.send(JSON.stringify(err))
                        } else {
                          var stockData = {
                            "$inc": {
                              currentStock: parseFloat(req.body.type == 'out' ? req.body.quantity * -1 : req.body.quantity)
                            }
                          }
                          Stock.findByIdAndUpdate(req.body.stockid, stockData, function (error, item) {
                            if (error || item == null) {
                              var err = {
                                status: 400,
                                message: "Adjust Failed"
                              }
                              return res.send(JSON.stringify(err))
                            } else {
                              var ret = {
                                status: 200,
                                message: "Adjust Succesful"
                              }
                              return res.send(JSON.stringify(ret))
                            }
                          })
                        }
                      })
                  }
                })
              }
            })
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All data required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}


//POST Receive Item
exports.receive_item_process = function (req, res, next) {
  let promises = []
  var updateItems = JSON.parse(req.body.updateItems)
  var isDifference = false
  Session.findOne({
    token: req.body.token
  }).populate('user').exec(function (err, session) {
    if (err) {
      console.log(err)
      var err = {
        status: 400,
        message: 'Delivery Order Update failed'
      }
      return res.send(JSON.stringify(err))
    } else {
      var user_data = session.user
      //convert base64 to file
      //Update Delivery Order Data
      var dataUpdate = {
        recipientUser: user_data._id,
        receiptDate: req.body.receiptDate,
        receiptTime: req.body.receiptTime,
        imageUrl: '',
        updatedDate: Date.now(),
        updatedBy: user_data._id,
      }
      DeliveryOrder.findOneAndUpdate({
        _id: req.body.id
      }, dataUpdate, function (error, result) {
        if (error || result == null) {
          console.log(error)
          var err = {
            status: 400,
            message: "Receive Item"
          }
          return res.send(JSON.stringify(err))
        } else {
          //Update DeliveryOrderItem
          updateItems.map((ui, i) => {
            promises.push(new Promise((resolve, reject) => {
              if (ui.qtySend != ui.qty) {
                isDifference = true
                //Update Delivery Order Data
                var dataUpdate = {
                  isDifference: true,
                  updatedDate: Date.now(),
                  updatedBy: user_data._id,
                }
                DeliveryOrder.findOneAndUpdate({
                  _id: req.body.id
                }, dataUpdate, function (error, result) {})
              }
              //Cek Stock Store
              Stock.findOne({
                item: ui.itemid,
                store: user_data.store
              }, function (error, stock_find) {
                if (error || stock_find == null) {
                  reject(error)
                  //Create Stock
                  var stockData = {
                    item: ui.itemid,
                    ownerType: 'store',
                    headOffice: null,
                    store: user_data.store,
                    initialStock: 0,
                    currentStock: ui.qty,
                    createdDate: Date.now(),
                    createdBy: user_data._id,
                  }
                  Stock.create(stockData, function (error, stock) {
                    if (error || stock == null) {
                      reject(error)
                    } else {
                      //Create initial stock 0
                      var transData = {
                        hoId: null,
                        stock: stock._id,
                        quantity: 0,
                        type: 'initial',
                        description: 'Create initial stock',
                        createdDate: Date.now(),
                        createdBy: user_data._id,
                      }
                      StockTransaction.create(transData, function (error, trans) {
                        if (error || trans == null) {
                          reject(error)
                        } else {
                          //Create In Stock
                          var transData = {
                            hoId: null,
                            stock: stock._id,
                            quantity: ui.qty,
                            type: 'in',
                            description: 'Delivery Order',
                            createdDate: Date.now(),
                            createdBy: user_data._id,
                          }
                          StockTransaction.create(transData, function (error, trans) {
                            if (error || trans == null) {
                              reject(error)
                            } else {
                              resolve(isDifference)
                            }
                          })
                        }
                      })
                    }
                  })
                } else {
                  //Update stock on Store
                  var currentStock = stock_find.currentStock
                  var dataUpdate = {
                    currentStock: parseFloat(currentStock) + parseFloat(ui.qty),
                    updatedDate: Date.now(),
                    updatedBy: user_data._id
                  }
                  Stock.findOneAndUpdate({
                    _id: stock_find._id
                  }, dataUpdate, function (error, result) {
                    if (error || result == null) {
                      reject(error)
                    } else {
                      resolve(isDifference)
                    }
                  })
                }
                //Update DOItem on HO
                var fileName = random_password() + ".png"
                var fileUrl = "./assets/image/" + fileName
                var fullFileURL = "/image/" + fileName
                var buff = new Buffer(ui.img.replace(/^data:image\/(png|gif|jpeg)base64,/, ''), 'base64')

                fs.writeFile(fileUrl, buff, function (err) {
                  var dataUpdate = {
                    image: fullFileURL,
                    quantityReceive: ui.qty,
                    updatedDate: Date.now(),
                    updatedBy: user_data._id,
                  }
                  DOItem.findOneAndUpdate({
                    _id: ui.itemOrderId
                  }, dataUpdate, function (error, result) {
                    if (error) {
                      reject(error)
                    }
                  })
                })
              })
            }))
          })

          Promise.all(promises).then(function (isDiff) {
            DeliveryOrder.findOne({
              _id: req.body.id
            }, function (err, deliveryOrder) {
              if (err) {
                console.log("post failed")
                req.flash('info', 'Delivery Order Update Failed')
                req.flash('info_type', 'error')
                res.redirect('/sm/delivery_order')
              } else {
                if (deliveryOrder.isDifference) {
                  var config = {
                    headers: {
                      "Authorization": "key=AAAAprZkeKw:APA91bFr-VGo60kWu4aGpn4xE72aeNSylttzeikRjMaFmahkMOA5zgIuktslcNJDlt2KXW-1bKJc7Ph3QNloLWUJsnuBLV7G2FnhUIXwifng9NX7wkGI1XosQ2Uq6b-y-PdUszsfPSaj",
                      "Content-Type": "application/json"
                    }
                  }
                  const data = {
                    "to": "/topics/general",
                    "notification": {
                      "body": "Difference on Delivery Order " + ("#DO" + req.body.id.toString().slice(-5)).toUpperCase(),
                      "title": "Difference Alert",
                      "content_available": true,
                      "priority": "high"
                    },
                    "data": {
                      "body": "Difference on Delivery Order " + ("#DO" + req.body.id.toString().slice(-5)).toUpperCase(),
                      "title": "Difference Alert",
                      "content_available": true,
                      "priority": "high"
                    }
                  }
                  axios.post('https://fcm.googleapis.com/fcm/send', data, config)
                    .then((res) => {
                      console.log("done")
                      var err = { status:200, message:'Delivery Order Update success'}
                      return res.send(JSON.stringify(err))
                    })
                    .catch((err) => {
                      console.log("post failed")
                      var err = { status:200, message:"Delivery Order Update Failed"}
                      return res.send(JSON.stringify(err))
                    })
                } else {
                  console.log('here')
                  var err = {
                    status: 200,
                    message: 'Delivery Order Update success'
                  }
                  return res.send(JSON.stringify(err))
                }
              }
            })
          }).catch(err => {
            console.log(err)
            var err = {
              status: 400,
              message: "Delivery Order Update Failed"
            }
            return res.send(JSON.stringify(err))
          })
        }
      })
    }
  })
}

exports.read_table = function (req, res, next) {
  Table.find({
      brand: req.params.brandId,
      store: req.params.storeId
    })
    .exec(function (error, result) {
      if (err) {
        var err = {
          status: 400,
          message: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      }
      if (!result || result.length == 0) {
        var err = {
          status: 400,
          message: 'Data not found'
        }
        return res.send(JSON.stringify(err))
      } else {
        var ret = {
          status: 200,
          data: result
        }
        return res.send(ret)
      }
    })
}

exports.read_payment = function (req, res, next) {
  Payment.find({
      brand: req.params.brandId
    })
    .exec(function (error, result) {
      if (err) {
        var err = {
          status: 400,
          message: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      }
      if (!result || result.length == 0) {
        var err = {
          status: 400,
          message: 'Data not found'
        }
        return res.send(JSON.stringify(err))
      } else {
        var ret = {
          status: 200,
          data: result
        }
        return res.send(ret)
      }
    })
}

exports.read_product_category = function (req, res, next) {
  ProductCategory.find({
      brand: req.params.brandId
    })
    .exec(function (error, result) {
      if (err) {
        var err = {
          status: 400,
          message: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      }
      if (!result || result.length == 0) {
        var err = {
          status: 400,
          message: 'Data not found'
        }
        return res.send(JSON.stringify(err))
      } else {
        var ret = {
          status: 200,
          data: result
        }
        return res.send(ret)
      }
    })
}

exports.read_product = function (req, res, next) {
  Product.find({
      brand: req.params.brandId
    }).populate('productcategory productgroup')
    .exec(function (error, result) {
      if (err) {
        var err = {
          status: 400,
          message: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      }
      if (!result || result.length == 0) {
        var err = {
          status: 400,
          message: 'Data not found'
        }
        return res.send(JSON.stringify(err))
      } else {
        var ret = {
          status: 200,
          data: result
        }
        return res.send(ret)
      }
    })
}

exports.read_product_group = function (req, res, next) {
  ProductGroup.find({
      brand: req.params.brandId
    })
    .exec(function (error, result) {
      if (err) {
        var err = {
          status: 400,
          message: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      }
      if (!result || result.length == 0) {
        var err = {
          status: 400,
          message: 'Data not found'
        }
        return res.send(JSON.stringify(err))
      } else {
        var ret = {
          status: 200,
          data: result
        }
        return res.send(ret)
      }
    })
}

exports.read_discount = function (req, res, next) {
  Discount.find({
      brand: req.params.brandId
    }).populate('product productgroup productcategory payment')
    .exec(function (error, result) {
      if (err) {
        var err = {
          status: 400,
          message: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      }
      if (!result || result.length == 0) {
        var err = {
          status: 400,
          message: 'Data not found'
        }
        return res.send(JSON.stringify(err))
      } else {
        var ret = {
          status: 200,
          data: result
        }
        return res.send(ret)
      }
    })
}

function random_password() {
  var pass = (Math.random().toString(36).substr(2)).toUpperCase()
  pass = crypto.createHash('md5').update(pass).digest('hex')
  return pass
}

function decodeBase64Image(dataString) {
  var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
  var response = {};
  if (matches.length !== 3) {
    return new Error('Invalid input string');
  }
  response.type = matches[1];
  response.data = new Buffer(matches[2], 'base64');
  return response;
}
