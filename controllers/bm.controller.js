const User = require('../models/user.model')
const News = require('../models/news.model')
const Session = require('../models/session.model')
const Product = require('../models/product.model')
const DeliveryOrder = require('../models/delivery_order.model')
const DOItem = require('../models/do_item.model')
const Brand = require('../models/brand.model')
const Payment = require('../models/payment.model')
const Table = require('../models/table.model')
const ProductCategory = require('../models/product_category.model')
const ProductGroup = require('../models/product_group.model')
const ProductUpdate = require('../models/product_update.model')
const Discount = require('../models/discount.model')
const Store = require('../models/store.model')
const Stock = require('../models/stock.model')
const StockTransaction = require('../models/stock_transaction.model')
const Unit = require('../models/unit.model')
const Item = require('../models/item.model')
const Revenue = require('../models/revenue.model')
const moment = require('moment')
var crypto = require('crypto')
var dateFormat = require('dateformat')
var async = require('async')
var QRCode = require('qrcode')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

//Dashboard
exports.dashboard = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  async.parallel([
       //Load News Data
       function(callback) {
            News.find({},null,{limit:1}).sort({datecreated: -1}).exec(function(err,news){
               if (err) return callback(err)
               locals.news = news[0]
               callback()
           })
       },
       //Load Brand Data
       function(callback) {
            Brand.findOne({_id:user_data.brand},function(err,brand){
              if (err) return callback(err)
               locals.brand = brand
               callback()
           })
       },
       //Load total Products Data
       function(callback) {
            Product.count({brand:user_data.brand},function(err,count){
              if (err) return callback(err)
               locals.total_products = count
               callback()
           })
       },
       //Load total Store Data
       function(callback) {
            Store.count({brand:user_data.brand},function(err,count){
              if (err) return callback(err)
               locals.total_stores = count
               callback()
           })
       },
       //Get revenue today
       function(callback) {
         var now = (new Date())
         var startOfToday = new Date(now.getFullYear(), now.getMonth(), now.getDate())
         Revenue.find({createdDate:{$gte: startOfToday}, brand:user_data.brand}).populate('store').sort({createdDate: -1}).exec(function(err,result){
            if (err) return callback(err)
            // var income = 0
            // var outcome = 0
            var total = 0
            result.forEach(function(item){
              total+=item.total
            })
            locals.total_daily_revenue = total
            locals.revenue = result
            callback()
        })
      },
      //Get Revenue This Month
      function(callback){
        var start = moment().format("YYYY-MM-01")
        var end = moment().format("YYYY-MM-") + moment().daysInMonth()
        Revenue.find({createdDate:{$gte: start, $lt: end}, brand:user_data.brand},null).sort({createdDate: -1}).exec(function(err,result){
           if (err) return callback(err)
           // var income = 0
           // var outcome = 0
           var total = 0
           result.forEach(function(item){
             total+=item.total
           })
           locals.total_monthly_revenue = total
           callback()
       })
      }

   ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
       if (err) return next(err) //If an error occurred, we let express handle it by calling the `next` function
       //Here `locals` will be an object with `user` and `posts` keys
       //Example: `locals = {user: ..., posts: [...]}`
       res.render('main', {
         title : appName+" | "+"Dashboard",
         user_data : user_data,
         news : locals.news,
         brand : locals.brand,
         total_daily_revenue : locals.total_daily_revenue,
         total_monthly_revenue : locals.total_monthly_revenue,
         total_products : locals.total_products,
         total_stores : locals.total_stores,
         revenue : locals.revenue
       })
   })
}

//change password
exports.change_password_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
      if (req.body.password != '' && req.body.conf_password != '' ){
        if( req.body.password == req.body.conf_password){
        var userData={
          password : crypto.createHash('md5').update(req.body.password).digest('hex'),
          updatedDate: Date.now(),
          upeatedBy: user_data._id,
        }
        User.findByIdAndUpdate({_id:user_data._id}, userData, function (error, user) {
          if (error || user==null) {
            var err = { status:400, message:error }
            return res.send(JSON.stringify(err))
          } else {
            req.flash('info', 'Change password success')
            req.flash('info_type', 'success')
            res.redirect('/bm/profile')
          }
        })
      } else {
        req.flash('info', 'Password didn\'t match')
        req.flash('info_type', 'danger')
        res.redirect('/bm/profile')
      }
      } else {
        req.flash('info', 'All fields required')
        req.flash('info_type', 'danger')
        res.redirect('/bm/profile')
      }
}

//function auth redirect
function auth_redirect(req, res){
  sess = req.session
  if(sess.user_login) return true
  else res.redirect('/signin')
}

//profile
exports.profile = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined'){
        messages = res.locals.info
        messages_type = res.locals.info_type
    }
  async.parallel([
       //Load News Data
       function(callback) {
            News.find({},null,{limit:1}).sort({datecreated: -1}).exec(function(err,news){
               if (err) return callback(err)
               locals.news = news[0]
               callback()
           })
       },
       //Load Brand Data
       function(callback) {
            Brand.findOne({_id:user_data.brand},function(err,brand){
              if (err) return callback(err)
               locals.brand = brand
               callback()
           })
       }

   ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
       if (err) console.log('Error') //If an error occurred, we let express handle it by calling the `next` function
       //Here `locals` will be an object with `user` and `posts` keys
       //Example: `locals = {user: ..., posts: [...]}`
       res.render('profile', {
         title : appName+" | "+"Profile",
         user_data : user_data,
         brand : locals.brand,
         messages : messages,
         messages_type : messages_type
       })
   })
}


// POST bm update_profile
exports.update_profile = function (req, res, next) {
  Session.validate(req.headers.key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.name != '' && req.body.email != '' ){
        var userData={
          name: req.body.name,
          email : req.body.email,
          imageProfile: "none",
          updatedDate: Date.now(),
          upeatedBy: user._id,
        }
        User.create(userData, function (error, user) {
          if (error || user==null) {
            var err = { status:400, message:error }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              message: 'Update profile success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'All fields required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}


//setting
exports.setting = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined'){
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load Brand Data
    function(callback) {
      Brand.findOne({_id:user_data.brand},function(err,brand){
        if (err) return callback(err)
         locals.brand = brand
         callback()
     })
    }
  ], function(err) {
      if (err) console.log('Error')
      res.render('setting', {
        title : appName+" | "+"Setting",
        user_data : user_data,
        brand : locals.brand,
        messages : messages,
        messages_type : messages_type
      })
  })
}


// POST sm update logo
exports.update_logo_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var file_url = ""
  var file_url = req.file.fieldname!=undefined?('/image/' +  res.req.file.filename):''
  if(file_url!='') {
    var brandData={
      imageUrlLogo: file_url,
      updatedDate: Date.now(),
      updatedBy: user_data._id,
    }
    Brand.findOneAndUpdate({_id:user_data.brand}, brandData, function (error, brand) {
      if (error || brand==null) {
        req.flash('info', 'Update Logo Failed')
        req.flash('info_type', 'danger')
        res.redirect('/bm/setting')
      } else {
        req.flash('info', 'Update Logo Success')
        req.flash('info_type', 'success')
        res.redirect('/bm/setting')
      }
    })
  } else {
    req.flash('info', 'Image file required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/setting')
  }
}

// POST sm update background
exports.update_background_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var file_url = ""
  var file_url = req.file.fieldname!=undefined?('/image/' +  res.req.file.filename):''
  if(file_url!='') {
    var brandData={
      imageUrlBackground: file_url,
      updatedDate: Date.now(),
      updatedBy: user_data._id,
    }
    Brand.findOneAndUpdate({_id:user_data.brand}, brandData, function (error, brand) {
      if (error || brand==null) {
        req.flash('info', 'Update Background Failed')
        req.flash('info_type', 'danger')
        res.redirect('/bm/setting')
      } else {
        req.flash('info', 'Update Background Success')
        req.flash('info_type', 'success')
        res.redirect('/bm/setting')
      }
    })
  } else {
    req.flash('info', 'Image file required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/setting')
  }
}


//product
exports.product = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined'){
        messages = res.locals.info
        messages_type = res.locals.info_type
    }
  async.parallel([
        function(callback) {
            Product.find({brand:user_data.brand}).sort({createdDate : -1}).populate('brand productCategory productGroup createdBy updatedBy')
            .exec(function (err,products) {
              if (err) return callback(err)
              locals.products = products
              callback()
          })
        },
        function(callback){
          Brand.findOne({_id:user_data.brand},function(err,brand){
            if (err) return callback(err)
            locals.brand = brand
            callback()
          })
        },
        function(callback){
          ProductCategory.find({brand:user_data.brand}, function(err, productcategory){
            if(err) return callback(err)
            locals.productcategory = productcategory
            callback()
          })
        },
        function(callback){
          ProductGroup.find({brand:user_data.brand}, function(err, productgroup){
            if(err) return callback(err)
            locals.productgroup = productgroup
            callback()
          })
        }
   ], function(err) {
       if (err) return next(err)
       res.render('product', {
         title : appName+" | "+"Products",
         user_data : user_data,
         products : locals.products,
         brand : locals.brand,
         lowest_unit : locals.lowest_unit,
         product_category : locals.productcategory,
         product_group : locals.productgroup,
         messages : messages,
         messages_type : messages_type
       })
   })
}
exports.create_product_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if ( req.body.name && req.body.description && req.body.productCode && req.body.price && req.body.productCategory &&
    req.body.printerId && req.body.validStartDate){
      ProductCategory.findById(req.body.productCategory, {code:1, name:1, productGroup:1}).exec(function (err, productCategory) {
      if (err || productCategory == null) {
        req.flash('info', 'Create product failed')
        req.flash('info_type', 'danger')
        res.redirect('/bm/product')
      } else {
        var file_url = req.file?('/image/' +  res.req.file.filename):''
        // if(!Array.isArray(req.body.productcategory)) req.body.productcategory = [req.body.productcategory]
        var productData={
          name: req.body.name,
          description: req.body.description,
          productCode: req.body.productCode,
          price: req.body.price,
          productCategory: productCategory._id,
          productGroup: productCategory.productGroup,
          imageProduct: file_url,
          group: req.body.group,
          printerId: req.body.printerId,
          validStartDate: req.body.validStartDate,
          brand: user_data.brand,
          createdDate: Date.now(),
          createdBy: user_data._id,
        }
        Product.create(productData, function (error, product) {
          if (error || product==null) {
            req.flash('info', 'Create product failed')
            req.flash('info_type', 'danger')
            res.redirect('/bm/product')
          } else {
            req.flash('info', 'Create product success')
            req.flash('info_type', 'success')
            res.redirect('/bm/product')
          }
        })
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/product')
  }
}
exports.update_product_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.id && req.body.name && req.body.description && req.body.productCode && req.body.price && req.body.productCategory &&
    req.body.printerId && req.body.validStartDate){
      ProductCategory.findById(req.body.productCategory, {code:1, name:1, productGroup:1}).exec(function (err, productCategory) {
      if (err || productCategory == null) {
        req.flash('info', 'Create product failed')
        req.flash('info_type', 'danger')
        res.redirect('/bm/product')
      } else {
        var file_url = req.file?('/image/' +  res.req.file.filename):''
        // if(!Array.isArray(req.body.productcategory)) req.body.productcategory = [req.body.productcategory]
        var productData={
          name: req.body.name,
          description: req.body.description,
          productCode: req.body.productCode,
          price: req.body.price,
          group: req.body.group,
          printerId: req.body.printerId,
          productCategory: productCategory._id,
          productGroup: productCategory.productGroup,
          validStartDate: req.body.validStartDate,
          brand: user_data.brand,
          updatedDate: Date.now(),
          updatedBy: user_data._id,
        }
        if(file_url) productData.imageProduct = file_url
        Product.findOneAndUpdate({_id:req.body.id},productData, function (error, product) {
          if (error || product==null) {
            console.log(error)
            req.flash('info', 'Update product failed')
            req.flash('info_type', 'danger')
            res.redirect('/bm/product')
          } else {
            req.flash('info', 'Update product success')
            req.flash('info_type', 'success')
            res.redirect('/bm/product')
          }
        })
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/product')
  }
}
exports.delete_product_process = function (req, res, next) {
  auth_redirect(req, res)
  if (req.params.productId){
    Product.findByIdAndRemove(req.params.productId, function (error, product) {
      if (error || product==null) {
        console.log(error)
        req.flash('info', 'Delete product failed')
        req.flash('info_type', 'danger')
        res.redirect('/bm/product')
      } else {
        req.flash('info', 'Delete product success')
        req.flash('info_type', 'success')
        res.redirect('/bm/product')
      }
    })
  } else {
    req.flash('info', 'Product id required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/product')
  }
}
exports.single_product = function (req, res, next) {
  Session.validate(req.session.key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.productId){
        Product.findOne({_id:req.params.productId},{'__v':0})
        .exec(function (error, product) {
          if (err) {
            var err = { status:400, message:err }
            return res.send(JSON.stringify(err))
          } if (product==null) {
            var err = { status:400, message:'Product not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data: product
            }
            // console.log(ret)
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'Product id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
// API POST bm create product
exports.create_product = function (req, res, next) {
  Session.validate(req.headers.key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.name && req.body.description && req.body.PLUId && req.body.price &&
        req.body.group && req.body.printerId && req.body.validStartDate ){
        var productData={
          name: req.body.name,
          description: req.body.description,
          PLUId: req.body.PLUId,
          price: req.body.price,
          group: req.body.group,
          printerId: req.body.printerId,
          validStartDate: req.body.validStartDate,
          brandId: user.brandId,
          createdDate: Date.now(),
          createdBy: user._id,
        }
        Product.create(productData, function (error, product) {
          if (error || product==null) {
            var err = { status:400, message:error }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              message: 'Create product success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'All fields required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
// API POST bm update product
exports.update_product = function (req, res, next) {
  Session.validate(req.headers.key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.id && req.body.name && req.body.description && req.body.PLUId && req.body.price &&
        req.body.group && req.body.printerId && req.body.validStartDate ){
        var productData={
          name: req.body.name,
          description: req.body.description,
          PLUId: req.body.PLUId,
          price: req.body.price,
          group: req.body.group,
          printerId: req.body.printerId,
          validStartDate: req.body.validStartDate,
          brandId: user.brandId,
          updatedDate: Date.now(),
          updatedBy: user._id,
        }
        Product.findOneAndUpdate({_id:req.body.id},productData, function (error, product) {
          if (error) {
            var err = { status:400, message:error }
            return res.send(JSON.stringify(err))
          } if (product==null) {
            var err = { status:400, message:'Product not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              message: 'Update product success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'All fields required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
// API GET bm product list
exports.product_list = function (req, res, next) {
  Session.validate(req.headers.key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      Product.find({},{'__v':0})
      .exec(function (error, products) {
        if (err) {
          var err = { status:400, message:err }
          return res.send(JSON.stringify(err))
        } if (!products || products.length==0) {
          var err = { status:400, message:'Product list not found' }
          return res.send(JSON.stringify(err))
        } else {
          var ret = {
            status:200,
            data: products
          }
          return res.send(JSON.stringify(ret))
        }
      })
    }
  })
}
// GET API bm delete product
exports.delete_product = function (req, res, next) {
  Session.validate(req.headers.key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.productId){
        Product.findByIdAndRemove(req.params.productId, function (error, product) {
          if (error) {
            var err = { status:400, message:error }
            return res.send(JSON.stringify(err))
          } if (product==null) {
            var err = { status:400, message:'Product not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              message: 'Delete product success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'Product id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}


//product_update
exports.product_update = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined'){
        messages = res.locals.info
        messages_type = res.locals.info_type
    }
    async.parallel([
      function(callback) {
        ProductUpdate.find({brand:user_data.brand}).sort({createdDate : -1})
        .populate('brand').exec(function (err,product_update_list) {
          if (err) return callback(err)
          locals.product_update_list = product_update_list
          callback()
        })
      },
      function(callback) {
        Product.find({brand:user_data.brand})
        .exec(function (err,product_list) {
          if (err) return callback(err)
          locals.product_list = product_list
          callback()
        })
      },      
    ], function(err) {
       if (err) return next(err)
       res.render('product_update', {
          title : appName+" | "+"Products Update",
          user_data : user_data,
          product_update_list : locals.product_update_list,
          product_list : locals.product_list,
          messages : messages,
          messages_type : messages_type
       })
   })
}
exports.create_product_update = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined'){
        messages = res.locals.info
        messages_type = res.locals.info_type
    }
    async.parallel([
      function(callback) {
        Product.find({brand:user_data.brand})
        .exec(function (err,product_list) {
          if (err) return callback(err)
          locals.product_list = product_list
          callback()
        })
      },      
    ], function(err) {
       if (err) return next(err)
       console.log(locals.product_updatecategory)
       res.render('create_product_update', {
          title : appName+" | "+"Create Products Update",
          user_data : user_data,
          product_list : locals.product_list,
          messages : messages,
          messages_type : messages_type
       })
   })
}
exports.create_product_update_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.validDate && req.body.product_updates){
    var productUpdates = JSON.parse(req.body.product_updates)
    var createData = {
      brand: user_data.brand,
      hoId: user_data.headOffice,
      validDate: req.body.validDate,
      updateToPOSDate: moment().add(1, 'day'),
      productChanges: productUpdates.map(p => {
        return {
          product: p.ProductId,
          name: p.NewName == '-' ? null : p.NewName,
          description: p.NewDescription == '-' ? null : p.NewDescription,    
          price: p.NewPrice
        }
      }),
      createdDate: Date.now(),
      createdBy: user_data._id,
    }
    ProductUpdate.create(createData, function (error, result) {
      if (error || result == null) {
        console.log(error)
        req.flash('info', 'Create delivery order failed')
        req.flash('info_type', 'danger')
        res.redirect('/bm/create_product_update')
      } else {
        req.flash('info', 'Create delivery order success')
        req.flash('info_type', 'success')
        res.redirect('/bm/product_update')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/product_update')
  }
}
exports.update_product_update = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined'){
        messages = res.locals.info
        messages_type = res.locals.info_type
    }
    async.parallel([
      function(callback) {
        ProductUpdate.findById(req.params.id).sort({createdDate : -1})
        .populate('brand productChanges.product').exec(function (err,product_update) {
          if (err) return callback(err)
          locals.product_update = product_update
          callback()
        })
      },
      function(callback) {
        Product.find({brand:user_data.brand})
        .exec(function (err,product_list) {
          if (err) return callback(err)
          locals.product_list = product_list
          callback()
        })
      },      
    ], function(err) {
       if (err) return next(err)
       res.render('edit_product_update', {
          title : appName+" | "+"Edit Products Update",
          user_data : user_data,
          product_update : locals.product_update,
          product_list : locals.product_list,
          messages : messages,
          messages_type : messages_type
       })
   })
}
exports.update_product_update_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.validDate && req.body.product_updates){
    var productUpdates = JSON.parse(req.body.product_updates)
    var updateData = {
      brand: user_data.brand,
      hoId: user_data.headOffice,
      validDate: req.body.validDate,
      updateToPOSDate: moment().add(1, 'day'),
      productChanges: productUpdates.map(p => {
        return {
          product: p.ProductId,
          name: p.NewName == '-' ? null : p.NewName,
          description: p.NewDescription == '-' ? null : p.NewDescription,    
          price: p.NewPrice
        }
      }),
      createdDate: Date.now(),
      createdBy: user_data._id,
    }
    ProductUpdate.findByIdAndUpdate(req.params.id, updateData, function (error, result) {
      if (error || result == null) {
        console.log(error)
        req.flash('info', 'Update delivery order failed')
        req.flash('info_type', 'danger')
        res.redirect('/bm/update_product_update')
      } else {
        req.flash('info', 'Update delivery order success')
        req.flash('info_type', 'success')
        res.redirect('/bm/product_update')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/product_update')
  }
}
exports.delete_product_update_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.id){
    ProductUpdate.findByIdAndRemove(req.params.id, function (error, product_update) {
      if (error || product_update==null) {
        console.log(error)
        req.flash('info', 'Delete product_update failed')
        req.flash('info_type', 'danger')
        res.redirect('/bm/product_update')
      } else {
        req.flash('info', 'Delete product_update success')
        req.flash('info_type', 'success')
        res.redirect('/bm/product_update')
      }
    })
  } else {
    req.flash('info', 'Product id required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/product_update')
  }
}
exports.single_product_update = function (req, res, next) {
  Session.validate(req.session.key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        ProductUpdate.findById(req.params.id,{'__v':0}).populate('brand productChanges.product')
        .exec(function (err, product_update) {
          if (err) {
            var err = { status:400, message:err }
            return res.send(JSON.stringify(err))
          } if (product_update==null) {
            var err = { status:400, message:'Product not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data: product_update
            }
            // console.log(ret)
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'Product id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}


//news
exports.news = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined'){
        messages = res.locals.info
        messages_type = res.locals.info_type
    }
  async.parallel([
       //Load News Data
       function(callback) {
            News.find({brand:user_data.brand}).sort({createdDate : -1}).populate('brand createdBy updatedBy').exec(function(err,news){
              if (err) return callback(err)
               locals.news = news
               callback()
           })
       },
       function(callback){
        Brand.findOne({_id:user_data.brand},function(err,brand){
          if (err) return callback(err)
           locals.brand = brand
           callback()
         })
       }
   ], function(err) {
       if (err) return next(err)
       res.render('news', {
         title : appName+" | "+"News",
         user_data : user_data,
         news : locals.news,
         brand: locals.brand,
         messages : messages,
         messages_type : messages_type
       })
   })
}
// POST sm create news
exports.create_news_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.title && req.body.description &&  req.body.valid_start_date ){
    var newsData={
      title: req.body.title,
      description: req.body.description,
      validStartDate: req.body.valid_start_date,
      brand: user_data.brand,
      createdDate: Date.now(),
      createdBy: user_data._id,
    }
    News.create(newsData, function (error, news) {
      if (error || news==null) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Create news success')
        req.flash('info_type', 'success')
        res.redirect('/bm/news')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/news')
  }
}
// POST sm delete news
exports.delete_news_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.newsId){
    News.findByIdAndRemove(req.params.newsId, function (error, news) {
      if (error || news==null) {
        console.log(error)
        req.flash('info', 'Delete news failed')
        req.flash('info_type', 'danger')
        res.redirect('/bm/news')
      } else {
        req.flash('info', 'Delete news success')
        req.flash('info_type', 'success')
        res.redirect('/bm/news')
      }
    })
  } else {
    req.flash('info', 'News id required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/news')
  }
}


// POST API bm create news
exports.create_news = function (req, res, next) {
  Session.validate(req.headers.key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.title && req.body.description && req.body.validStartDate ){
        var newsData={
          title: req.body.title,
          description: req.body.description,
          validStartDate: req.body.validStartDate,
          brandId: 'brandId',
          createdDate: Date.now(),
          createdBy: user._id,
        }
        News.create(newsData, function (error, news) {
          if (error || news==null) {
            var err = { status:400, message:error }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              message: 'Create news success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'All fields required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// POST API bm update news
exports.update_news = function (req, res, next) {
  Session.validate(req.headers.key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.id && req.body.title && req.body.description && req.body.validStartDate ){
        var newsData={
          title: req.body.title,
          description: req.body.description,
          validStartDate: req.body.validStartDate,
          brandId: 'brandId',
          updatedDate: Date.now(),
          updatedBy: user._id,
        }
        News.findOneAndUpdate({_id:req.body.id},newsData, function (error, news) {
          if (error) {
            var err = { status:400, message:error }
            return res.send(JSON.stringify(err))
          } if (news==null) {
            var err = { status:400, message:'News not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              message: 'Update news success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'All fields required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// API GET bm news list
exports.news_list = function (req, res, next) {
  Session.validate(req.headers.key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      News.find({},{'__v':0})
      .exec(function (error, news) {
        if (err) {
          var err = { status:400, message:err }
          return res.send(JSON.stringify(err))
        } if (!news || news.length==0) {
          var err = { status:400, message:'News list not found' }
          return res.send(JSON.stringify(err))
        } else {
          var ret = {
            status:200,
            data: news
          }
          return res.send(JSON.stringify(ret))
        }
      })
    }
  })
}

// GET API bm single news
exports.single_news = function (req, res, next) {
  Session.validate(req.headers.key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.newsId){
        News.findOne({_id:req.params.newsId},{'__v':0})
        .exec(function (error, news) {
          if (err) {
            var err = { status:400, message:err }
            return res.send(JSON.stringify(err))
          } if (news==null) {
            var err = { status:400, message:'News not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data: news
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'News id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// GET API bm delete news
exports.delete_news = function (req, res, next) {
  Session.validate(req.headers.key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.newsId){
        News.findByIdAndRemove(req.params.newsId, function (error, news) {
          if (error) {
            var err = { status:400, message:error }
            return res.send(JSON.stringify(err))
          } if (news==null) {
            var err = { status:400, message:'News not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              message: 'Delete news success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'News id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
////////////////////////////////////
//TABLE
///////////////////////////////////
//VIEW
exports.table = function(req, res){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined'){
        messages = res.locals.info
        messages_type = res.locals.info_type
    }
    Brand.findOne({_id:user_data.brand},function(err,brand){
      if (err) return callback(err)
      Store.find({brand:brand._id}, function(err, store){
        if (err) return callback(err)
        Table.find({brand:user_data.brand}).populate("brand store createdBy updatedBy").sort({createdDate:-1}).exec(function(err, table){
          if(err) console.log(err)
          else {
            res.render('table', {
              appName: appName,
              title : appName+" | "+"Table",
              user_data : user_data,
              table: table,
              brand: brand,
              store: store,
              messages:messages,
              messages_type:messages_type
            })
          }
        })
      })
    })
}

//CREATE
exports.table_create_process = function (req, res, next){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.number && req.body.status){
    var dataCreate={
      number: req.body.number,
      brand: user_data.brand,
      store: req.body.store,
      type: req.body.type,
      status: req.body.status,
      createdDate: Date.now(),
      createdBy: user_data._id,
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    Table.create(dataCreate, function (error, result) {
      if (error || result==null) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Update Data success')
        req.flash('info_type', 'success')
        res.redirect('/bm/table')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/table')
  }
}
//UPDATE
exports.table_edit_process = function (req, res, next){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.number && req.body.status){
    var dataUpdate={
      number: req.body.number,
      type: req.body.type,
      store: req.body.store,
      status: req.body.status,
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    Table.findOneAndUpdate({_id:req.body.id},dataUpdate, function (error, result) {
      if (error || result==null) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Update Data success')
        req.flash('info_type', 'success')
        res.redirect('/bm/table')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/table')
  }
}

//SINGE DATA
exports.get_single_tables = function (req, res, next) {
  var key = req.session.key
  Session.validate(key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        Table.findOne({_id:req.params.id},{'__v':0})
        .exec(function (error, result) {
          if (error) {
            var err = { status:400, message:'Something went wrong' }
            return res.send(JSON.stringify(err))
          } if (result==null) {
            var err = { status:400, message:'Data not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data: result
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'Data id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

//DELETE
exports.table_delete_process = function (req, res, next) {
  auth_redirect(req, res)
  var redirect = ""
  redirect = req.params.redirect
  if (req.params.id){
    Table.findByIdAndRemove(req.params.id, function (error, result) {
      if (error) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } if (result==null) {
        req.flash('info', 'Head Office not found')
        req.flash('info_type', 'danger')
        res.redirect('/bm/table')
      } else {
        req.flash('info', 'Head Office  User success')
        req.flash('info_type', 'success')
        res.redirect('/bm/table')
      }
    })
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/bm')
  }
}
////////////////////////////////////
//Payment
///////////////////////////////////
//VIEW
exports.payment = function(req, res){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined'){
        messages = res.locals.info
        messages_type = res.locals.info_type
    }
      Brand.findOne({_id:user_data.brand},function(err,brand){
        if (err) return callback(err)
        Payment.find({brand:user_data.brand}).populate("brand createdBy updatedBy").sort({createdDate:-1}).exec(function(err, payment){
          if(err) console.log(err)
          else {
            res.render('payment', {
              appName: appName,
              title : appName+" | "+"Payment",
              user_data : user_data,
              brand: brand,
              payment: payment,
              messages:messages,
              messages_type:messages_type
            })
          }
        })
     })

}

//CREATE
exports.payment_create_process = function (req, res, next){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name && req.body.type){
    var file_url = req.file?('/image/' +  res.req.file.filename):''
    var dataCreate={
      name: req.body.name,
      brand: user_data.brand,
      type: req.body.type,
      createdDate: Date.now(),
      createdBy: user_data._id,
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    if(file_url) dataCreate.logo=file_url
    Payment.create(dataCreate, function (error, result) {
      if (error || result==null) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Update Data success')
        req.flash('info_type', 'success')
        res.redirect('/bm/payment')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/payment')
  }
}
//UPDATE
exports.payment_edit_process = function (req, res, next){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name && req.body.type && req.body.id){
    var file_url = req.file?('/image/' +  res.req.file.filename):''
    var dataUpdate={
      name: req.body.name,
      type: req.body.type,
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    if(file_url) dataCreate.logo=file_url
    Payment.findOneAndUpdate({_id:req.body.id},dataUpdate, function (error, result) {
      if (error || result==null) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Update Data success')
        req.flash('info_type', 'success')
        res.redirect('/bm/payment')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/payment')
  }
}

//SINGE DATA
exports.get_single_payments = function (req, res, next) {
  var key = req.session.key
  Session.validate(key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        Payment.findOne({_id:req.params.id},{'__v':0})
        .exec(function (error, result) {
          if (error) {
            var err = { status:400, message:'Something went wrong' }
            return res.send(JSON.stringify(err))
          } if (result==null) {
            var err = { status:400, message:'Data not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data: result
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'Data id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

//DELETE
exports.payment_delete_process = function (req, res, next) {
  auth_redirect(req, res)
  var redirect = ""
  redirect = req.params.redirect
  if (req.params.id){
    Payment.findByIdAndRemove(req.params.id, function (error, result) {
      if (error) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } if (result==null) {
        req.flash('info', 'Head Office not found')
        req.flash('info_type', 'danger')
        res.redirect('/bm/payment')
      } else {
        req.flash('info', 'Head Office  User success')
        req.flash('info_type', 'success')
        res.redirect('/bm/payment')
      }
    })
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/bm')
  }
}
////////////////////////////////////
//ProductCategory
///////////////////////////////////
//VIEW
exports.product_category = function(req, res){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined'){
      messages = res.locals.info
      messages_type = res.locals.info_type
    }
      Brand.findOne({_id:user_data.brand},function(err,brand){
        if (err) return callback(err)
        ProductGroup.find({brand:user_data.brand}, function(err, product_group){
          if(err) return callback(err)
          ProductCategory.find({brand:user_data.brand}).populate("brand productGroup createdBy updatedBy").sort({createdDate:-1}).exec(function(err, product_category){
            if(err) console.log(err)
            else {
              res.render('product_category', {
                appName: appName,
                title : appName+" | "+"Product Category",
                user_data : user_data,
                brand: brand,
                product_group: product_group,
                product_category: product_category,
                messages:messages,
                messages_type:messages_type
              })
            }
          })
        })
     })
}

//CREATE
exports.product_category_create_process = function (req, res, next){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name && req.body.productGroup){
    var dataCreate={
      code: req.body.code,
      name: req.body.name,
      brand: user_data.brand,
      productGroup: req.body.productGroup,
      createdDate: Date.now(),
      createdBy: user_data._id,
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    ProductCategory.create(dataCreate, function (error, result) {
      if (error || result==null) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Update Data success')
        req.flash('info_type', 'success')
        res.redirect('/bm/product_category')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/product_category')
  }
}
//UPDATE
exports.product_category_edit_process = function (req, res, next){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name && req.body.id && req.body.productGroup && req.body.code){
    var dataUpdate={
      code: req.body.code,
      name: req.body.name,
      productGroup: req.body.productGroup,
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    ProductCategory.findOneAndUpdate({_id:req.body.id},dataUpdate, function (error, result) {
      if (error || result==null) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Update Data success')
        req.flash('info_type', 'success')
        res.redirect('/bm/product_category')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/product_category')
  }
}

//SINGE DATA
exports.get_single_product_categorys = function (req, res, next) {
  var key = req.session.key
  Session.validate(key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        ProductCategory.findOne({_id:req.params.id},{'__v':0})
        .exec(function (error, result) {
          if (error) {
            var err = { status:400, message:'Something went wrong' }
            return res.send(JSON.stringify(err))
          } if (result==null) {
            var err = { status:400, message:'Data not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data: result
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'Data id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

//DELETE
exports.product_category_delete_process = function (req, res, next) {
  auth_redirect(req, res)
  var redirect = ""
  redirect = req.params.redirect
  if (req.params.id){
    ProductCategory.findByIdAndRemove(req.params.id, function (error, result) {
      if (error) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } if (result==null) {
        req.flash('info', 'Data not found')
        req.flash('info_type', 'danger')
        res.redirect('/bm/product_category')
      } else {
        req.flash('info', 'Delete User success')
        req.flash('info_type', 'success')
        res.redirect('/bm/product_category')
      }
    })
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/bm')
  }
}
////////////////////////////////////
//Product Group
///////////////////////////////////
//VIEW
exports.product_group = function(req, res){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined'){
        messages = res.locals.info
        messages_type = res.locals.info_type
    }
      Brand.findOne({_id:user_data.brand},function(err,brand){
        if (err) return callback(err)
        ProductGroup.find({brand:user_data.brand}).populate("brand createdBy updatedBy").sort({createdDate:-1}).exec(function(err, product_group){
          if(err) console.log(err)
          else {
            res.render('product_group', {
              appName: appName,
              title : appName+" | "+"Product group",
              user_data : user_data,
              brand: brand,
              product_group: product_group,
              messages:messages,
              messages_type:messages_type
            })
          }
        })
     })

}

//CREATE
exports.product_group_create_process = function (req, res, next){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name && req.body.code){
    var dataCreate={
      code: req.body.code,
      name: req.body.name,
      brand: user_data.brand,
      createdDate: Date.now(),
      createdBy: user_data._id,
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    ProductGroup.create(dataCreate, function (error, result) {
      if (error || result==null) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Update Data success')
        req.flash('info_type', 'success')
        res.redirect('/bm/product_group')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/product_group')
  }
}
//UPDATE
exports.product_group_edit_process = function (req, res, next){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name && req.body.id && req.body.code){
    var dataUpdate={
      code: req.body.code,
      name: req.body.name,
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    ProductGroup.findOneAndUpdate({_id:req.body.id},dataUpdate, function (error, result) {
      if (error || result==null) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Update Data success')
        req.flash('info_type', 'success')
        res.redirect('/bm/product_group')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/product_group')
  }
}

//SINGE DATA
exports.get_single_product_groups = function (req, res, next) {
  var key = req.session.key
  Session.validate(key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        ProductGroup.findOne({_id:req.params.id},{'__v':0})
        .exec(function (error, result) {
          if (error) {
            var err = { status:400, message:'Something went wrong' }
            return res.send(JSON.stringify(err))
          } if (result==null) {
            var err = { status:400, message:'Data not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data: result
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'Data id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

//DELETE
exports.product_group_delete_process = function (req, res, next) {
  auth_redirect(req, res)
  redirect = req.params.redirect
  if (req.params.id){
    ProductGroup.findByIdAndRemove(req.params.id, function (error, result) {
      if (error) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } if (result==null) {
        req.flash('info', 'Data not found')
        req.flash('info_type', 'danger')
        res.redirect('/bm/product_group')
      } else {
        req.flash('info', 'Delete Data success')
        req.flash('info_type', 'success')
        res.redirect('/bm/product_group')
      }
    })
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/bm')
  }
}

////////////////////////////////////
//Discount
///////////////////////////////////
exports.discount = function(req, res){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined'){
        messages = res.locals.info
        messages_type = res.locals.info_type
    }
      Product.find({brand:user_data.brand}, function(err, product){
        if (err) return callback(err)
        else {
          ProductCategory.find({brand:user_data.brand}, function(err, productcategory){
            if (err) return callback(err)
            else {
              ProductGroup.find({brand:user_data.brand}, function(err, productgroup){
                if (err) return callback(err)
                else {
                  Payment.find({brand:user_data.brand}, function(err, payment){
                    if (err) return callback(err)
                    else{
                      Brand.findOne({_id:user_data.brand},function(err,brand){
                        if (err) return callback(err)
                        Discount.find({brand:user_data.brand}).populate("brand createdBy updatedBy").sort({createdDate:-1}).exec(function(err, discount){
                          if(err) console.log(err)
                          else {
                            res.render('discount', {
                              appName: appName,
                              title : appName+" | "+"Product group",
                              user_data : user_data,
                              brand: brand,
                              discount: discount,
                              product: product,
                              productcategory: productcategory,
                              productgroup: productgroup,
                              payment: payment,
                              messages:messages,
                              messages_type:messages_type
                            })
                          }
                        })
                     })
                    }
                  })
                }
              })
            }
          })
        }
      })
}
exports.discount_create_process = function (req, res, next){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name && req.body.value && req.body.valueType){
    var dataCreate={
      name: req.body.name,
      brand: user_data.brand,
      value: req.body.value,
      valueType: req.body.valueType,
      createdDate: Date.now(),
      createdBy: user_data._id,
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    if(req.body.product)
    dataCreate.product=req.body.product
    if(req.body.productcategory)
    dataCreate.productcategory=req.body.productcategory
    if(req.body.productgroup)
    dataCreate.productgroup=req.body.productgroup
    if(req.body.payment)
    dataCreate.payment=req.body.payment
    if(req.body.maxValue)
    dataCreate.maxValue= req.body.maxValue
    Discount.create(dataCreate, function (error, result) {
      if (error || result==null) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Update Data success')
        req.flash('info_type', 'success')
        res.redirect('/bm/discount')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/discount')
  }
}
exports.discount_edit_process = function (req, res, next){
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name && req.body.id){
    var dataUpdate={
      name: req.body.name,
      valueType: req.body.valueType,
      maxValue: req.body.maxValue,
      product: req.body.product,
      productcategory: req.body.productcategory,
      productgroup: req.body.productgroup,
      payment: req.body.payment,
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    Discount.findOneAndUpdate({_id:req.body.id},dataUpdate, function (error, result) {
      if (error || result==null) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Update Data success')
        req.flash('info_type', 'success')
        res.redirect('/bm/discount')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/bm/discount')
  }
}
exports.get_single_discounts = function (req, res, next) {
  var key = req.session.key
  Session.validate(key, 'bm', function (error, user) {
    if(error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: error}
      return res.send(JSON.stringify(err))
    } else if (user.error){
      var err = { status:400, message:'Session key incorrect or not found', innerMessage: user.message}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        Discount.findOne({_id:req.params.id},{'__v':0})
        .exec(function (error, result) {
          if (error) {
            var err = { status:400, message:'Something went wrong' }
            return res.send(JSON.stringify(err))
          } if (result==null) {
            var err = { status:400, message:'Data not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data: result
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'Data id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
exports.discount_delete_process = function (req, res, next) {
  auth_redirect(req, res)
  var redirect = ""
  redirect = req.params.redirect
  if (req.params.id){
    Discount.findByIdAndRemove(req.params.id, function (error, result) {
      if (error) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } if (result==null) {
        req.flash('info', 'Data not found')
        req.flash('info_type', 'danger')
        res.redirect('/bm/discount')
      } else {
        req.flash('info', 'Delete Data success')
        req.flash('info_type', 'success')
        res.redirect('/bm/discount')
      }
    })
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/bm')
  }
}


//revenue
exports.revenue = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load Revenue Data
    function (callback) {
      Revenue.find({brand:user_data.brand}).populate('store createdBy updatedBy').sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.revenue = result
        callback()
      })
    },
    // Load Brand Data
    function (callback) {
      Brand.find({}, function (err, result) {
        if (err) return callback(err)
        locals.brands = result
        callback()
      })
    }
  ], function (err) { //This function gets called after the two tasks have called their "task callbacks"
    if (err) return next(err) //If an error occurred, we let express handle it by calling the `next` function
    //Here `locals` will be an object with `user` and `posts` keys
    res.render('revenue', {
      title: appName + " | " + "Revenue",
      user_data: user_data,
      revenue: locals.revenue,
      brands: locals.brands,
      messages: messages,
      messages_type: messages_type
    })
  })
}