const User = require('../models/user.model')
const Brand = require('../models/brand.model')
const News = require('../models/news.model')
const Product = require('../models/product.model')
const Revenue = require('../models/revenue.model')
const Session = require('../models/session.model')
const Store = require('../models/store.model')
var dateFormat = require('dateformat')
var request = require('request')
var crypto = require('crypto')
var nodemailer = require('nodemailer')
const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId

var transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: '587',
  auth: {
    user: "maningcorp@gmail.com",
    pass: "maning123!"
  },
  secureConnection: 'false',
  tls: {
    ciphers: 'SSLv3',
    rejectUnauthorized: false

  }
})

function random_password() {
  var pass = (Math.random().toString(36).substr(2)).toUpperCase()
  pass = crypto.createHash('md5').update(pass).digest('hex')
  return pass
}

exports.file = function(req, res){
  res.send("7D573EB7EA08047100B5AB8B406E2ECBA957590EDD5785268958498D1203E717 comodoca.com 60a5484a8046a")
}

//Dashboard checking
exports.main_page = function (req, res) {
  auth_redirect(req, res)
  if (req.session.user_data) {
    var user_data = req.session.user_data
    if (user_data.role == 'sm_supervisor' || user_data.role == 'sm_warehouse') res.redirect('/sm')
    if (user_data.role == 'bm') res.redirect('/bm')
    if (user_data.role == 'ho') res.redirect('/ho')
    if (user_data.role == 'ho_staff') res.redirect('/ho_staff')
    if (user_data.role == 'admin') res.redirect('/admin')
  }
}

//forgot password
exports.forgot_password = function (req, res) {
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  res.render('forgot_password', {
    title: appName + " | " + "Forgot Password",
    messages: messages,
    messages_type: messages_type
  })
}

//signin
exports.signin = function (req, res) {
  var messages = {}
  res.locals.info = req.flash('info')
  var redirect = req.params.redirect
  sess = req.session
  sess.redirect = redirect
  // console.log(sess.redirect)
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
  }
  res.render('signin', {
    title: appName + " | " + "SignIn",
    messages: messages,
    messages_type: 'danger'
  })
}

//signin
exports.signin_process = function (req, res) {
  // console.log(req.body)
  var email = req.body.email.toString().toLowerCase()
  var password = req.body.password
  sess = req.session
  var redirect = sess.redirect
  var locals = {}
  User.authenticate(email, password, function (error, user) {
    console.log("BAPAKE 2",user)
    if (error || !user || user.length <= 0) {
      var ret = {
        status: 400,
        message: "Invalid Email/Password"
      }
      res.send(JSON.stringify(ret))
    } else {
      console.log("BAPAKE",user)
      locals.user = user
      var sessionData = {
        token: crypto.createHash('md5').update(req.sessionID + (new Date(user.expire))).digest('hex'),
        user: user._id,
        createdDate: Date.now(),
      }
      var options = {
        upsert: true,
        new: true,
        setDefaultsOnInsert: true
      }
      Session.findOneAndUpdate({
        user: ObjectId(user._id)
      }, sessionData, options, function (error, session) {
        if (!error || session != null) {
          // var date_expire = (new Date(user.expire))
          // var date_now = (new Date())
          // Get client data from billing
          //checkToken
          //Jika role nya store manager, dicek token di storenya
          //Jika role nya head office, dicek token di usernya
          if (user.role == "ho" || user.role == "ho_staff") {
            if (user.headOffice.token) {
              request(billingAPI + "checktoken/" + user.headOffice.token, {
                headers: {
                  'secretCode': secretCode
                }
              }, function (error, response, body) {
                var result = JSON.parse(body)
                if (result.status == "200") {
                  sess.user_login = email
                  sess.user_data = user
                  sess.key = session.token
                  if (sess.redirect) {
                    var redirect = sess.redirect
                    redirect = "/" + sess.redirect.replace(/-/g, "/")
                    var ret = {
                      status: 200,
                      message: "Login successful, please wait..",
                      redirect: redirect
                    }
                    res.send(JSON.stringify(ret))
                  } else {
                    var ret = {
                      status: 200,
                      message: "Login successful, please wait.."
                    }
                    res.send(JSON.stringify(ret))
                  }
                } else if (result.status == "300") {
                  var ret = {
                    status: 300,
                    message: result.message
                  }
                  res.send(JSON.stringify(ret))
                } else {

                  var ret = {
                    status: 400,
                    message: result.message
                  }
                  res.send(JSON.stringify(ret))
                }
              })
            } else {
              var ret = {
                status: 300,
                message: "No token found, please input your token"
              }
              res.send(JSON.stringify(ret))
            }
          } else if (user.role == "sm_warehouse" || user.role == "sm_supervisor") {
            if (user.store.token) {
              request(billingAPI + "checkToken/" + user.store.token, {
                headers: {
                  'secretCode': secretCode
                }
              }, function (error, response, body) {
                var result = JSON.parse(body)
                if (result.status == "200") {
                  sess.user_login = email
                  sess.user_data = user
                  sess.key = session.token
                  if (sess.redirect) {
                    var redirect = sess.redirect
                    redirect = "/" + sess.redirect.replace(/-/g, "/")
                    var ret = {
                      status: 200,
                      message: "Login successful, please wait..",
                      redirect: redirect
                    }
                    res.send(JSON.stringify(ret))
                  } else {
                    var ret = {
                      status: 200,
                      message: "Login successful, please wait.."
                    }
                    res.send(JSON.stringify(ret))
                  }
                } else {
                  var ret = {
                    status: result.status,
                    message: result.message
                  }
                  res.send(JSON.stringify(ret))
                }
              })
            } else {
              var ret = {
                status: 300,
                message: "No token found, please input your token"
              }
              res.send(JSON.stringify(ret))
            }
          } else {
            sess.user_login = email
            sess.user_data = user
            sess.key = session.token
            if (sess.redirect) {
              var redirect = sess.redirect
              redirect = "/" + sess.redirect.replace(/-/g, "/")
              var ret = {
                status: 200,
                message: "Login successful, please wait..",
                redirect: redirect
              }
              res.send(JSON.stringify(ret))
            } else {
              var ret = {
                status: 200,
                message: "Login successful, please wait.."
              }
              res.send(JSON.stringify(ret))
            }
          }
        } else {
          var ret = {
            status: 400,
            message: "Invalid Email/Password"
          }
          res.send(JSON.stringify(ret))
        }
      })
    }
  })
}

//signout
exports.signout = function (req, res) {
  Session.findOneAndRemove({
    token: req.session.key
  }, function (error) {
    if (error) {
      console.log('Login Berhasil')
    } else {
      req.session.destroy(function (err) {
        if (err) {
          console.log(err)
        } else {
          res.redirect('/')
        }
      })
    }
  })
}


// POST forgot password
exports.forgot_password_process = function (req, res, next) {
  if (req.body.email) {
    User.findOne({
      email: req.body.email
    }, {}, function (err, user) {
      if (err || !user) {
        req.flash('info', 'Email is not registered')
        req.flash('info_type', 'danger')
        res.redirect('/forgot_password')
        // var err = { status:400, message:'Email is not registered' }
        // return res.send(JSON.stringify(err))
      } else {
        var newpass = random_password()
        User.findOneAndUpdate({
          _id: user._id
        }, {
          password: newpass
        }, function (error, usr) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            var baseUrl = req.headers.host + '/' + req.originalUrl.split('/')[1]
            var mailOptions = {
              from: 'ISIPos@gmail.com',
              to: req.body.email,
              subject: 'Pricesync Account Forgot Password',
              html: `Please click this link if you want to reset your password <br>
                  <a href="https://` + baseUrl + `/reset_password/` + req.body.email + `/` + newpass + `">
                    https://` + baseUrl + `/reset_password/` + req.body.email + `/` + newpass + `
                  </a>
                `
            }
            transporter.sendMail(mailOptions, function (error, info) {
              if (error) {
                var err = {
                  status: 400,
                  message: error
                }
                return res.send(JSON.stringify(err))
              } else {
                // var ret = {
                //   status:200,
                //   message: 'Email Sent. Please open your email to reset password'
                // }
                req.flash('info', 'Email Sent. Please open your email to reset password')
                req.flash('info_type', 'success')
                res.redirect('/forgot_password')
                // return res.send(JSON.stringify(ret))
              }
            })
          }
        })
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/forgot_password')
  }
}

// GET admin reset_password
exports.reset_password = function (req, res, next) {
  User.findOne({
      email: req.params.email,
      password: req.params.rstcode
    }, {
      '__v': 0
    })
    .exec(function (error, user) {
      var baseUrl = req.headers.host + '/' + req.originalUrl.split('/')[1]
      res.render('reset_password', {
        title: "Reset Password",
        email: req.params.email,
        postUrl: 'https://' + baseUrl + '/reset_password'
      })
    })
}

// POST admin reset_password_process
exports.reset_password_process = function (req, res, next) {
  User.findOne({
      email: req.body.email
    }, {
      '__v': 0
    })
    .exec(function (error, user) {
      if (req.body.password != '' && req.body.conf_password != '' && req.body.password == req.body.conf_password) {
        var userData = {
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          updatedDate: Date.now(),
          upeatedBy: user._id,
        }
        User.findOneAndUpdate({
          _id: user._id
        }, userData, function (error, user) {
          if (error || user == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            // var ret = {
            //   status:200,
            //   message: 'Reset password success.'
            // }
            // return res.send(JSON.stringify(ret))
            req.flash('info', 'Reset Password Success')
            req.flash('info_type', 'success')
            res.redirect('/forgot_password')
          }
        })
      } else {
        // var err = { status:400, message:'All fields required' }
        // return res.send(JSON.stringify(err))
        req.flash('info', 'Reset Password Failed')
        req.flash('info_type', 'danger')
        res.redirect('/forgot_password')
      }
    })
}



// //CREATE
// exports.user_create = function (req, res) {
//     let user = new User(
//         {
//             name: req.body.name,
//             email : req.body.email,
//             password : crypto.createHash('md5').update(req.body.password).digest('hex'),
//             role: req.body.role,
//             expire: req.body.expire,
//             brandID: req.body.brandID,
//             storeID: req.body.storeID,
//             image_profile: "none",
//             createdDate: req.body.createdDate,
//             createdBy: req.body.createdBy,
//             updatedDate: req.body.updatedDate,
//             updatedBy: req.body.updatedBy
//         }
//     )

//     user.save(function (err) {
//         if (err) {
//             return next(err)
//         }
//         res.send('User Created successfully')
//     })
// }

// exports.news_create = function (req, res) {
//     let news = new News(
//         {
//             description: req.body.description,
//             datecreated : req.body.datecreated
//         }
//     )

//     news.save(function (err) {
//         if (err) {
//             return next(err)
//         }
//         res.send('News Created successfully')
//     })
// }
// exports.brand_create = function (req, res) {
//     let brand = new Brand(
//         {
//             name: req.body.name,
//             createdDate : req.createdDate,
//             createdBy : req.body.createdBy
//         }
//     )

//     brand.save(function (err) {
//         if (err) {
//             return next(err)
//         }
//         res.send('Brand Created successfully')
//     })
// }
// exports.store_create = function (req, res) {
//     let store = new Store(
//         {
//             name: req.body.name,
//             brand: req.body.brand,
//             createdDate: req.body.createdDate,
//             createdBy: req.body.createdBy,
//             updatedDate: req.body.updatedDate,
//             updatedBy: req.body.updatedBy
//         }
//     )

//     store.save(function (err) {
//         if (err) {
//             return next(err)
//         }
//         res.send('Store Created successfully')
//     })
// }


// //READ
// exports.user_details = function (req, res) {
//     User.find({'email': req.params.email}, function (err, user) {
//         if (err) return next(err)
//         res.send(user)
//     })
// }
// exports.read_brands = function (req, res) {
//     Brand.find({}, function (err, brand) {
//         if (err) return next(err)
//         res.send(brand)
//     })
// }
// exports.read_stores = function (req, res) {
//     Store.find({}, function (err, store) {
//         if (err) return next(err)
//         res.send(store)
//     })
// }
// //UPDATE
// exports.user_update = function (req, res) {
//     User.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, user) {
//         if (err) return next(err)
//         res.send('User upated.')
//     })
// }
// //DELETE
// exports.user_delete = function (req, res) {
//     User.remove({email:req.params.email}, function (err) {
//         if (err) return next(err)
//         res.send('Deleted successfully!')
//     })
// }


//function auth redirect
function auth_redirect(req, res) {
  sess = req.session
  if (sess.user_login) return true
  else res.redirect('/signin')
}