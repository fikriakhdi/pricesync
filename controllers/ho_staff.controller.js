const User = require('../models/user.model')
const News = require('../models/news.model')
const Session = require('../models/session.model')
const Product = require('../models/product.model')
const DeliveryOrder = require('../models/delivery_order.model')
const DOItem = require('../models/do_item.model')
const HeadOffice = require('../models/head_office.model')
const Brand = require('../models/brand.model')
const Store = require('../models/store.model')
const Stock = require('../models/stock.model')
const StockTransaction = require('../models/stock_transaction.model')
const Unit = require('../models/unit.model')
const Item = require('../models/item.model')
const ItemCategory = require('../models/item_category.model')
const Supplier = require('../models/supplier.model')
const Revenue = require('../models/revenue.model')
const Customer = require('../models/customer.model')
const moment = require('moment')
var crypto = require('crypto')
var dateFormat = require('dateformat')
var async = require('async')
var QRCode = require('qrcode')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const ObjectId = mongoose.Types.ObjectId

function ID() {
  return Math.random().toString(36).substr(2, 6).toUpperCase()
}
//Dashboard
exports.dashboard = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  console.log(user_data)
  var locals = {}
  async.parallel([
    function (callback) {
      HeadOffice.findById(user_data.headOffice).populate('brands')
        .exec(function (err, ho) {
          if (err) return callback(err)
          else {
            let storeslen = 0
            ho.brands.map((br, i) => {
              storeslen += br.stores.length
              if (i == ho.brands.length - 1) {
                locals.storeslen = storeslen
                locals.brandslen = ho.brands.length
                callback()
              }
            })
          }
        })
    },

    function (callback) {
      Supplier.find({
        headOffice: user_data.headOffice
      }, function (err, suppliers) {
        if (err) return callback(err)
        locals.supplierslen = suppliers.length
        callback()
      })
    },

    function (callback) {
      Item.find({
          hoId: user_data.headOffice
        })
        .exec(function (err, items) {
          if (err) return callback(err)
          locals.itemslen = items.length
          callback()
        })
    },

    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice
        })
        .exec(function (err, dorder) {
          if (err) return callback(err)
          locals.dolen = dorder.length
          callback()
        })
    },

    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          isDifference: true
        }).populate({
          path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData_alert = DOData
          // console.log(DOData)
          callback()
        })
    }

  ], function (err) {
    if (err) console.log(err)
    res.render('main', {
      title: appName + " | " + "Dashboard",
      user_data: user_data,
      brandslen: locals.brandslen,
      storeslen: locals.storeslen,
      supplierslen: locals.supplierslen,
      itemslen: locals.itemslen,
      DOData_alert: locals.DOData_alert,
      dolen: locals.dolen
    })
  })
}

//change password
exports.change_password_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.password != '' && req.body.conf_password != '') {
    if (req.body.password == req.body.conf_password) {
      var userData = {
        password: crypto.createHash('md5').update(req.body.password).digest('hex'),
        updatedDate: Date.now(),
        upeatedBy: user_data._id,
      }
      User.findByIdAndUpdate({
        _id: user_data._id
      }, userData, function (error, user) {
        if (error || user == null) {
          var err = {
            status: 400,
            message: error
          }
          return res.send(JSON.stringify(err))
        } else {
          req.flash('info', 'Change password success')
          req.flash('info_type', 'success')
          res.redirect('/ho/profile')
        }
      })
    } else {
      req.flash('info', 'Password didn\'t match')
      req.flash('info_type', 'danger')
      res.redirect('/ho/profile')
    }
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/profile')
  }
}


//profile
exports.profile = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load Head Office Data
    function (callback) {
      HeadOffice.findById(user_data.headOffice._id, function (err, ho) {
        if (err) return callback(err)
        locals.ho = ho
        callback()
      })
    },
    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          isDifference: true
        }).populate({
          path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData_alert = DOData
          // console.log(DOData)
          callback()
        })
    }

  ], function (err) { //This function gets called after the two tasks have called their "task callbacks"
    if (err) console.log('Error', err) //If an error occurred, we let express handle it by calling the `next` function
    //Here `locals` will be an object with `user` and `posts` keys
    //Example: `locals = {user: ..., posts: [...]}`
    res.render('profile', {
      title: appName + " | " + "Profile",
      user_data: user_data,
      ho: locals.ho,
      DOData_alert: locals.DOData_alert,
      messages: messages,
      messages_type: messages_type
    })
  })
}

// POST ho update_profile
exports.update_profile = function (req, res, next) {
  Session.validate(req.headers.key, 'ho', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.name != '' && req.body.email != '') {
        var userData = {
          name: req.body.name,
          email: req.body.email,
          imageProfile: "none",
          updatedDate: Date.now(),
          upeatedBy: user._id,
        }
        User.create(userData, function (error, user) {
          if (error || user == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Update profile success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

//receiving goods
exports.receiving_goods = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load Item Data
    function (callback) {
      Item.find({
          hoId: user_data.headOffice
        }).populate({
          path: 'stock',
          populate: {
            path: 'unit'
          }
        })
        .exec(function (err, items) {
          if (err) return callback(err)
          locals.items = items
          callback()
        })
    },
    function (callback) {
      HeadOffice.findById(user_data.headOffice).populate({
          path: 'brands',
          populate: {
            path: 'stores'
          }
        })
        .exec(function (err, HOData) {
          if (err) return callback(err)
          locals.brands = HOData.brands
          callback()
        })
    },
    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          type: "direct_ho"
        }).sort({
          createdDate: -1
        }).populate({
          path: 'headOffice supplier deliveryItem store recipientUser storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData = DOData
          // console.log(DOData[0].deliveryItem[0])
          callback()
        })
    },
    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          isDifference: true
        }).sort({
          createdDate: -1
        }).populate({
          path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData_alert = DOData
          // console.log(DOData)
          callback()
        })
    }
  ], function (err) {
    if (err) return next(err)
    res.render('receiving_goods', {
      title: appName + " | " + "Receiving Goods",
      user_data: user_data,
      item: locals.item,
      brands: locals.brands,
      DOData: locals.DOData,
      DOData_alert: locals.DOData_alert,
      messages: messages,
      messages_type: messages_type
    })
  })
}

//delivery order
exports.delivery_order = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load Item Data
    function (callback) {
      Item.find({
          hoId: user_data.headOffice
        }).populate({
          path: 'stock',
          populate: {
            path: 'unit'
          }
        })
        .exec(function (err, items) {
          if (err) return callback(err)
          locals.items = items
          callback()
        })
    },
    function (callback) {
      HeadOffice.findById(user_data.headOffice).populate({
          path: 'brands',
          populate: {
            path: 'stores'
          }
        })
        .exec(function (err, HOData) {
          if (err) return callback(err)
          locals.brands = HOData.brands
          callback()
        })
    },
    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          type: "indirect_store"
        }).sort({
          createdDate: -1
        }).populate({
          path: 'headOffice supplier deliveryItem store recipientUser storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData = DOData
          // console.log(DOData[0].deliveryItem[0])
          callback()
        })
    },
    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          isDifference: true
        }).sort({
          createdDate: -1
        }).populate({
          path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData_alert = DOData
          // console.log(DOData)
          callback()
        })
    }
  ], function (err) {
    if (err) return next(err)
    res.render('delivery_order', {
      title: appName + " | " + "Delivery Order",
      user_data: user_data,
      item: locals.item,
      brands: locals.brands,
      DOData: locals.DOData,
      DOData_alert: locals.DOData_alert,
      messages: messages,
      messages_type: messages_type
    })
  })
}


// create delivery order
exports.create_delivery_order = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (req.params.type != '') {
    if (typeof res.locals.info != 'undefined') {
      messages = res.locals.info
      messages_type = res.locals.info_type
    }
    async.parallel([
      function (callback) {
        Stock.find({
            headOffice: user_data.headOffice
          }).populate({
            path: 'item',
            populate: {
              path: 'itemCategory unit'
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, stocks) {
            if (err) return callback(err)
            locals.stocks = stocks
            // var activeStock = stocks.map(st => st.item)
            // locals.items = activeStock
            callback()
          })
      },
      function (callback) {
        HeadOffice.findById(user_data.headOffice).populate({
            path: 'brands',
            populate: {
              path: 'stores'
            }
          })
          .exec(function (err, HOData) {
            if (err) return callback(err)
            locals.brands = HOData.brands
            callback()
          })
      },
      function (callback) {
        Unit.find({
          type: "lowest"
        }, function (err, units) {
          if (err) return callback(err)
          locals.lowest_unit = units
          callback()
        })
      },
      function (callback) {
        Supplier.find({
            headOffice: user_data.headOffice
          })
          .exec(function (err, suppliers) {
            if (err) return callback(err)
            locals.suppliers = suppliers
            callback()
          })
      },
      function (callback) {
        DeliveryOrder.find({
            headOffice: user_data.headOffice,
            isDifference: true
          }).populate({
            path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData_alert = DOData
            // console.log(DOData)
            callback()
          })
      }
    ], function (err) {
      if (err) return next(err)
      res.render('create_delivery_order', {
        title: appName + " | " + "Delivery Order",
        user_data: user_data,
        dotype: req.params.dotype,
        stocks: locals.stocks,
        brands: locals.brands,
        stores: locals.stores,
        suppliers: locals.suppliers,
        lowest_unit: locals.lowest_unit,
        DOData_alert: locals.DOData_alert,
        messages: messages,
        messages_type: messages_type
      })
    })
  } else {
    req.flash('info', 'DO type required')
    req.flash('info_type', 'danger')
    if (req.params.type == 'direct_ho') {
      res.redirect('/ho/receiving_goods')
    } else {
      res.redirect('/sm/delivery_order')
    }
  }
}

// POST ho create delivery order
exports.create_delivery_order_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}

  if (req.body.dotype != 'indirect_store') {
    var file_url = req.file ? ('/image/' + res.req.file.filename) : ''
  }

  if (req.body.dotype && req.body.deliveryDate && req.body.driverName && req.body.items &&
    (((req.body.dotype != 'indirect_store') && req.body.supplierId && file_url) ||
      (req.body.dotype == 'indirect_store' && req.body.storeId))) {
    var isDifference = false
    var itemIds = []
    let promises = []
    var itemsData = JSON.parse(req.body.items)
    // console.log('itemsData',itemsData)
    itemsData.map((itm, i) => {
      console.log('item', i)
      promises.push(new Promise((resolve, reject) => {
        var DOIData = {
          item: itm.ItemId,
          stock: itm.StockId,
          // quantitySend: req.body.dotype == 'indirect_store' ? itm.Quantity: (itm.QuantitySent != '-'? itm.QuantitySent : ''),
          quantitySend: req.body.dotype == 'indirect_store' ? itm.Quantity : '',
          quantityReceive: req.body.dotype != 'indirect_store' ? itm.QuantityReceived : '',
          totalWeight: req.body.dotype != 'indirect_store' ? (itm.TotalWeight != '-' ? itm.TotalWeight : '') : '',
          unitPrice: req.body.dotype != 'indirect_store' ? (itm.UnitPrice != '-' ? itm.UnitPrice : '') : '',
          totalPrice: req.body.dotype != 'indirect_store' ? (itm.UnitPrice != '-' ? (itm.TotalWeight != '-' ? itm.UnitPrice * itm.TotalWeight : itm.UnitPrice * itm.QuantityReceived) : '') : '',
          unit: itm.UnitId,
          createdDate: Date.now(),
          createdBy: user_data._id,
        }
        // check if there are any differences
        if (req.body.dotype != 'indirect_store' && DOIData.quantitySend != DOIData.quantityReceive && DOIData.quantitySend != '') isDifference = true
        DOItem.create(DOIData, function (error, DOI) {
          if (error || DOI == null) {
            reject(error)
          } else {
            var transData = {
              hoId: user_data.headOffice,
              doItemId: DOI._id,
              stock: itm.StockId,
              quantity: req.body.dotype == 'indirect_store' ? (itm.Quantity * -1) : itm.QuantityReceived,
              type: req.body.dotype != 'indirect_store' ? 'in' : 'out',
              description: req.body.dotype == 'indirect_store' ? 'Delivery order creation' : 'Restock items',
              createdDate: Date.now(),
              createdBy: user_data._id,
            }
            console.log("444444", transData)
            StockTransaction.create(transData, function (err, newstock) {
              console.log("444444", newstock)
              if (err || newstock == null) {
                reject(err)
              } else {
                StockTransaction.aggregate([{
                    $match: {
                      hoId: user_data.headOffice._id,
                      stock: ObjectId(itm.StockId)
                    }
                  },
                  {
                    $group: {
                      _id: "$type",
                      sum: {
                        $sum: "$quantity"
                      }
                    }
                  }
                ], function (err, result) {
                  if (err) {
                    reject(err)
                  } else {
                    var currentStock = result.reduce(function (prev, cur) {
                      return prev + cur.sum
                    }, 0)
                    var stockData = {
                      // "$inc":{currentStock: parseFloat(req.body.dotype == 'indirect_store' ? (itm.Quantity*-1) : itm.QuantityReceived)},
                      currentStock: currentStock,
                      updatedDate: Date.now(),
                      updatedBy: user_data._id,
                    }
                    console.log("77777777", stockData)
                    Stock.findByIdAndUpdate(itm.StockId, stockData, function (err, stock) {
                      if (err || stock == null) {
                        reject(err)
                      } else {
                        itemIds.push(DOI._id.valueOf())
                        resolve()
                      }
                    })
                  }
                })
              }
            })
          }
        })
      }))
    })
    Promise.all(promises).then(function (results) {
      var DOData = {
        doCode: "DO" + ID(),
        headOffice: user_data.headOffice,
        deliveryDate: req.body.deliveryDate,
        driverName: req.body.driverName,
        deliveryItem: itemIds,
        type: req.body.dotype,
        isDifference: isDifference,
        recipientUser: user_data._id,
        createdDate: Date.now(),
        createdBy: user_data._id,
      }
      if (req.body.dotype == 'indirect_store') DOData.store = req.body.storeId
      else {
        DOData.supplier = req.body.supplierId
        DOData.receiptDate = Date.now()
        DOData.imageUrl = file_url
      }

      DeliveryOrder.create(DOData, function (error, DO) {
        if (error || DO == null) {
          console.log(error)
          req.flash('info', 'Create delivery order failed')
          req.flash('info_type', 'danger')
          if (req.body.dotype == 'direct_ho') res.redirect('/ho/receiving_goods')
          else res.redirect('/ho/delivery_order')
        } else {
          req.flash('info', 'Create delivery order success')
          req.flash('info_type', 'success')
          if (req.body.dotype == 'direct_ho') res.redirect('/ho/receiving_goods')
          else res.redirect('/ho/delivery_order')
        }
      })
    }).catch(err => {
      console.log(err)
      req.flash('info', 'Create delivery order failed')
      req.flash('info_type', 'danger')
      if (req.body.dotype == 'direct_ho') res.redirect('/ho/receiving_goods')
      else res.redirect('/ho/delivery_order')
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    if (req.body.dotype == 'direct_ho') res.redirect('/ho/receiving_goods')
    else res.redirect('/ho/delivery_order')
  }
}

// Details of delivery order
exports.delivery_order_detail = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    function (callback) {
      DeliveryOrder.findOne({
          _id: req.params.doId
        }).populate({
          path: 'deliveryItem headOffice supplier store recipientUser',
          populate: {
            path: 'item unit brand'
          }
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData = DOData
          callback()
        })
    },
    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          isDifference: true
        }).populate({
          path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData_alert = DOData
          // console.log(DOData)
          callback()
        })
    }
  ], function (err) {
    if (err) return next(err)
    res.render('delivery_order_det', {
      title: appName + " | " + "Delivery Order Details",
      user_data: user_data,
      lowest_unit: locals.lowest_unit,
      DOData: locals.DOData,
      DOData_alert: locals.DOData_alert,
      messages: messages,
      messages_type: messages_type
    })
  })
}


// Invoice delivery order
exports.invoice_delivery_order = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    function (callback) {
      DeliveryOrder.findOne({
          _id: req.params.DOId
        }).populate({
          path: 'deliveryItem headOffice supplier store storeWarehouse storeCasheer',
          populate: {
            path: 'item unit brand'
          }
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData = DOData
          callback()
        })
    },
  ], function (err) {
    if (err) return next(err)
    var fullUrl = req.protocol + "://" + req.get('host') + "/sm/receive_item/" + locals.DOData.doCode
    QRCode.toDataURL(locals.DOData.doCode, function (err, url) {
      if (err) console.log('error: ' + err)
      res.render('invoice_delivery_order', {
        title: appName + " | " + "Invoice Delivery Order",
        user_data: user_data,
        lowest_unit: locals.lowest_unit,
        DOData: locals.DOData,
        messages: messages,
        messages_type: messages_type,
        qrcode: url
      })
    })
  })
}

//GET delivery_order_print
exports.delivery_order_print = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    function (callback) {
      DeliveryOrder.findOne({
          _id: req.params.id
        }).sort({
          createdDate: -1
        }).populate({
          path: 'headOffice supplier deliveryItem store recipientUser storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData = {
            type: DOData.type,
            driverName: DOData.driverName,
            recipientUser: DOData.recipientUser,
            supplier: DOData.supplier,
            headOffice: DOData.headOffice,
            store: DOData.store,
            deliveryDate: DOData.deliveryDate,
            deliveryItem: DOData.deliveryItem,
            totalBill: DOData.deliveryItem.map(di=>di.totalPrice).reduce((acc, curr) => acc + curr)
          }
          // console.log(DOData[0].deliveryItem[0])
          callback()
        })
    },
  ], function (err) {
    if (err) return next(err)
    DeliveryOrder.find({
        headOffice: user_data.headOffice,
        isDifference: true
      }).populate({
        path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
        populate: {
          path: 'item stockRef unit brand',
          populate: {
            path: 'itemCategory'
          }
        }
      }).sort({
        createdDate: -1
      })
      .exec(function (err, DOData_alert) {
        if (err || DOData_alert == null) {
          return console.log(err)
        } else {
          locals.DOData_alert = DOData_alert
          res.render('delivery_order_print', {
            title: appName + " | " + "Delivery Order Print Details",
            user_data: user_data,
            lowest_unit: locals.lowest_unit,
            DOData: locals.DOData,
            DOData_alert: locals.DOData_alert,
            showPrice: req.params.showprice == 'true' ? true : false,
            messages: messages,
            messages_type: messages_type,
          })
        }
      })
  })
}


// POST sm delete DO
exports.delete_restock_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.DOId) {
    DeliveryOrder.findByIdAndRemove(req.params.DOId).populate({
        path: 'deliveryItem',
        populate: {
          path: 'stockRef'
        }
      })
      .exec(function (error, DO) {
        if (error || DO == null) {
          console.log(error)
          req.flash('info', 'Delete delivery order failed')
          req.flash('info_type', 'danger')
          res.redirect('/ho/delivery_order')
        } else {
          let promises = []
          DO.deliveryItem.map((doi, i) => {
            promises.push(new Promise((resolve, reject) => {
              DOItem.findByIdAndRemove(doi._id, function (error, doitem) {
                if (error || doitem == null) {
                  console.log(error)
                  reject(error)
                } else {
                  StockTransaction.deleteMany({
                    doItemId: doitem._id
                  }, function (error, trans) {
                    if (error || trans == null) {
                      console.log(error)
                      reject(error)
                    } else {
                      StockTransaction.aggregate([{
                          $match: {
                            hoId: user_data.headOffice,
                            stock: ObjectId(doitem.stock)
                          }
                        },
                        {
                          $group: {
                            _id: "$type",
                            sum: {
                              $sum: "$quantity"
                            }
                          }
                        }
                      ], function (err, result) {
                        if (err) {
                          reject(err)
                        } else {
                          var currentStock = result.reduce(function (prev, cur) {
                            return prev + cur.sum
                          }, 0)
                          var stockData = {
                            currentStock: currentStock,
                            updatedDate: Date.now(),
                            updatedBy: user_data._id,
                          }
                          Stock.findByIdAndUpdate(doitem.stock, stockData, function (err, stock) {
                            if (err || stock == null) {
                              reject(err)
                            } else {
                              resolve()
                            }
                          })
                        }
                      })
                    }
                  })
                }
              })
            }))
          })
          Promise.all(promises).then(function (results) {
            req.flash('info', 'Delete Goods success')
            req.flash('info_type', 'success')
            res.redirect('/ho/receiving_goods')
          }).catch(err => {
            console.log(err)
            req.flash('info', 'Delete Goods success')
            req.flash('info_type', 'danger')
            res.redirect('/ho/receiving_goods')
          })
        }
      })
  } else {
    req.flash('info', 'Delivery order id required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/delivery_order')
  }
}

// POST sm delete DO
exports.delete_do_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var Dotype_data = req.params.DOtype
  console.log("sssssss", Dotype_data)
  if (req.params.DOId) {
    DeliveryOrder.findByIdAndRemove(req.params.DOId).populate({
        path: 'deliveryItem',
        populate: {
          path: 'stockRef'
        }
      })
      .exec(function (error, DO) {
        if (error || DO == null) {
          console.log(error)
          req.flash('info', 'Delete delivery order failed')
          req.flash('info_type', 'danger')
          if (Dotype_data == "indirect_store") {
            res.redirect('/ho/delivery_order')
          }
          if (Dotype_data == "direct_ho") {
            res.redirect('/ho/receiving_goods')
          }
        } else {
          let promises = []
          DO.deliveryItem.map((doi, i) => {
            promises.push(new Promise((resolve, reject) => {
              DOItem.findByIdAndRemove(doi._id, function (error, doitem) {
                if (error || doitem == null) {
                  console.log(error)
                  reject(error)
                } else {
                  StockTransaction.deleteMany({
                    doItemId: doitem._id
                  }, function (error, trans) {
                    if (error || trans == null) {
                      console.log(error)
                      reject(error)
                    } else {
                      StockTransaction.aggregate([{
                          $match: {
                            hoId: user_data.headOffice._id,
                            stock: ObjectId(doitem.stock)
                          }
                        },
                        {
                          $group: {
                            _id: "$type",
                            sum: {
                              $sum: "$quantity"
                            }
                          }
                        }
                      ], function (err, result) {
                        if (err) {
                          reject(err)
                        } else {
                          var currentStock = result.reduce(function (prev, cur) {
                            return prev + cur.sum
                          }, 0)
                          var stockData = {
                            currentStock: currentStock,
                            updatedDate: Date.now(),
                            updatedBy: user_data._id,
                          }
                          Stock.findByIdAndUpdate(doitem.stock, stockData, function (err, stock) {
                            if (err || stock == null) {
                              reject(err)
                            } else {
                              resolve()
                            }
                          })
                        }
                      })
                    }
                  })
                }
              })
            }))
          })
          Promise.all(promises).then(function (results) {
            req.flash('info', 'Delete delivery order success')
            req.flash('info_type', 'success')
            if (Dotype_data == "indirect_store") {
              res.redirect('/ho/delivery_order')
            }
            if (Dotype_data == "direct_ho") {
              res.redirect('/ho/receiving_goods')
            }
          }).catch(err => {
            console.log(err)
            req.flash('info', 'Delete delivery order item failed')
            req.flash('info_type', 'danger')
            if (Dotype_data == "indirect_store") {
              res.redirect('/ho/delivery_order')
            }
            if (Dotype_data == "direct_ho") {
              res.redirect('/ho/receiving_goods')
            }
          })
        }
      })
  } else {
    req.flash('info', 'Delivery order id required')
    req.flash('info_type', 'danger')
    if (Dotype_data == "indirect_store") {
      res.redirect('/ho/delivery_order')
    }
    if (Dotype_data == "direct_ho") {
      res.redirect('/ho/receiving_goods')
    }
  }
}

// GET API ho adjust doitem process
exports.adjust_doitem_process = function (req, res, next) {
  Session.validate(req.session.key, 'ho', function (error, user) {
    var user_data = req.session.user_data
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.doitemid && req.body.itemid && req.body.quantity && req.body.type && req.body.stockid) {
        //find Adjust before (if do was adjusted before)
        var transData = {
          hoId: user_data.headOffice,
          doItemId: req.body.doitemid,
          stock: req.body.stockid,
          quantity: req.body.quantity,
          type: (req.body.type == "IN" ? "in" : "out"),
          description: 'Re-adjustment DO item',
          createdDate: Date.now(),
          createdBy: user_data._id,
        }
        console.log(transData)
        StockTransaction.create(transData, function (err, newstock) {
          if (err || newstock == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            DOItem.findById(req.body.doitemid, function (err, DO) {
              if (err) {
                var err = {
                  status: 400,
                  message: err
                }
                return res.send(JSON.stringify(err))
              } else if (DO.quantityAdjustment != null) {
                var stockData = {
                  "$inc": {
                    currentStock: parseFloat(DO.adjustmentType == 'Plus' ? DO.quantityAdjustment * -1 : DO.quantityAdjustment)
                  }
                }
                // console.log(stockData)
                Stock.findByIdAndUpdate(req.body.stockid, stockData, function (error, item) {
                  if (error || item == null) {
                    var err = {
                      status: 400,
                      message: error
                    }
                    return res.send(JSON.stringify(err))
                  } else {
                    var DOItemData = {
                      quantityAdjustment: req.body.quantity,
                      adjustmentType: req.body.type == 'out' ? 'Minus' : 'Plus'
                    }
                    DOItem.findOneAndUpdate({
                        _id: req.body.doitemid
                      }, DOItemData).populate('unit item')
                      .exec(function (error, item) {
                        if (err) {
                          var err = {
                            status: 400,
                            message: err
                          }
                          console.log('error disini 1')
                          return res.send(JSON.stringify(err))
                        }
                        if (item == null) {
                          var err = {
                            status: 400,
                            message: 'DO Item not found'
                          }
                          console.log('error disini 2')
                          return res.send(JSON.stringify(err))
                        } else {
                          var stockData = {
                            "$inc": {
                              currentStock: parseFloat(req.body.type == 'out' ? req.body.quantity * -1 : req.body.quantity)
                            }
                          }
                          Stock.findByIdAndUpdate(req.body.stockid, stockData, function (error, item) {
                            if (error || item == null) {
                              var err = {
                                status: 400,
                                message: error
                              }
                              console.log('error disini 3')
                              return res.send(JSON.stringify(err))
                            } else {
                              var ret = {
                                status: 200,
                                data: {
                                  adjustmentType: req.body.type == 'out' ? 'Minus' : 'Plus',
                                  adjustmentVal: req.body.quantity
                                }
                              }
                              return res.send(JSON.stringify(ret))
                            }
                          })
                        }
                      })
                  }
                })
              } else {
                var transData = {
                  hoId: user_data.headOffice,
                  doItemId: req.body.doitemid,
                  stock: req.body.stockid,
                  quantity: req.body.quantity,
                  type: (req.body.type == "IN" ? "in" : "out"),
                  description: 'Adjustment DO item',
                  createdDate: Date.now(),
                  createdBy: user_data._id,
                }
                console.log(transData)
                StockTransaction.create(transData, function (err, newstock) {
                  if (err || newstock == null) {
                    console.log(error)
                    var err = {
                      status: 400,
                      message: error
                    }
                    return res.send(JSON.stringify(err))
                  } else {
                    var DOItemData = {
                      quantityAdjustment: req.body.quantity,
                      adjustmentType: req.body.type == 'out' ? 'Minus' : 'Plus'
                    }
                    DOItem.findOneAndUpdate({
                        _id: req.body.doitemid
                      }, DOItemData).populate('unit item')
                      .exec(function (error, item) {
                        if (err) {
                          var err = {
                            status: 400,
                            message: err
                          }
                          return res.send(JSON.stringify(err))
                        }
                        if (item == null) {
                          var err = {
                            status: 400,
                            message: 'DO Item not found'
                          }
                          return res.send(JSON.stringify(err))
                        } else {
                          var stockData = {
                            "$inc": {
                              currentStock: parseFloat(req.body.type == 'out' ? req.body.quantity * -1 : req.body.quantity)
                            }
                          }
                          Stock.findByIdAndUpdate(req.body.stockid, stockData, function (error, item) {
                            if (error || item == null) {
                              var err = {
                                status: 400,
                                message: error
                              }
                              return res.send(JSON.stringify(err))
                            } else {
                              var ret = {
                                status: 200,
                                data: {
                                  adjustmentType: req.body.type == 'out' ? 'Minus' : 'Plus',
                                  adjustmentVal: req.body.quantity
                                }
                              }
                              return res.send(JSON.stringify(ret))
                            }
                          })
                        }
                      })
                  }
                })
              }
            })
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All data required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}


//unit
exports.unit = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  Unit.find({
      hoId: user_data.headOffice,
      type: "lowest"
    }).sort({
      createdDate: -1
    }).populate('highestUnit createdBy updatedBy')
    .exec(function (err, units) {
      if (err || units == null) {
        return console.log(err)
      } else {
        locals.units = units
        DeliveryOrder.find({
            headOffice: user_data.headOffice,
            isDifference: true
          }).populate({
            path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err || DOData == null) {
              return console.log(err)
            } else {
              locals.DOData_alert = DOData
              res.render('unit', {
                title: appName + " | " + "Units",
                user_data: user_data,
                units: locals.units,
                DOData_alert: locals.DOData_alert,
                messages: messages,
                messages_type: messages_type
              })
            }
          })
      }
    })
}


// POST ho add unit
exports.add_unit_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.unit_type_opt && (req.body.name || (req.body.highest_name && req.body.conversion && req.body.lowest_unit_ref))) {
    var unitData = {
      name: req.body.unit_type_opt == "lowest" ? req.body.name : req.body.highest_name + "/" + req.body.conversion,
      type: req.body.unit_type_opt,
      label: req.body.unit_type_opt == "lowest" ? null : req.body.highest_name,
      lowestUnit: req.body.unit_type_opt == "lowest" ? null : req.body.lowest_unit_ref,
      HTLConversion: req.body.conversion,
      hoId: user_data.headOffice,
      createdDate: Date.now(),
      createdBy: user_data._id,
    }
    Unit.create(unitData, function (error, unit) {
      if (error || unit == null) {
        if (error.errmsg.includes('duplicate key')) {
          req.flash('info', 'Create unit failed, duplicate name')
        } else {
          req.flash('info', 'Create unit failed ')
        }
        req.flash('info_type', 'danger')
        res.redirect('/ho/' + req.body.origin_path)
      } else {
        if (req.body.unit_type_opt == "lowest") {
          req.flash('info', 'Create unit success')
          req.flash('info_type', 'success')
          res.redirect('/ho/' + req.body.origin_path)
        } else {
          Unit.findByIdAndUpdate(req.body.lowest_unit_ref, {
            $push: {
              highestUnit: unit._id
            }
          }, function (error, unit) {
            if (error || unit == null) {
              req.flash('info', 'Update refered unit failed')
              req.flash('info_type', 'danger')
              res.redirect('/ho/' + req.body.origin_path)
            } else {
              req.flash('info', 'Create unit success')
              req.flash('info_type', 'success')
              res.redirect('/ho/' + req.body.origin_path)
            }
          })
        }
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/delivery_order')
  }
}

// GET API ho single unit
exports.single_unit = function (req, res, next) {
  Session.validate(req.session.key, 'ho', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id) {
        Unit.findOne({
            _id: req.params.id
          }, {
            '__v': 0
          })
          .exec(function (error, unit) {
            if (err) {
              var err = {
                status: 400,
                message: err
              }
              return res.send(JSON.stringify(err))
            }
            if (unit == null) {
              var err = {
                status: 400,
                message: 'Unit not found'
              }
              return res.send(JSON.stringify(err))
            } else {
              var ret = {
                status: 200,
                data: {
                  unit_id: unit._id,
                  name: unit.name,
                }
              }
              // console.log(ret)
              return res.send(JSON.stringify(ret))
            }
          })
      } else {
        var err = {
          status: 400,
          message: 'Unit id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// POST sm update unit
exports.update_unit_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var user_data = req.session.user_data
  if (req.body.unit_id && req.body.name) {
    var UnitData = {
      name: req.body.name,
      updatedDate: Date.now(),
      updatedBy: user_data._id,
    }
    Unit.findByIdAndUpdate(req.body.unit_id, UnitData, function (error, unit) {
      if (error || unit == null) {
        console.log(error)
        req.flash('info', 'Update unit failed')
        req.flash('info_type', 'danger')
        res.redirect('/ho/unit')
      } else {
        req.flash('info', 'Update unit success')
        req.flash('info_type', 'success')
        res.redirect('/ho/unit')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/unit')
  }
}

// POST sm delete unit
exports.delete_unit_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.id) {
    Unit.findByIdAndRemove(req.params.id, function (error, unit) {
      if (error || unit == null) {
        console.log(error)
        req.flash('info', 'Delete unit failed')
        req.flash('info_type', 'danger')
        res.redirect('/ho/unit')
      } else {
        req.flash('info', 'Delete unit success')
        req.flash('info_type', 'success')
        res.redirect('/ho/unit')
      }
    })
  } else {
    req.flash('info', 'Unit id required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/unit')
  }
}


//items
exports.item = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  var item_det = []
  var promises = []
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  Item.find({
      hoId: user_data.headOffice
    }).sort({
      createdDate: -1
    }).populate({
      path: 'itemCategory unit createdBy updatedBy',
      populate: {
        path: 'highestUnit'
      }
    })
    .exec(function (err, items) {
      if (err || items == null) {
        return false
      } else {
        locals.items = items
        Unit.find({
            type: "lowest"
          }).populate('highestUnit')
          .exec(function (err, units) {
            if (err || units == null) {
              return console.log(err)
            } else {
              locals.units = units
              ItemCategory.find({
                  hoId: user_data.headOffice
                })
                .exec(function (err, categories) {
                  if (err || categories == null) {
                    return console.log(err)
                  } else {
                    locals.categories = categories
                    DeliveryOrder.find({
                        headOffice: user_data.headOffice,
                        isDifference: true
                      }).populate({
                        path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
                        populate: {
                          path: 'item stockRef unit brand',
                          populate: {
                            path: 'itemCategory'
                          }
                        }
                      }).sort({
                        createdDate: -1
                      })
                      .exec(function (err, DOData) {
                        if (err || DOData == null) {
                          return console.log(err)
                        } else {
                          locals.DOData_alert = DOData
                          res.render('item', {
                            title: appName + " | " + "Items",
                            user_data: user_data,
                            items: locals.items,
                            units: locals.units,
                            DOData_alert: locals.DOData_alert,
                            categories: locals.categories,
                            messages: messages,
                            messages_type: messages_type
                          })
                        }
                      })

                  }
                })
            }
          })
      }
    })
}


// POST sm create item
exports.create_item_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var user_data = req.session.user_data
  if (req.body.category_id && req.body.item_code_prefix && req.body.name && req.body.unit_id) {
    async.parallel([
      function (callback) {
        ItemCategory.findOneAndUpdate({
            'prefix': req.body.item_code_prefix
          }, {
            '$inc': {
              lastIndex: 1
            }
          })
          .exec(function (err, category) {
            if (err) return callback(err)
            locals.LastItemPrefixNum = category.lastIndex
            callback()
          })
      },
    ], function (err) {
      if (err) return next(err)
      var pad = 5
      var ItemData = {
        name: req.body.name,
        itemCategory: req.body.category_id,
        itemCodeNumber: '' + Array(Math.max(pad - String(locals.LastItemPrefixNum).length + 1, 0)).join(0) + locals.LastItemPrefixNum,
        unit: req.body.unit_id,
        hoId: user_data.headOffice,
        createdDate: Date.now(),
        createdBy: user_data._id,
      }
      Item.create(ItemData, function (error, item) {
        if (error || item == null) {
          console.log(error)
          req.flash('info', 'Create item failed')
          req.flash('info_type', 'danger')
          res.redirect('/ho/item')
        } else {
          req.flash('info', 'Create item success')
          req.flash('info_type', 'success')
          res.redirect('/ho/item')

        }
      })
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/item')
  }
}


// GET API ho single item
exports.single_item = function (req, res, next) {
  Session.validate(req.session.key, 'ho', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.itemId) {
        Item.findOne({
            _id: req.params.itemId
          }, {
            '__v': 0
          }).populate('unit stock createdBy updatedBy')
          .exec(function (error, item) {
            if (err) {
              var err = {
                status: 400,
                message: err
              }
              return res.send(JSON.stringify(err))
            }
            if (item == null) {
              var err = {
                status: 400,
                message: 'Item not found'
              }
              return res.send(JSON.stringify(err))
            } else {
              var ret = {
                status: 200,
                data: {
                  item_id: item._id,
                  name: item.name,
                }
              }
              // console.log(ret)
              return res.send(JSON.stringify(ret))
            }
          })
      } else {
        var err = {
          status: 400,
          message: 'Item id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// POST sm update item
exports.update_item_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var user_data = req.session.user_data
  if (req.body.item_id && req.body.name) {
    var ItemData = {
      name: req.body.name,
      updatedDate: Date.now(),
      updatedBy: user_data._id,
    }
    Item.findByIdAndUpdate(req.body.item_id, ItemData, function (error, item) {
      if (error || item == null) {
        console.log(error)
        req.flash('info', 'Update item failed')
        req.flash('info_type', 'danger')
        res.redirect('/ho/item')
      } else {
        req.flash('info', 'Update item success')
        req.flash('info_type', 'success')
        res.redirect('/ho/item')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/item')
  }
}

// POST sm delete item
exports.delete_item_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.itemId) {
    Item.findByIdAndRemove(req.params.itemId, function (error, item) {
      if (error || item == null) {
        console.log(error)
        req.flash('info', 'Delete item failed')
        req.flash('info_type', 'danger')
        res.redirect('/ho/item')
      } else {
        StockTransaction.deleteMany({
          item: item._id
        }, function (error, stocks) {
          if (error || stocks == null) {
            console.log(error)
            req.flash('info', 'Delete stock of item failed')
            req.flash('info_type', 'danger')
            res.redirect('/ho/item')
          } else {
            req.flash('info', 'Delete item success')
            req.flash('info_type', 'success')
            res.redirect('/ho/item')
          }
        })
      }
    })
  } else {
    req.flash('info', 'Item id required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/item')
  }
}

//stock
exports.active_stock = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    function (callback) {
      Stock.find({
          headOffice: user_data.headOffice
        }).populate({
          path: 'item',
          populate: {
            path: 'itemCategory unit'
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, stocks) {
          if (err) return callback(err)
          locals.stocks = stocks
          var activeStockIds = stocks.map(st => st.item)
          Item.find({
              _id: {
                $nin: activeStockIds
              }
            }).populate({
              path: 'itemCategory unit',
              populate: {
                path: 'highestUnit'
              }
            })
            .exec(function (err, items) {
              if (err) return callback(err)
              locals.items = items
              callback()
            })
        })
    },
    function (callback) {
      Unit.find({
          type: "lowest"
        }).populate('highestUnit')
        .exec(function (err, units) {
          if (err) return callback(err)
          locals.units = units
          callback()
        })
    },
    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          isDifference: true
        }).populate({
          path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData_alert = DOData
          // console.log(DOData)
          callback()
        })
    }
  ], function (err) {
    if (err) return next(err)
    res.render('active_stock', {
      title: appName + " | " + "Active Stock",
      user_data: user_data,
      stocks: locals.stocks,
      items: locals.items,
      messages: messages,
      DOData_alert: locals.DOData_alert,
      messages_type: messages_type
    })
  })
}


//stock
exports.stock = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    function (callback) {
      Stock.find({
          headOffice: user_data.headOffice
        }).populate({
          path: 'item',
          populate: {
            path: 'itemCategory unit'
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, stocks) {
          if (err) return callback(err)
          locals.stocks = stocks
          var activeStockIds = stocks.map(st => st.item)
          Item.find({
              _id: {
                $nin: activeStockIds
              }
            }).populate({
              path: 'itemCategory unit',
              populate: {
                path: 'highestUnit'
              }
            })
            .exec(function (err, items) {
              if (err) return callback(err)
              locals.items = items
              callback()
            })
        })
    },
    function (callback) {
      Unit.find({
          type: "lowest"
        }).populate('highestUnit')
        .exec(function (err, units) {
          if (err) return callback(err)
          locals.units = units
          callback()
        })
    },
    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          isDifference: true
        }).populate({
          path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData_alert = DOData
          // console.log(DOData)
          callback()
        })
    }
  ], function (err) {
    if (err) return next(err)
    res.render('stock', {
      title: appName + " | " + "Stock",
      user_data: user_data,
      stocks: locals.stocks,
      items: locals.items,
      messages: messages,
      DOData_alert: locals.DOData_alert,
      messages_type: messages_type
    })
  })
}


// POST ho create stock
exports.create_stock_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.selected_item && req.body.initial_stock) {
    var stockData = {
      item: req.body.selected_item,
      ownerType: user_data.role == 'store' ? 'store' : 'ho',
      headOffice: user_data.headOffice,
      store: user_data.role == 'store' ? user_data.store : null,
      initialStock: req.body.initial_stock,
      currentStock: req.body.initial_stock,
      notes: req.body.notes,
      createdDate: Date.now(),
      createdBy: user_data._id,
    }
    Stock.create(stockData, function (error, stock) {
      if (error || stock == null) {
        console.log(error)
        req.flash('info', 'Create stock failed')
        req.flash('info_type', 'danger')
        res.redirect('/ho/stock')
      } else {
        var transData = {
          hoId: user_data.headOffice,
          stock: stock._id,
          quantity: req.body.initial_stock,
          type: 'initial',
          description: 'Create initial stock',
          createdDate: Date.now(),
          createdBy: user_data._id,
        }
        StockTransaction.create(transData, function (err, trans) {
          if (error || trans == null) {
            console.log(error)
            req.flash('info', 'Create stock transaction failed')
            req.flash('info_type', 'danger')
            res.redirect('/ho/stock')
          } else {
            req.flash('info', 'Create stock success')
            req.flash('info_type', 'success')
            res.redirect('/ho/stock')
          }
        })
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/stock')
  }
}


// POST sm delete stock
exports.delete_stock_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.stockId) {
    Stock.findByIdAndRemove(req.params.stockId, function (error, stock) {
      if (error || stock == null) {
        console.log(error)
        req.flash('info', 'Delete stock failed')
        req.flash('info_type', 'danger')
        res.redirect('/ho/stock')
      } else {
        DOItem.deleteMany({
          stock: stock._id
        }, function (error, doi) {
          if (error || doi == null) {
            console.log(error)
            req.flash('info', 'Delete DO item relation failed')
            req.flash('info_type', 'danger')
            res.redirect('/ho/stock')
          } else {
            StockTransaction.deleteMany({
              stock: stock._id
            }, function (error, stocks) {
              if (error || stocks == null) {
                console.log(error)
                req.flash('info', 'Delete stock transaction failed')
                req.flash('info_type', 'danger')
                res.redirect('/ho/stock')
              } else {
                req.flash('info', 'Delete stock success')
                req.flash('info_type', 'success')
                res.redirect('/ho/stock')
              }
            })
          }
        })
      }
    })
  } else {
    req.flash('info', 'Stock id required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/stock')
  }
}


// GET API ho single stock
exports.single_stock = function (req, res, next) {
  Session.validate(req.session.key, 'ho', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.stockId) {
        Stock.findOne({
            _id: req.params.stockId
          }, {
            '__v': 0
          }).populate('createdBy updatedBy')
          .exec(function (error, stock) {
            if (err) {
              var err = {
                status: 400,
                message: err
              }
              return res.send(JSON.stringify(err))
            }
            if (stock == null) {
              var err = {
                status: 400,
                message: 'Stock not found'
              }
              return res.send(JSON.stringify(err))
            } else {
              var ret = {
                status: 200,
                data: {
                  _id: stock._id,
                  initialStock: stock.initialStock,
                  notes: stock.notes
                }
              }
              return res.send(JSON.stringify(ret))
            }
          })
      } else {
        var err = {
          status: 400,
          message: 'Supplier id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}


// POST ho update stock
exports.update_stock_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.stock_id && req.body.initial_stock) {
    var transData = {
      quantity: req.body.initial_stock,
      updatedDate: Date.now(),
      updatedBy: user_data._id,
    }
    StockTransaction.findOneAndUpdate({
      stock: req.body.stock_id,
      type: 'initial'
    }, transData, function (err, trans) {
      if (err || trans == null) {
        req.flash('info', 'Update stock transaction failed')
        req.flash('info_type', 'danger')
        res.redirect('/ho/stock')
      } else {
        StockTransaction.aggregate([{
            $match: {
              hoId: user_data.headOffice,
              stock: ObjectId(req.body.stock_id)
            }
          },
          {
            $group: {
              _id: "$type",
              sum: {
                $sum: "$quantity"
              }
            }
          }
        ], function (err, result) {
          if (err) {
            reject(err)
          } else {
            var currentStock = result.reduce(function (prev, cur) {
              return prev + cur.sum
            }, 0)
            var stockData = {
              initialStock: req.body.initial_stock,
              currentStock: currentStock,
              notes: req.body.notes,
              updatedDate: Date.now(),
              updatedBy: user_data._id,
            }
            Stock.findOneAndUpdate({
              _id: req.body.stock_id
            }, stockData, function (error, stock) {
              if (error || stock == null) {
                console.log(error)
                req.flash('info', 'Update stock failed')
                req.flash('info_type', 'danger')
                res.redirect('/ho/stock')
              } else {
                req.flash('info', 'Update stock success')
                req.flash('info_type', 'success')
                res.redirect('/ho/stock')
              }
            })
          }
        })
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/stock')
  }
}


//Spoil Function
exports.spoil_stock_process = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  Stock.findOne({
    _id: req.body.stock_id
  }, function (err, stock) {
    if (err || stock == null) {
      var err = {
        status: 400,
        message: err
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.quantity <= stock.currentStock) {
        //Decreasing Stock
        var StockData = {
          "$inc": {
            currentStock: parseFloat(req.body.quantity) * -1
          }
        }
        Stock.findByIdAndUpdate(req.body.stock_id, StockData, function (err, stock) {
          if (err || stock == null) {
            console.log(err)
            req.flash('info', 'Failed to spoil item stock')
            req.flash('info_type', 'danger')
            res.redirect('/ho/receiving_goods')
          } else {
            var transData = {
              hoId: user_data.headOffice,
              stock: stock._id,
              quantity: parseFloat(req.body.quantity) * -1,
              type: 'out',
              description: 'Spoil Item : ' + req.body.reason,
              createdDate: Date.now(),
              createdBy: user_data._id,
            }
            StockTransaction.create(transData, function (error, trans) {
              if (error || trans == null) {
                req.flash('info', 'Failed to spoil item stock')
                req.flash('info_type', 'danger')
                res.redirect('/ho/receiving_goods')
              } else {
                req.flash('info', 'Spoile item successful')
                req.flash('info_type', 'success')
                res.redirect('/ho/receiving_goods')
              }
            })
          }
        })
      } else {
        req.flash('info', 'Spoil quantity can\'t exceed the current stock')
        req.flash('info_type', 'danger')
        res.redirect('/sm/stock')
      }
    }
  })
}

//stock transaction
exports.stock_transaction = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    function (callback) {
      StockTransaction.find({
          hoId: user_data.headOffice
        }).populate({
          path: 'stock doItemId createdBy updatedBy',
          populate: {
            path: 'item',
            populate: {
              path: 'unit itemCategory'
            }
          }
        })
        .sort({
          createdDate: -1
        }).exec(function (err, trans) {
          if (err) return callback(err)
          locals.trans = trans
          callback()
        })
    },
    function (callback) {
      Item.find({
          hoId: user_data.headOffice
        }).populate({
          path: 'itemCategory unit stock',
          populate: {
            path: 'highestUnit'
          }
        })
        .exec(function (err, items) {
          if (err) return callback(err)
          locals.items = items
          callback()
        })
    },
    function (callback) {
      Unit.find({
          type: "lowest"
        }).populate('highestUnit')
        .exec(function (err, units) {
          if (err) return callback(err)
          locals.units = units
          callback()
        })
    },
    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          isDifference: true
        }).populate({
          path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData_alert = DOData
          // console.log(DOData)
          callback()
        })
    }
  ], function (err) {
    if (err) return next(err)
    res.render('stock_transaction', {
      title: appName + " | " + "Stock History",
      user_data: user_data,
      trans: locals.trans,
      items: locals.items,
      units: locals.units,
      DOData_alert: locals.DOData_alert,
      messages: messages,
      messages_type: messages_type
    })
  })
}


// // POST ho restock item
// exports.restock_item_process = function (req, res, next) {
//   auth_redirect(req, res)
//   var user_data = req.session.user_data
//   if (req.body.selected_item && req.body.quantity && req.body.cnv && req.body.unit_id){
//     var ItemData={
//       "$inc":{currentStock: req.body.quantity * req.body.cnv}
//     }
//     Item.findByIdAndUpdate(req.body.selected_item, ItemData, function (error, item) {
//       if (error || item==null) {
//         console.log(error)
//         req.flash('info', 'Restock item failed')
//         req.flash('info_type', 'danger')
//         res.redirect('/ho/stock')
//       } else {
//         var stockData={
//           hoId: user_data.headOffice,
//           item: item._id,
//           quantity: req.body.quantity * req.body.cnv,
//           updatedStock: item.currentStock + (req.body.quantity * req.body.cnv),
//           type: 'in',
//           description: 'Restock item',
//           createdDate: Date.now(),
//           createdBy: user_data._id,
//         }
//         StockTransaction.create(stockData, function (err, stock) {
//           if (error || stock==null) {
//             console.log(error)
//             req.flash('info', 'Restock item failed')
//             req.flash('info_type', 'danger')
//             res.redirect('/ho/stock')
//           } else {
//             req.flash('info', 'Restock item success')
//             req.flash('info_type', 'success')
//             res.redirect('/ho/stock')
//           }
//         })
//       }
//     })
//   } else {
//     req.flash('info', 'All fields required')
//     req.flash('info_type', 'danger')
//     res.redirect('/ho/stock')
//   }
// }


//item category
exports.item_category = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load Item Data
    function (callback) {
      ItemCategory.find({
          hoId: user_data.headOffice
        }).sort({
          createdDate: -1
        }).populate('createdBy updatedBy')
        .exec(function (err, categories) {
          if (err) return callback(err)
          locals.categories = categories
          callback()
        })
    },
    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          isDifference: true
        }).populate({
          path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData_alert = DOData
          // console.log(DOData)
          callback()
        })
    }
  ], function (err) {
    if (err) return next(err)
    res.render('item_category', {
      title: "Item Category",
      user_data: user_data,
      categories: locals.categories,
      messages: messages,
      DOData_alert: locals.DOData_alert,
      messages_type: messages_type
    })
  })
}


// POST ho create item category
exports.create_item_category_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name) {
    var catData = {
      name: req.body.name,
      prefix: req.body.prefix,
      lastIndex: 0,
      hoId: user_data.headOffice,
      createdDate: Date.now(),
      createdBy: user_data._id,
    }
    ItemCategory.create(catData, function (error, category) {
      if (error || category == null) {
        if (error.errmsg.includes('duplicate key')) {
          req.flash('info', 'Create unit failed, duplicate prefix')
        } else {
          req.flash('info', 'Create item category failed')
        }
        req.flash('info_type', 'danger')
        res.redirect('/ho/item_category')
      } else {
        req.flash('info', 'Create item category success')
        req.flash('info_type', 'success')
        res.redirect('/ho/item_category')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/item_category')
  }
}



// GET API ho single item category
exports.single_item_category = function (req, res, next) {
  Session.validate(req.session.key, 'ho', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.categoryId) {
        ItemCategory.findOne({
            _id: req.params.categoryId
          }, {
            '__v': 0
          }).populate('createdBy updatedBy')
          .exec(function (error, category) {
            if (err) {
              var err = {
                status: 400,
                message: err
              }
              return res.send(JSON.stringify(err))
            }
            if (category == null) {
              var err = {
                status: 400,
                message: 'Item category not found'
              }
              return res.send(JSON.stringify(err))
            } else {
              var ret = {
                status: 200,
                data: {
                  _id: category._id,
                  name: category.name,
                  prefix: category.prefix
                }
              }
              return res.send(JSON.stringify(ret))
            }
          })
      } else {
        var err = {
          status: 400,
          message: 'Supplier id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}


// POST ho update item category
exports.update_item_category_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.category_id && req.body.name && req.body.prefix) {
    var catData = {
      name: req.body.name,
      prefix: req.body.prefix,
      updatedDate: Date.now(),
      updatedBy: user_data._id,
    }
    ItemCategory.findOneAndUpdate({
      _id: req.body.category_id
    }, catData, function (error, category) {
      if (error || category == null) {
        console.log(error)
        req.flash('info', 'Update item category failed')
        req.flash('info_type', 'danger')
        res.redirect('/ho/item_category')
      } else {
        req.flash('info', 'Update item category success')
        req.flash('info_type', 'success')
        res.redirect('/ho/item_category')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/item_category')
  }
}

// POST sm delete item category
exports.delete_item_category_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.categoryId) {
    ItemCategory.findByIdAndRemove(req.params.categoryId, function (error, category) {
      if (error || category == null) {
        console.log(error)
        req.flash('info', 'Delete item category failed')
        req.flash('info_type', 'danger')
        res.redirect('/ho/item_category')
      } else {
        req.flash('info', 'Delete item category success')
        req.flash('info_type', 'success')
        res.redirect('/ho/item_category')
      }
    })
  } else {
    req.flash('info', 'Category id required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/item_category')
  }
}


//supplier
exports.supplier = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load Supplier Data
    function (callback) {
      Supplier.find({
          headOffice: user_data.headOffice
        }).sort({
          createdDate: -1
        }).populate('createdBy updatedBy')
        .exec(function (err, suppliers) {
          if (err) return callback(err)
          locals.suppliers = suppliers
          callback()
        })
    },
    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          isDifference: true
        }).populate({
          path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData_alert = DOData
          // console.log(DOData)
          callback()
        })
    }
  ], function (err) {
    if (err) return next(err)
    res.render('supplier', {
      title: appName + " | " + "Supplier",
      user_data: user_data,
      suppliers: locals.suppliers,
      DOData_alert: locals.DOData_alert,
      messages: messages,
      messages_type: messages_type
    })
  })
}

// POST ho create supplier
exports.create_supplier_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name) {
    var suppData = {
      name: req.body.name,
      headOffice: user_data.headOffice,
      createdDate: Date.now(),
      createdBy: user_data._id,
    }
    Supplier.create(suppData, function (error, supplier) {
      if (error || supplier == null) {
        console.log(error)
        req.flash('info', 'Create supplier failed')
        req.flash('info_type', 'danger')
        res.redirect('/ho/supplier')
      } else {
        req.flash('info', 'Create supplier success')
        req.flash('info_type', 'success')
        res.redirect('/ho/supplier')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/supplier')
  }
}


// GET API ho single supplier
exports.single_supplier = function (req, res, next) {
  Session.validate(req.session.key, 'ho', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.supplierId) {
        Supplier.findOne({
            _id: req.params.supplierId
          }, {
            '__v': 0
          }).populate('createdBy updatedBy')
          .exec(function (error, supplier) {
            if (err) {
              var err = {
                status: 400,
                message: err
              }
              return res.send(JSON.stringify(err))
            }
            if (supplier == null) {
              var err = {
                status: 400,
                message: 'Supplier not found'
              }
              return res.send(JSON.stringify(err))
            } else {
              var ret = {
                status: 200,
                data: {
                  supplier_id: supplier._id,
                  name: supplier.name,
                }
              }
              return res.send(JSON.stringify(ret))
            }
          })
      } else {
        var err = {
          status: 400,
          message: 'Supplier id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}


// POST ho update supplier
exports.update_supplier_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.supplier_id && req.body.name) {
    var suppData = {
      name: req.body.name,
      updatedDate: Date.now(),
      updatedBy: user_data._id,
    }
    Supplier.findOneAndUpdate({
      _id: req.body.supplier_id
    }, suppData, function (error, supplier) {
      if (error || supplier == null) {
        console.log(error)
        req.flash('info', 'Update supplier failed')
        req.flash('info_type', 'danger')
        res.redirect('/ho/supplier')
      } else {
        req.flash('info', 'Update supplier success')
        req.flash('info_type', 'success')
        res.redirect('/ho/supplier')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/supplier')
  }
}

// POST sm delete supplier
exports.delete_supplier_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.supplierId) {
    Supplier.findByIdAndRemove(req.params.supplierId, function (error, item) {
      if (error || item == null) {
        console.log(error)
        req.flash('info', 'Delete supplier failed')
        req.flash('info_type', 'danger')
        res.redirect('/ho/supplier')
      } else {
        req.flash('info', 'Delete supplier success')
        req.flash('info_type', 'success')
        res.redirect('/ho/supplier')
      }
    })
  } else {
    req.flash('info', 'Item id required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/supplier')
  }
}

//Customer
exports.customer = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load Head Office Data
    function (callback) {
      Customer.find({
        headOffice: user_data.headOffice
      }).sort({
        createdDate: -1
      }).populate('brand headOffice store createdBy updatedBy').exec(function (err, customer) {
        if (err) return callback(err)
        locals.customer = customer
        callback()
      })
    }, //Load Brands Data
    function (callback) {
      Brand.find({
        headOffice: user_data.headOffice
      }, function (err, result) {
        if (err) return callback(err)
        locals.brands = result
        callback()
      })
    },
    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          isDifference: true
        }).populate({
          path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData_alert = DOData
          // console.log(DOData)
          callback()
        })
    }

  ], function (err) {
    if (err) console.log('Error')
    res.render('customer', {
      title: appName + " | " + "Customer",
      user_data: user_data,
      customer: locals.customer,
      brands: locals.brands,
      DOData_alert: locals.DOData_alert,
      messages: messages,
      messages_type: messages_type
    })
  })
}

//POST Create Customer
exports.create_customer_process = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var customerData = {
    companyId: req.body.companyId,
    brandId: req.body.brandId,
    unitKerja: req.body.unitKerja,
    custId: req.body.custId,
    headOffice: user_data.headOffice,
    brand: req.body.brand,
    store: req.body.store,
    custName: req.body.custName,
    custAddr1: req.body.custAddr1,
    custTelp1: req.body.custTelp1,
    custEmail: req.body.custEmail,
    custDisc: req.body.custDisc,
    custNotes: req.body.custNotes,
    custRewards: 0,
    custExp: req.body.custExp,
    custBirth: req.body.custBirth,
    isMember: req.body.isMember,
    createdDate: Date.now(),
    createdBy: user_data._id,
    updatedDate: Date.now(),
    updatedBy: user_data._id,
  }
  Customer.create(customerData, function (error, result) {
    if (error || result == null) {
      var err = {
        status: 400,
        message: error
      }
      return res.send(JSON.stringify(err))
    } else {
      req.flash('info', 'Create Customer success')
      req.flash('info_type', 'success')
      res.redirect('/ho/customer')
    }
  })
}

//POST Update Customer
exports.update_customer_process = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var customerData = {
    companyId: req.body.companyId,
    brandId: req.body.brandId,
    unitKerja: req.body.unitKerja,
    custId: req.body.custId,
    headOffice: user_data.headOffice,
    brand: req.body.brand,
    store: req.body.store,
    custName: req.body.custName,
    custAddr1: req.body.custAddr1,
    custTelp1: req.body.custTelp1,
    custEmail: req.body.custEmail,
    custDisc: req.body.custDisc,
    custNotes: req.body.custNotes,
    custExp: req.body.custExp,
    custBirth: req.body.custBirth,
    isMember: req.body.isMember,
    updatedDate: Date.now(),
    updatedBy: user_data._id,
  }
  if (req.body.id) {
    Customer.findOneAndUpdate({
      _id: req.body.id
    }, customerData, function (error, result) {
      if (error || result == null) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Update Customer success')
        req.flash('info_type', 'success')
        res.redirect('/ho/customer')
      }
    })
  } else {
    req.flash('info', 'All Fields required')
    req.flash('info_type', 'success')
    res.redirect('/ho/customer')
  }
}


exports.user = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var user_list = []
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
      //Load User Data
      function (callback) {
        User.find({
          headOffice: user_data.headOffice,
          role: {
            $ne: "ho"
          }
        }).populate("headOffice brand store createdBy updatedBy").sort({
          createdDate: -1
        }).exec(function (err, user) {
          if (err) return callback(err)
          var total_expired_user = 0
          var total_active_user = 0
          locals.user_list = user
          // console.log(user)
          callback()
        })
      },
      //Load Brand Data
      function (callback) {
        Brand.find({
          headOffice: user_data.headOffice
        }, function (err, result) {
          if (err) return callback(err)
          locals.brands = result
          callback()
        })
      },
      function (callback) {
        DeliveryOrder.find({
            headOffice: user_data.headOffice,
            isDifference: true
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData_alert = DOData
            callback()
          })
      },
    ],
    function (err) {
      if (err) return next(err)
      res.render('user', {
        title: appName + " | " + "User",
        user_data: user_data,
        DOData_alert: locals.DOData_alert,
        brands: locals.brands,
        user_list: locals.user_list,
        total_expired_user: locals.total_expired_user,
        total_active_user: locals.total_active_user,
        messages: messages,
        messages_type: messages_type
      })
    })
}

//Post Create User
exports.create_user_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name != '' && req.body.email != '' && req.body.password != '' && req.body.confirm_password &&
    req.body.role != '' && req.body.expire != '') {
    if (req.body.password == req.body.confirm_password) {
      var file_url = req.file ? ('/image/' + res.req.file.filename) : ''
      if (req.body.role == 'sm_warehouse' || req.body.role == 'sm_supervisor') {
        var userData = {
          name: req.body.name,
          email: req.body.email,
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          role: req.body.role,
          expire: req.body.expire,
          brand: req.body.brand,
          store: req.body.store,
          headOffice: user_data.headOffice,
          imageProfile: file_url,
          createdDate: Date.now(),
          createdBy: user_data._id,
        }
      } else if (req.body.role == 'bm') {
        var userData = {
          name: req.body.name,
          email: req.body.email,
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          role: req.body.role,
          expire: req.body.expire,
          headOffice: user_data.headOffice,
          brand: req.body.brand,
          store: null,
          imageProfile: file_url,
          createdDate: Date.now(),
          createdBy: user_data._id,
        }
      }
      User.findOne({
        email: req.body.email
      }, function (error, result) {
        if (!result) {
          User.create(userData, function (error, user) {
            if (error || user == null) {
              req.flash('info', 'Create User Failed! Something went wrong.')
              req.flash('info_type', 'danger')
              res.redirect('/ho/user')
            } else {
              req.flash('info', 'Create User success')
              req.flash('info_type', 'success')
              res.redirect('/ho/user')
            }
          })
        } else {
          req.flash('info', 'Email is already used')
          req.flash('info_type', 'danger')
          res.redirect('/ho/user')
        }
      })
    } else {
      req.flash('info', 'Password isn\'t match')
      req.flash('info_type', 'danger')
      res.redirect('/ho/user')
    }
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/user')
  }
}

//POST admin update user non API
exports.edit_user_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.name != '' && req.body.email != '' && req.body.role != '' && req.body.expire != '') {
    if (user_data._id != req.body.id) {
      var file_url = req.file ? ('/image/' + res.req.file.filename) : ''
      if (req.body.role == 'sm_supervisor' || req.body.role == 'sm_warehouse') {
        var userData = {
          name: req.body.name,
          email: req.body.email,
          role: req.body.role,
          expire: req.body.expire,
          headOffice: user_data.headOffice,
          brand: req.body.brand,
          store: req.body.store,
          updatedDate: Date.now(),
          updatedBy: user_data._id,
        }
        if (file_url != '') userData.imageProfile = file_url
      } else if (req.body.role == 'bm') {
        var userData = {
          name: req.body.name,
          email: req.body.email,
          role: req.body.role,
          expire: req.body.expire,
          headOffice: user_data.headOffice,
          brand: req.body.brand,
          store: null,
          updatedDate: Date.now(),
          updatedBy: user_data._id,
        }
        if (file_url != '') userData.imageProfile = file_url
      }
      User.findOneAndUpdate({
        _id: req.body.id
      }, userData, function (error, user) {
        if (error || user == null) {
          req.flash('info', 'Edit User Failed')
          req.flash('info_type', 'danger')
          res.redirect('/ho/user')
        } else {
          console.log(file_url)
          req.flash('info', 'Edit User success')
          req.flash('info_type', 'success')
          res.redirect('/ho/user')
        }
      })
    } else {
      req.flash('info', 'You can\'t edit your own account')
      req.flash('info_type', 'danger')
      res.redirect('/ho/user')
    }
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/user')
  }
}

// GET admin single user
exports.single_user = function (req, res, next) {
  if (req.params.userId) {
    User.findOne({
      _id: req.params.userId
    }, {
      '__v': 0
    }).populate("headOffice brand store").exec(function (err, user) {
      if (err) {
        var err = {
          status: 400,
          message: err
        }
        return res.send(JSON.stringify(err))
      }
      if (user == null) {
        var err = {
          status: 400,
          message: 'User not found'
        }
        return res.send(JSON.stringify(err))
      } else {
        var ret = {
          status: 200,
          data: user,
          date: dateFormat(user.datecreated, "yyyy-mm-dd")
        }
        return res.send(JSON.stringify(ret))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'User id required'
    }
    return res.send(JSON.stringify(err))
  }
}

exports.delete_user_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.userId) {
    if (user_data._id != req.params.userId) {
      User.findByIdAndRemove(req.params.userId, function (error, revenue) {
        if (error) {
          var err = {
            status: 400,
            message: error
          }
          return res.send(JSON.stringify(err))
        }
        if (revenue == null) {
          req.flash('info', 'User not found')
          req.flash('info_type', 'danger')
          res.redirect('/ho/user')
        } else {
          req.flash('info', 'Delete User success')
          req.flash('info_type', 'success')
          res.redirect('/ho/user')
        }
      })
    } else {
      req.flash('info', 'You can\'t delete your own account')
      req.flash('info_type', 'danger')
      res.redirect('/ho/user')
    }
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/user')
  }
}

//Get Store list
exports.get_store_list = function (req, res, next) {
  auth_redirect(req, res)
  if (req.params.brandId) {
    Store.find({
        brand: req.params.brandId
      }, {
        '__v': 0
      })
      .exec(function (error, result) {
        if (err) {
          var err = {
            status: 400,
            message: err
          }
          return res.send(JSON.stringify(err))
        }
        if (result == null) {
          var err = {
            status: 400,
            message: 'No Store'
          }
          return res.send(JSON.stringify(err))
        } else {
          var ret = {
            status: 200,
            data: result
          }
          return res.send(JSON.stringify(ret))
        }
      })
  } else {
    var err = {
      status: 400,
      message: 'Brand id required'
    }
    return res.send(JSON.stringify(err))
  }
}

//Get Single Customer
exports.get_single_customer = function (req, res, next) {
  auth_redirect(req, res)
  if (req.params.customerId) {
    Customer.findOne({
        _id: req.params.customerId
      }, {
        '__v': 0
      })
      .exec(function (error, result) {
        if (err) {
          var err = {
            status: 400,
            message: err
          }
          return res.send(JSON.stringify(err))
        }
        if (result == null) {
          var err = {
            status: 400,
            message: 'No Customer'
          }
          return res.send(JSON.stringify(err))
        } else {
          var ret = {
            status: 200,
            data: result
          }
          return res.send(JSON.stringify(ret))
        }
      })
  } else {
    var err = {
      status: 400,
      message: 'Customer id required'
    }
    return res.send(JSON.stringify(err))
  }
}

// GET delete customer
exports.delete_customer_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.customerId) {
    Customer.findByIdAndRemove(req.params.customerId, function (error, revenue) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (revenue == null) {
        req.flash('info', 'Customer not found')
        req.flash('info_type', 'danger')
        res.redirect('/ho/customer')
      } else {
        req.flash('info', 'Delete Customer success')
        req.flash('info_type', 'success')
        res.redirect('/ho/customer')
      }
    })
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/customer')
  }
}


//function auth redirect
function auth_redirect(req, res) {
  sess = req.session
  if (sess.user_login) return true
  else res.redirect('/signin')
}

exports.test_agregate = function (req, res) {
  StockTransaction.aggregate([{
      $match: {
        hoId: '5c8a94ebf142791a8ead1308',
        stock: ObjectId('5ca1bd52145526269f475db9')
      }
    },
    {
      $group: {
        _id: "$type",
        sum: {
          $sum: "$quantity"
        }
      }
    }
  ], function (err, result) {
    if (err) {
      req.flash('info', 'Create delivery order failed')
      req.flash('info_type', 'danger')
      res.redirect(failRedir)
    } else {
      var ret = result.reduce(function (prev, cur) {
        return prev + cur.sum
      }, 0)
      console.log(result, parseFloat(ret))
      res.json(parseFloat(ret))
    }
  })
}

// POST API HO return doitem process
exports.return_doitem_process = function (req, res, next) {
  Session.validate(req.session.key, 'ho', function (error, user) {
    var user_data = req.session.user_data
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.doitemid && req.body.stockid && req.body.quantity && req.body.notes) {
        var DOItemData = {
          quantityReturn: req.body.quantity,
          notesReturn: req.body.notes
        }
        DOItem.findOneAndUpdate({
            _id: req.body.doitemid
          }, DOItemData).populate('unit item')
          .exec(function (error, item) {
            if (err) {
              var err = {
                status: 400,
                message: err
              }
              return res.send(JSON.stringify(err))
            }
            if (item == null) {
              var err = {
                status: 400,
                message: 'DO Item not found'
              }
              return res.send(JSON.stringify(err))
            } else {
              //Decreasing Stock
              var StockData = {
                "$inc": {
                  currentStock: parseFloat(req.body.quantity * -1)
                }
              }
              Stock.findByIdAndUpdate(req.body.stockid, StockData, function (error, update_stock) {
                if (error || update_stock == null) {
                  var err = {
                    status: 400,
                    message: 'Return DO item failed'
                  }
                  return res.send(JSON.stringify(err))
                } else {
                  var stockData = {
                    hoId: update_stock.headOffice,
                    doItemId: req.body.doitemid,
                    quantity: req.body.quantity,
                    stock: req.body.stockid,
                    type: "out",
                    description: 'Return DO item',
                    createdDate: Date.now(),
                    createdBy: user_data._id,
                  }
                  StockTransaction.create(stockData, function (err, trans_stock) {
                    if (err || trans_stock == null) {
                      return res.send(JSON.stringify(err))
                    } else {
                      var ret = {
                        status: 200,
                        data: {
                          returnNotes: req.body.notes,
                          returnVal: req.body.quantity
                        }
                      }
                      return res.send(JSON.stringify(ret))
                    }
                  })
                }
              })
            }
          })
      } else {
        var err = {
          status: 400,
          message: 'All data required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

//Notification
exports.notification = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  async.parallel([
    function (callback) {
      DeliveryOrder.find({
          headOffice: user_data.headOffice,
          isDifference: true
        }).populate({
          path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData_alert = DOData
          // console.log(DOData)
          callback()
        })
    }

  ], function (err) {
    if (err) return next(err)
    res.render('notification', {
      title: appName + " | " + "Notification",
      user_data: user_data,
      DOData_alert: locals.DOData_alert
    })
  })
}

//restock_report
exports.restock_report = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var type = req.params.type
  var locals = {}
  if (type == "supplier") {
    console.log(type)
    async.parallel([
      function (callback) {
        DeliveryOrder.find({
            headOffice: user_data.headOffice,
            isDifference: true
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData_alert = DOData
            // console.log(DOData)
            callback()
          })
      },
      function (callback) {
        DeliveryOrder.find({
            type: "direct_ho"
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData = DOData
            callback()
          })
      },
      function (callback) {
        Supplier.find().exec(function (err, result) {
            if (err) return callback(err)
            locals.supplier_list = result
            callback()
          })
      }      
    ], function (err) {
      res.render('restock_report', {
        title: appName + " | " + "Restock Report : " + type,
        user_data: user_data,
        supplier_list: locals.supplier_list,
        DOData: locals.DOData,
        DOData_alert: locals.DOData_alert,
        type: type
      })
    })
  } else if (type == "item") {
    async.parallel([
      function (callback) {
        StockTransaction.find({
            hoId: user_data.headOffice,
            description: "Restock items"
          }).populate({
            path: 'stock doItemId createdBy updatedBy',
            populate: {
              path: 'item',
              populate: {
                path: 'unit itemCategory'
              }
            }
          })
          .exec(function (err, trans) {
            if (err) return callback(err)
            locals.trans = trans
            callback()
          })
      },
      function (callback) {
        Item.find({
            hoId: user_data.headOffice
          }).populate({
            path: 'itemCategory unit stock',
            populate: {
              path: 'highestUnit'
            }
          })
          .exec(function (err, items) {
            if (err) return callback(err)
            locals.items = items
            callback()
          })
      },
      function (callback) {
        Unit.find({
            type: "lowest"
          }).populate('highestUnit')
          .exec(function (err, units) {
            if (err) return callback(err)
            locals.units = units
            callback()
          })
      },
      function (callback) {
        DeliveryOrder.find({
            headOffice: user_data.headOffice,
            isDifference: true
          }).populate({
            path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData_alert = DOData
            // console.log(DOData)
            callback()
          })
      }
    ], function (err) {
      if (err) return next(err)
      res.render('restock_report_item', {
        title: appName + " | " + "Re-stock Item",
        user_data: user_data,
        trans: locals.trans,
        items: locals.items,
        units: locals.units,
        DOData_alert: locals.DOData_alert,
        type: type
      })
    })
  }
}

//restock_report single supplier
exports.restock_report_single = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var type = req.params.type
  var locals = {}
  if (type == "supplier") {
    async.parallel([
      function (callback) {
        DeliveryOrder.find({
            headOffice: user_data.headOffice,
            isDifference: true
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData_alert = DOData
            callback()
          })
      },
      function (callback) {
        DeliveryOrder.find({
            type: "direct_ho",
            supplier: req.params.id
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData = DOData
            callback()
          })
      },
      function (callback) {
        Supplier.findById(req.params.id).exec(function (err, supplier) {
            if (err) return callback(err)
            locals.supplier = supplier
            callback()
          })
      }      
    ], function (err) {
      res.render('restock_report_single', {
        title: appName + " | " + "Restock Report : " + locals.supplier.name,
        user_data: user_data,
        DOData: locals.DOData,
        DOData_alert: locals.DOData_alert,
        dateFrom: {
          inputVal: req.params.dateFrom != '-' ? req.params.dateFrom.split('-').join('/') : '',
          paramsVal: req.params.dateFrom
        },
        dateTo: {
          inputVal: req.params.dateTo != '-' ? req.params.dateTo.split('-').join('/') : '',
          paramsVal: req.params.dateTo
        },
        type: type,
        supplier: locals.supplier
      })
    })
  } else if (type == "item") {
    async.parallel([
      function (callback) {
        DeliveryOrder.find({
            headOffice: user_data.headOffice,
            isDifference: true
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData_alert = DOData
            callback()
          })
      },
      function (callback) {
        DeliveryOrder.find({
            type: "direct_ho"
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            var item_filter = []
            DOData.map((f, i) => {
              f.deliveryItem.map((g, j) => {
                if (g.item._id == req.params.id) {
                  item_filter.push(f)
                }
              })
              if (i == DOData.length) {
                console.log("disini")
                locals.DOData = item_filter
              }
              locals.DOData = item_filter
            })
            callback()
          })
      }
    ], function (err) {
      console.log(locals.DOData)
      res.render('restock_report_single', {
        title: appName + " | " + "Restock Report : Detail Item",
        user_data: user_data,
        DOData: locals.DOData,
        DOData_alert: locals.DOData_alert,
        type: type,
        single_type: "Detail Item"
      })
    })
  }
}

//GET restock_report_print
exports.restock_report_print = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    function (callback) {
      var findQuery = { supplier: req.params.id }
      if (req.params.dateFrom == req.params.dateTo) {
        var start = moment(req.params.dateFrom).startOf('day');
        var end = moment(start).endOf('day');
        findQuery.deliveryDate = { $gte: start, $lte: end }
      } else if (req.params.dateFrom != '-' && req.params.dateTo == '-') {
        findQuery.deliveryDate = { $gte: req.params.dateFrom }
      } else if (req.params.dateFrom == '-' && req.params.dateTo != '-') {
        findQuery.deliveryDate = { $lte: req.params.dateTo }
      } else if (req.params.dateFrom != '-' && req.params.dateTo != '-') {
        findQuery.deliveryDate = { $gte: req.params.dateFrom, $lte: req.params.dateTo }
      }
      DeliveryOrder.find(findQuery)
        .sort({createdDate: -1}).populate({
          path: 'headOffice supplier deliveryItem store recipientUser storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        })
        .exec(function (err, result) {
          if (err) return callback(err)
          locals.DOData = result
          // console.log(DOData[0].deliveryItem[0])
          callback()
        })
    },
    function (callback) {
      Supplier.findById(req.params.id).exec(function (err, result) {
        if (err) return callback(err)
        locals.supplier = result
        callback()
      })
    },
    function (callback) {
      HeadOffice.findById(user_data.headOffice).exec(function (err, result) {
        if (err) return callback(err)
        locals.headOffice = result
        callback()
      })
    }         
  ], function (err) {
    if (err) console.log(err)
    DeliveryOrder.find({
        headOffice: user_data.headOffice,
        isDifference: true
      }).populate({
        path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
        populate: {
          path: 'item stockRef unit brand',
          populate: {
            path: 'itemCategory'
          }
        }
      }).sort({
        createdDate: -1
      })
      .exec(function (err, DOData) {
        if (err || DOData == null) {
          return console.log(err)
        } else {
          locals.DOData_alert = DOData
          res.render('restock_report_print', {
            title: appName + " | " + "Delivery Order Print Details",
            user_data: user_data,
            lowest_unit: locals.lowest_unit,
            supplier: locals.supplier,
            headOffice: locals.headOffice,
            DOData: locals.DOData,
            DOData_alert: locals.DOData_alert,
            showPrice: true,
            messages: messages,
            messages_type: messages_type,
          })
        }
      })
  })
}

//do_report
exports.do_report = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var type = req.params.type
  var locals = {}
  if (type == "store") {

    console.log(type)
    async.parallel([
      function (callback) {
        DeliveryOrder.find({
            headOffice: user_data.headOffice,
            isDifference: true
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData_alert = DOData
            // console.log(DOData)
            callback()
          })
      },
      function (callback) {
        DeliveryOrder.find({
            type: "indirect_store"
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData = DOData
            callback()
          })
      }
    ], function (err) {
      res.render('do_report', {
        title: appName + " | " + "Delivery Order Report : " + type,
        user_data: user_data,
        DOData: locals.DOData,
        DOData_alert: locals.DOData_alert,
        type: type
      })
    })
  } else if (type == "item") {
    async.parallel([
      function (callback) {
        StockTransaction.find({
            hoId: user_data.headOffice,
            description: "Delivery order creation"
          }).populate({
            path: 'stock doItemId createdBy updatedBy',
            populate: {
              path: 'item',
              populate: {
                path: 'unit itemCategory'
              }
            }
          })
          .exec(function (err, trans) {
            if (err) return callback(err)
            locals.trans = trans
            callback()
          })
      },
      function (callback) {
        Item.find({
            hoId: user_data.headOffice
          }).populate({
            path: 'itemCategory unit stock',
            populate: {
              path: 'highestUnit'
            }
          })
          .exec(function (err, items) {
            if (err) return callback(err)
            locals.items = items
            callback()
          })
      },
      function (callback) {
        Unit.find({
            type: "lowest"
          }).populate('highestUnit')
          .exec(function (err, units) {
            if (err) return callback(err)
            locals.units = units
            callback()
          })
      },
      function (callback) {
        DeliveryOrder.find({
            headOffice: user_data.headOffice,
            isDifference: true
          }).populate({
            path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData_alert = DOData
            // console.log(DOData)
            callback()
          })
      }
    ], function (err) {
      if (err) return next(err)
      res.render('do_report_item', {
        title: appName + " | " + "Delivery Order Item",
        user_data: user_data,
        trans: locals.trans,
        items: locals.items,
        units: locals.units,
        DOData_alert: locals.DOData_alert,
        type: type
      })
    })
  }
}

//do_report single supplier
exports.do_report_single = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var type = req.params.type
  var locals = {}
  if (type == "store") {

    async.parallel([
      function (callback) {
        DeliveryOrder.find({
            headOffice: user_data.headOffice,
            isDifference: true
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData_alert = DOData
            callback()
          })
      },
      function (callback) {
        DeliveryOrder.find({
            type: "indirect_store",
            store: req.params.id
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData = DOData
            callback()
          })
      }
    ], function (err) {
      console.log(locals.DOData[0].store.name)
      res.render('do_report_single', {
        title: appName + " | " + "Delivery Order Report : " + locals.DOData[0].store.name,
        user_data: user_data,
        DOData: locals.DOData,
        DOData_alert: locals.DOData_alert,
        type: type,
        single_type: locals.DOData[0].store.name
      })
    })
  } else if (type == "item") {
    async.parallel([
      function (callback) {
        DeliveryOrder.find({
            headOffice: user_data.headOffice,
            isDifference: true
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData_alert = DOData
            callback()
          })
      },
      function (callback) {
        DeliveryOrder.find({
            type: "indirect_store"
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            var item_filter = []
            DOData.map((f, i) => {
              f.deliveryItem.map((g, j) => {
                if (g.item._id == req.params.id) {
                  item_filter.push(f)
                }
              })
              if (i == DOData.length) {
                locals.DOData = item_filter
              }
              locals.DOData = item_filter
            })
            callback()
          })
      }
    ], function (err) {
      console.log(locals.DOData)
      res.render('do_report_single', {
        title: appName + " | " + "Delivery Order Report : Detail Item",
        user_data: user_data,
        DOData: locals.DOData,
        DOData_alert: locals.DOData_alert,
        type: type,
        single_type: "Detail Item"
      })
    })
  } else if (type == "driver") {
    async.parallel([
      function (callback) {
        DeliveryOrder.find({
            headOffice: user_data.headOffice,
            isDifference: true
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData_alert = DOData
            callback()
          })
      },
      function (callback) {
        DeliveryOrder.find({
            type: "indirect_store",
            driverName: req.params.id
          }).populate({
            path: 'headOffice supplier deliveryItem store  createdBy updatedBy',
            populate: {
              path: 'item stockRef unit brand',
              populate: {
                path: 'itemCategory'
              }
            }
          }).sort({
            createdDate: -1
          })
          .exec(function (err, DOData) {
            if (err) return callback(err)
            locals.DOData = DOData
            callback()
          })
      }
    ], function (err) {
      res.render('do_report_single', {
        title: appName + " | " + "Delivery Order Report : Driver " + req.params.id,
        user_data: user_data,
        DOData: locals.DOData,
        DOData_alert: locals.DOData_alert,
        type: type,
        single_type: "Driver " + req.params.id
      })
    })
  }
}



// resolve_difference DO
exports.resolve_difference = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.doId) {
    DeliveryOrder.findByIdAndUpdate(req.params.doId, {
      isDifference: false
    }, function (error, DO) {
      if (error) {
        req.flash('info', 'Resolve diference delivery order failed')
        req.flash('info_type', 'danger')
        res.redirect('/ho/delivery_order_detail/' + req.params.doId)
      }
      if (DO == null) {
        req.flash('info', 'Delivery order not found')
        req.flash('info_type', 'danger')
        res.redirect('/ho/delivery_order_detail/' + req.params.doId)
      } else {
        req.flash('info', 'Resolve diference delivery order success')
        req.flash('info_type', 'success')
        res.redirect('/ho/notification')
      }
    })
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/ho/delivery_order_detail/' + req.params.doId)
  }
}

exports.changePaidStatus = function (req, res) {
  var doId = req.params.id
  var paidStatus = req.body.paidStatus
  if (doId) {
    var data_update = {
      paidStatus: (paidStatus == 'paid' ? 'paid' : 'unpaid')
    }
    DeliveryOrder.findByIdAndUpdate({
      _id: doId
    }, data_update, function (error, result) {
      if (error || result == null) {
        var err = {
          status: 400,
          messsage: "Something went wrong"
        }
        return res.send(JSON.stringify(err))
      } else {
        var err = {
          status: 200,
          message: 'Change Paid Status Success'
        }
        return res.send(JSON.stringify(err))
      }
    })
  }
}