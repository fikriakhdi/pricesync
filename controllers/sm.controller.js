const User = require('../models/user.model')
const News = require('../models/news.model')
const Product = require('../models/product.model')
const Brand = require('../models/brand.model')
const Store = require('../models/store.model')
const Revenue = require('../models/revenue.model')
const DeliveryOrder = require('../models/delivery_order.model')
const DOItem = require('../models/do_item.model')
const Stock = require('../models/stock.model')
const StockTransaction = require('../models/stock_transaction.model')
const Unit = require('../models/unit.model')
const Item = require('../models/item.model')
const Session = require('../models/session.model')
const formatCurrency = require('format-currency')
var fs = require('fs')
var hbs = require('hbs')
var crypto = require('crypto')
var dateFormat = require('dateformat')
var async = require('async')
var axios = require('axios')
var ObjectId = require('mongodb').ObjectID


//Dashboard
exports.dashboard = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load News Data
    function (callback) {
      News.find({
        brand: user_data.brand
      }, null, {
        limit: 1
      }).sort({
        createdDate: -1
      }).exec(function (err, news) {
        if (err) return callback(err)
        locals.news = news[0]
        callback()
      })
    },
    //Load Brand Data
    function (callback) {
      Brand.find({
        _id: user_data.brand
      }, function (err, brand) {
        if (err) return callback(err)
        locals.brand = brand[0]
        callback()
      })
    },
    //Load Store Data
    function (callback) {
      Store.find({
        _id: user_data.store
      }, function (err, store) {
        if (err) return callback(err)
        locals.store = store[0]
        callback()
      })
    },
    //Load Products Data
    function (callback) {
      Product.find({
        brand: user_data.brand
      }).populate('createdBy updatedBy').exec(function (err, products) {
        if (err) return callback(err)
        locals.products = products
        callback()
      })
    }
  ], function (err) { //This function gets called after the two tasks have called their "task callbacks"
    if (err) return next(err) //If an error occurred, we let express handle it by calling the `next` function
    //Here `locals` will be an object with `user` and `posts` keys
    //Example: `locals = {user: ..., posts: [...]}`
    res.render('main', {
      title: appName + " | " + "Dashboard",
      user_data: user_data,
      news: locals.news,
      brand: locals.brand,
      store: locals.store,
      products: locals.products,
      messages: messages,
      messages_type: messages_type
    })
  })
}


//function auth redirect
function auth_redirect(req, res, redirect = "") {
  sess = req.session
  if (sess.user_login) return true
  else res.redirect('/signin/' + redirect)
}

//profile
exports.profile = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load News Data
    function (callback) {
      News.find({}, null, {
        limit: 1
      }).sort({
        datecreated: -1
      }).exec(function (err, news) {
        if (err) return callback(err)
        locals.news = news[0]
        callback()
      })
    },
    //Load Brand Data
    function (callback) {
      Brand.find({
        _id: user_data.brand
      }, function (err, brand) {
        if (err) return callback(err)
        locals.brand = brand[0]
        callback()
      })
    },
    //Load Store Data
    function (callback) {
      Store.find({
        _id: user_data.store
      }, function (err, store) {
        if (err) return callback(err)
        locals.store = store[0]
        callback()
      })
    },
    //Load Products Data
    function (callback) {
      Product.find({
        brandId: user_data.brand
      }, function (err, products) {
        if (err) return callback(err)
        locals.products = products
        callback()
      })
    }
  ], function (err) { //This function gets called after the two tasks have called their "task callbacks"
    if (err) return next(err) //If an error occurred, we let express handle it by calling the `next` function
    //Here `locals` will be an object with `user` and `posts` keys
    //Example: `locals = {user: ..., posts: [...]}`
    res.render('profile', {
      title: appName + " | " + "Profile",
      user_data: user_data,
      brand: locals.brand,
      store: locals.store,
      messages: messages,
      messages_type: messages_type
    })
  })
}

//revenue
exports.revenue = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    //Load News Data
    function (callback) {
      News.find({}, null, {
        limit: 1
      }).sort({
        createdDate: -1
      }).exec(function (err, news) {
        if (err) return callback(err)
        locals.news = news[0]
        callback()
      })
    },
    //Load Brand Data
    function (callback) {
      Brand.findOne({
        _id: user_data.brand
      }, function (err, brand) {
        if (err) return callback(err)
        locals.brand = brand
        callback()
      })
    },
    //Load Revenue Data
    function (callback) {
      Revenue.find({
        store: user_data.store
      }).populate('store createdBy updatedBy').exec(function (err, revenue) {
        if (err) return callback(err)
        locals.revenue = revenue
        callback()
      })
    }
  ], function (err) { //This function gets called after the two tasks have called their "task callbacks"
    if (err) return next(err) //If an error occurred, we let express handle it by calling the `next` function
    //Here `locals` will be an object with `user` and `posts` keys
    //Example: `locals = {user: ..., posts: [...]}`
    var latest_news_date = dateFormat(locals.news.createdDate, "mmmm dd yyyy")
    var latest_news_desc = locals.news.description
    res.render('revenue', {
      title: appName + " | " + "Revenue",
      user_data: user_data,
      revenue: locals.revenue,
      brand: locals.brand,
      latest_news_date: latest_news_date,
      latest_news_desc: latest_news_desc,
      messages: messages,
      messages_type: messages_type
    })
  })
}

// POST sm update_profile
exports.update_profile = function (req, res, next) {
  Session.validate(req.headers.key, 'sm', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.name != '' && req.body.email != '') {
        var userData = {
          name: req.body.name,
          email: req.body.email,
          imageProfile: "none",
          updatedDate: Date.now(),
          upeatedBy: user._id,
        }
        User.create(userData, function (error, user) {
          if (error || user == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Update profile success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

exports.change_password_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.password != '' && req.body.conf_password != '' && req.body.password == req.body.conf_password) {
    var userData = {
      password: crypto.createHash('md5').update(req.body.password).digest('hex'),
      updatedDate: Date.now(),
      upeatedBy: user_data._id,
    }
    User.findByIdAndUpdate({
      _id: user_data._id
    }, userData, function (error, user) {
      if (error || user == null) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Change password success')
        req.flash('info_type', 'success')
        res.redirect('/sm/profile')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/sm/profile')
  }
}

// POST sm change_password
exports.change_password = function (req, res, next) {
  Session.validate(req.headers.key, 'sm', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.password != '' && req.body.conf_password != '' && req.body.password == req.body.conf_password) {
        var userData = {
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          updatedDate: Date.now(),
          upeatedBy: user._id,
        }
        User.findByIdAndUpdate({
          _id: user._id
        }, userData, function (error, user) {
          if (error || user == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Update profile success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}



// POST sm create revenue
exports.create_revenue_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.date && req.body.shift && req.body.cash && req.body.card &&
    req.body.debit && req.body.other && req.body.discount && req.body.tax) {
    var total = (parseInt(req.body.cash) + parseInt(req.body.card) + parseInt(req.body.debit) + parseInt(req.body.other)) - (parseInt(req.body.tax) + parseInt(req.body.discount))
    var revenueData = {
      date: req.body.date,
      shift: req.body.shift,
      cash: req.body.cash,
      card: req.body.card,
      debit: req.body.debit,
      other: req.body.other,
      discount: req.body.discount,
      tax: req.body.tax,
      total: total,
      brand: user_data.brand,
      store: user_data.store,
      createdDate: Date.now(),
      createdBy: user_data._id,
    }
    Revenue.create(revenueData, function (error, revenue) {
      if (error || revenue == null) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info', 'Create revenue success')
        req.flash('info_type', 'success')
        res.redirect('/sm/revenue')
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/sm/revenue')
  }
}


exports.create_revenue = function (req, res, next) {
  Session.validate(req.headers.key, 'sm', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.date && req.body.shift && req.body.cash && req.body.card &&
        req.body.debit && req.body.other && req.body.discount && req.body.tax) {
        var temp = JSON.stringify(user)
        var user = JSON.parse(temp)
        var revenueData = {
          date: req.body.date,
          shift: req.body.shift,
          cash: req.body.cash,
          card: req.body.card,
          debit: req.body.debit,
          other: req.body.other,
          discount: req.body.discount,
          tax: req.body.tax,
          store: user.store,
          createdDate: Date.now(),
          createdBy: user._id,
        }
        Revenue.create(revenueData, function (error, revenue) {
          if (error || revenue == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Create revenue success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}


// POST sm update revenue
exports.update_revenue = function (req, res, next) {
  Session.validate(req.headers.key, 'sm', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.id && req.body.date && req.body.shift && req.body.cash && req.body.card &&
        req.body.debit && req.body.other && req.body.discount && req.body.tax) {
        var temp = JSON.stringify(user)
        var user = JSON.parse(temp)
        var revenueData = {
          date: req.body.date,
          shift: req.body.shift,
          cash: req.body.cash,
          card: req.body.card,
          debit: req.body.debit,
          other: req.body.other,
          discount: req.body.discount,
          tax: req.body.tax,
          storeId: user.storeId,
          updatedDate: Date.now(),
          updatedBy: user._id,
        }
        Revenue.findOneAndUpdate({
          _id: req.body.id
        }, revenueData, function (error, revenue) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          }
          if (revenue == null) {
            var err = {
              status: 400,
              message: 'Revenue not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Update revenue success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'All fields required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}


// GET sm revenue list
exports.revenue_list = function (req, res, next) {
  Session.validate(req.headers.key, 'sm', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      Revenue.find({}, {
          '__v': 0
        })
        .exec(function (error, revenues) {
          if (err) {
            var err = {
              status: 400,
              message: err
            }
            return res.send(JSON.stringify(err))
          }
          if (!revenues || revenues.length == 0) {
            var err = {
              status: 400,
              message: 'Revenue list not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              data: revenues
            }
            return res.send(JSON.stringify(ret))
          }
        })
    }
  })
}

// POST sm single revenue
exports.single_revenue = function (req, res, next) {
  Session.validate(req.headers.key, 'sm', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.revenueId) {
        Revenue.findOne({
            _id: req.params.revenueId
          }, {
            '__v': 0
          })
          .exec(function (error, revenue) {
            if (err) {
              var err = {
                status: 400,
                message: err
              }
              return res.send(JSON.stringify(err))
            }
            if (revenue == null) {
              var err = {
                status: 400,
                message: 'Revenue not found'
              }
              return res.send(JSON.stringify(err))
            } else {
              var ret = {
                status: 200,
                data: revenue
              }
              return res.send(JSON.stringify(ret))
            }
          })
      } else {
        var err = {
          status: 400,
          message: 'Revenue id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

// GET sm delete revenue
exports.delete_revenue_process = function (req, res, next) {
  auth_redirect(req, res)
  if (req.params.revenueId) {
    Revenue.findByIdAndRemove(req.params.revenueId, function (error, revenue) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (revenue == null) {
        req.flash('info', 'Revenue not found')
        req.flash('info_type', 'danger')
        res.redirect('/sm/revenue')
      } else {
        req.flash('info', 'Delete revenue success')
        req.flash('info_type', 'success')
        res.redirect('/sm/revenue')
      }
    })
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/sm/revenue')
  }
}

exports.delete_revenue = function (req, res, next) {
  Session.validate(req.headers.key, 'sm', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.revenueId) {
        Revenue.findByIdAndRemove(req.params.revenueId, function (error, revenue) {
          if (error) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          }
          if (revenue == null) {
            var err = {
              status: 400,
              message: 'Revenue not found'
            }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status: 200,
              message: 'Delete revenue success.'
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Revenue id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

//POST sm Update Product
exports.update_product_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.id && req.body.name && req.body.description && req.body.PLUId && req.body.price &&
    req.body.group && req.body.printerId && req.body.validStartDate && req.body.brandId) {
    var productData = {
      name: req.body.name,
      description: req.body.description,
      PLUId: req.body.PLUId,
      price: req.body.price,
      group: req.body.group,
      printerId: req.body.printerId,
      validStartDate: req.body.validStartDate,
      brandId: req.body.brandId,
      updatedDate: Date.now(),
      updatedBy: user_data._id,
    }
    Product.findOneAndUpdate({
      _id: req.body.id
    }, productData, function (error, product) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (product == null) {
        req.flash('info', 'Product update failed')
        req.flash('info_type', 'danger')
        res.redirect('/sm')
      } else {
        req.flash('info', 'Product update success')
        req.flash('info_type', 'success')
        res.redirect('/sm')
      }
    })
  } else {
    req.flash('info', 'All fields Required')
    req.flash('info_type', 'danger')
    res.redirect('/sm')
  }
}

//GET sm Data Product
exports.get_single_product = function (req, res, next) {
  if (req.params.productId) {
    Product.findOne({
        _id: req.params.productId
      }, {
        '__v': 0
      })
      .exec(function (error, product) {
        if (err) {
          var err = {
            status: 400,
            message: err
          }
          return res.send(JSON.stringify(err))
        }
        if (product == null) {
          var err = {
            status: 400,
            message: 'Product not found'
          }
          return res.send(JSON.stringify(err))
        } else {
          var ret = {
            status: 200,
            data: product
          }
          return res.send(JSON.stringify(ret))
        }
      })
  } else {
    var err = {
      status: 400,
      message: 'Product id required'
    }
    return res.send(JSON.stringify(err))
  }
}

//Receive Item
exports.receive_item = function (req, res, callback) {

  var doId = req.params.doId
  auth_redirect(req, res, 'sm-receive_item-' + doId)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  DeliveryOrder.findOne({
      doCode: doId,
      store: user_data.store._id
    }).populate({
      path: 'deliveryItem headOffice supplier store ',
      populate: {
        path: 'item unit brand'
      }
    })
    .exec(function (err, DOData) {
      if (err || DOData == null) {
        return callback(err)
      } else {
        locals.DOData = DOData
        if (locals.DOData.type == 'indirect_store') {
          if (user_data.store._id == locals.DOData.store._id) {
            //only Store Warehouse can see this page
            if (user_data.role == 'sm_warehouse') {
              res.render('receive_item', {
                title: appName + " | " + "Receive Item",
                user_data: user_data,
                DOData: locals.DOData,
                messages: messages,
                messages_type: messages_type,
                store: locals.DOData.store,
              })
            } else {
              req.flash('info', 'You are not allowed to access this page')
              req.flash('info_type', 'danger')
              res.redirect('/sm/')
            }
          } else {
            req.flash('info', 'This Delivery Order isn\'t for your store')
            req.flash('info_type', 'danger')
            res.redirect('/sm/')
          }
        } else {
          req.flash('info', 'This not indirect Delivery Order')
          req.flash('info_type', 'danger')
          res.redirect('/sm/')
        }
      }
    })
}

//POST Receive Item
exports.receive_item_process = function (req, res, next) {
  let promises = []
  var updateItems = JSON.parse(req.body.updateItems)
  var isDifference = false
  var user_data = req.session.user_data
  //convert base64 to file
  //Update Delivery Order Data
  var dataUpdate = {
    recipientUser: user_data._id,
    receiptDate: req.body.receiptDate,
    receiptTime: req.body.receiptTime,
    imageUrl: '',
    updatedDate: Date.now(),
    updatedBy: user_data._id,
  }
  DeliveryOrder.findOneAndUpdate({
    _id: req.body.id
  }, dataUpdate, function (error, result) {
    if (error || result == null) {
      console.log(error)
      req.flash('info', 'Delivery Order Not Found')
      req.flash('info_type', 'danger')
      res.redirect('/sm/delivery_order')
    } else {
      //Update DeliveryOrderItem
      updateItems.map((ui, i) => {
        promises.push(new Promise((resolve, reject) => {
          if (ui.qtySend != ui.qty) {
            isDifference = true
            //Update Delivery Order Data
            var dataUpdate = {
              isDifference: true,
              updatedDate: Date.now(),
              updatedBy: user_data._id,
            }
            DeliveryOrder.findOneAndUpdate({
              _id: req.body.id
            }, dataUpdate, function (error, result) {
              if(error) reject()
              else if(result) resolve(result)
            })
          }
          //Cek Stock Store
          Stock.findOne({
            item: ui.itemid,
            store: user_data.store
          }, function (error, stock_find) {
            if (error || stock_find == null) {
              reject(error)
              //Create Stock
              var stockData = {
                item: ui.itemid,
                ownerType: 'store',
                headOffice: null,
                store: user_data.store,
                initialStock: 0,
                currentStock: ui.qty,
                createdDate: Date.now(),
                createdBy: user_data._id,
              }
              Stock.create(stockData, function (error, stock) {
                if (error || stock == null) {
                  reject(error)
                } else {
                  //Create initial stock 0
                  var transData = {
                    hoId: null,
                    stock: stock._id,
                    quantity: 0,
                    type: 'initial',
                    description: 'Create initial stock',
                    createdDate: Date.now(),
                    createdBy: user_data._id,
                  }
                  StockTransaction.create(transData, function (error, trans) {
                    if (error || trans == null) {
                      reject(error)
                    } else {
                      //Create In Stock
                      var transData = {
                        hoId: null,
                        stock: stock._id,
                        quantity: ui.qty,
                        type: 'in',
                        description: 'Delivery Order',
                        createdDate: Date.now(),
                        createdBy: user_data._id,
                      }
                      StockTransaction.create(transData, function (error, trans) {
                        if (error || trans == null) {
                          reject(error)
                        } else {
                          resolve(isDifference)
                        }
                      })
                    }
                  })
                }
              })
            } else {
              //Update stock on Store
              var currentStock = stock_find.currentStock
              var dataUpdate = {
                currentStock: parseFloat(currentStock) + parseFloat(ui.qty),
                updatedDate: Date.now(),
                updatedBy: user_data._id
              }
              Stock.findOneAndUpdate({
                _id: stock_find._id
              }, dataUpdate, function (error, result) {
                if (error || result == null) {
                  reject(error)
                } else {
                  resolve(isDifference)
                }
              })
            }
            //Update DOItem on HO
            var fileName = random_password() + ".png"
            var fileUrl = "./assets/image/" + fileName
            var fullFileURL = "/image/" + fileName
            var buff = decodeBase64Image(ui.img)
            fs.writeFile(fileUrl, buff.data, function (err) {
              var dataUpdate = {
                image: fullFileURL,
                quantityReceive: ui.qty,
                updatedDate: Date.now(),
                updatedBy: user_data._id,
              }
              DOItem.findOneAndUpdate({
                _id: ui.itemOrderId
              }, dataUpdate, function (error, result) {
                if (error) {
                  reject(error)
                }
              })
            })
          })
        }))
      })
      
  Promise.all(promises).then(function (isDiff) {
    DeliveryOrder.findOne({_id: req.body.id }, function(err, deliveryOrder ){
      if(err) {
        console.log("post failed")
        req.flash('info', 'Delivery Order Update Failed')
        req.flash('info_type', 'error')
        res.redirect('/sm/delivery_order')
      } else {
        if (deliveryOrder.isDifference) {
          console.log('send notif')
          var config = {
            headers: {
              "Authorization": "key=AAAAprZkeKw:APA91bFr-VGo60kWu4aGpn4xE72aeNSylttzeikRjMaFmahkMOA5zgIuktslcNJDlt2KXW-1bKJc7Ph3QNloLWUJsnuBLV7G2FnhUIXwifng9NX7wkGI1XosQ2Uq6b-y-PdUszsfPSaj",
              "Content-Type": "application/json"
            }
          }
          const data = {
            "to": "/topics/general",
            "notification": {
              "body": "Difference on Delivery Order " + ("#DO" + req.body.id.toString().slice(-5)).toUpperCase(),
              "title": "Difference Alert",
              "content_available": true,
              "priority": "high"
            },
            "data": {
              "body": "Difference on Delivery Order " + ("#DO" + req.body.id.toString().slice(-5)).toUpperCase(),
              "title": "Difference Alert",
              "content_available": true,
              "priority": "high"
            }
          }
          axios.post('https://fcm.googleapis.com/fcm/send', data, config)
            .then((res) => {
              console.log("done")
              req.flash('info', 'Delivery Order Update Success')
              req.flash('info_type', 'success')
              res.redirect('/sm/delivery_order')
            })
            .catch((err) => {
              console.log("post failed")
              req.flash('info', 'Delivery Order Update Failed')
              req.flash('info_type', 'error')
              res.redirect('/sm/delivery_order')
            })
        } else {
          console.log('here')
          req.flash('info', 'Delivery Order Update Success')
          req.flash('info_type', 'success')
          res.redirect('/sm/delivery_order')
        }
      }
    })
  }).catch(err => {
    console.log(err)
    req.flash('info', 'Delivery Order Update Failed')
    req.flash('info_type', 'error')
    res.redirect('/sm/delivery_order')
  })
    }
  })
}


//GET sm Delete Product
exports.delete_product_process = function (req, res, next) {
  auth_redirect(req, res)
  if (req.params.productId) {
    Product.findByIdAndRemove(req.params.productId, function (error, product) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (product == null) {
        req.flash('info', 'Product not found')
        req.flash('info_type', 'danger')
        res.redirect('/sm')
      } else {
        req.flash('info', 'Delete product success')
        req.flash('info_type', 'success')
        res.redirect('/sm')
      }
    })
  } else {
    req.flash('info', 'Id Required')
    req.flash('info_type', 'danger')
    res.redirect('/sm')
  }
}

//delivery order
exports.delivery_order = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    function (callback) {
      DeliveryOrder.find({
          store: user_data.store
        }).sort({
          createdDate: -1
        }).populate({
          path: 'deliveryItem store recipientUser supplier headOffice storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item unit brand'
          }
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData = DOData
          callback()
        })
    },
    //Load Brand Data
    function (callback) {
      Brand.findOne({
        _id: user_data.brand
      }, function (err, brand) {
        if (err) return callback(err)
        locals.brand = brand
        callback()
      })
    },
  ], function (err) {
    if (err) return next(err)
    res.render('delivery_order', {
      title: appName + " | " + "Delivery Order",
      user_data: user_data,
      item: locals.item,
      brand: locals.brand,
      DOData: locals.DOData,
      messages: messages,
      messages_type: messages_type
    })
  })
}

// POST API SM return doitem process
exports.return_doitem_process = function (req, res, next) {
  Session.validate(req.session.key, 'sm_warehouse', function (error, user) {
    var user_data = req.session.user_data
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.doitemid && req.body.stockid && req.body.quantity && req.body.notes) {
        var DOItemData = {
          quantityReturn: req.body.quantity,
          notesReturn: req.body.notes
        }
        DOItem.findOneAndUpdate({
            _id: req.body.doitemid
          }, DOItemData).populate('unit item')
          .exec(function (error, item) {
            if (err) {
              var err = {
                status: 400,
                message: err
              }
              return res.send(JSON.stringify(err))
            }
            if (item == null) {
              var err = {
                status: 400,
                message: 'DO Item not found'
              }
              return res.send(JSON.stringify(err))
            } else {
              //Increasing Stock on HO
              var StockData = {
                "$inc": {
                  currentStock: parseFloat(req.body.quantity)
                }
              }
              Stock.findByIdAndUpdate(req.body.stockid, StockData, function (error, stock) {
                if (error || stock == null) {
                  var err = {
                    status: 400,
                    message: 'Return DO item failed'
                  }
                  return res.send(JSON.stringify(err))
                } else {
                  var stockData = {
                    hoId: stock.headOffice,
                    doItemId: req.body.doitemid,
                    quantity: req.body.quantity,
                    stock: stock._id,
                    type: "in",
                    description: 'Return DO item',
                    createdDate: Date.now(),
                    createdBy: user_data._id,
                  }
                  StockTransaction.create(stockData, function (err, stock) {
                    if (error || stock == null) {
                      console.log(error)
                      var err = {
                        status: 400,
                        message: 'Return DO item failed'
                      }
                      return res.send(JSON.stringify(err))
                    } else {
                      //Search Stock to Increasing Current Stock on Store
                      Stock.findOne({
                        item: req.body.itemid,
                        store: user_data.store
                      }, function (err, result_stock) {
                        if (err || result_stock == null) {
                          var err = {
                            status: 400,
                            message: 'Return DO item failed'
                          }
                          return res.send(JSON.stringify(err))
                        } else {
                          //Increasing Stock
                          var StockData = {
                            "$inc": {
                              currentStock: parseFloat(req.body.quantity * -1)
                            }
                          }
                          Stock.findByIdAndUpdate(result_stock._id, StockData, function (error, update_stock) {
                            if (error || update_stock == null) {
                              var err = {
                                status: 400,
                                message: 'Return DO item failed'
                              }
                              return res.send(JSON.stringify(err))
                            } else {
                              var stockData = {
                                hoId: update_stock.headOffice,
                                doItemId: req.body.doitemid,
                                quantity: req.body.quantity,
                                stock: result_stock._id,
                                type: "out",
                                description: 'Return DO item',
                                createdDate: Date.now(),
                                createdBy: user_data._id,
                              }
                              StockTransaction.create(stockData, function (err, trans_stock) {
                                if (err || stock == null) {
                                  return res.send(JSON.stringify(err))
                                } else {
                                  var ret = {
                                    status: 200,
                                    data: {
                                      returnNotes: req.body.notes,
                                      returnVal: req.body.quantity
                                    }
                                  }
                                  return res.send(JSON.stringify(ret))
                                }
                              })
                            }
                          })

                        }
                      })
                    }
                  })
                }
              })
            }
          })
      } else {
        var err = {
          status: 400,
          message: 'All data required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })


}

//stock
exports.stock = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    function (callback) {
      Stock.find({
          store: user_data.store
        }).populate({
          path: 'item',
          populate: {
            path: 'itemCategory unit'
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, stocks) {
          if (err) return callback(err)
          locals.stocks = stocks
          var activeStockIds = stocks.map(st => st.item)
          Item.find({
              _id: {
                $nin: activeStockIds
              }
            }).populate({
              path: 'itemCategory unit',
              populate: {
                path: 'highestUnit'
              }
            })
            .exec(function (err, items) {
              if (err) return callback(err)
              locals.items = items
              callback()
            })
        })
    },
    function (callback) {
      Unit.find({
          type: "lowest"
        }).populate('highestUnit')
        .exec(function (err, units) {
          if (err) return callback(err)
          locals.units = units
          callback()
        })
    }
  ], function (err) {
    if (err) return next(err)
    res.render('stock', {
      title: appName + " | " + "Stock",
      user_data: user_data,
      stocks: locals.stocks,
      items: locals.items,
      messages: messages,
      messages_type: messages_type
    })
  })
}


// POST sm create stock
exports.create_stock_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.selected_item && req.body.initial_stock && req.body.notes) {
    var stockData = {
      item: req.body.selected_item,
      ownerType: user_data.role == 'sm_supervisor' ? 'store' : 'ho',
      headOffice: user_data.headOffice,
      store: user_data.role == 'sm_supervisor' ? user_data.store : null,
      initialStock: req.body.initial_stock,
      currentStock: req.body.initial_stock,
      notes: req.body.notes,
      createdDate: Date.now(),
      createdBy: user_data._id,
    }
    Stock.create(stockData, function (error, stock) {
      if (error || stock == null) {
        console.log(error)
        req.flash('info', 'Create stock failed')
        req.flash('info_type', 'danger')
        res.redirect('/sm/stock')
      } else {
        var transData = {
          hoId: user_data.headOffice,
          stock: stock._id,
          quantity: req.body.initial_stock,
          type: 'initial',
          description: 'Create initial stock',
          createdDate: Date.now(),
          createdBy: user_data._id,
        }
        StockTransaction.create(transData, function (err, trans) {
          if (error || trans == null) {
            console.log(error)
            req.flash('info', 'Create stock transaction failed')
            req.flash('info_type', 'danger')
            res.redirect('/sm/stock')
          } else {
            req.flash('info', 'Create stock success')
            req.flash('info_type', 'success')
            res.redirect('/sm/stock')
          }
        })
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/sm/stock')
  }
}


// POST sm delete stock
exports.delete_stock_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.stockId) {
    Stock.findByIdAndRemove(req.params.stockId, function (error, stock) {
      if (error || stock == null) {
        console.log(error)
        req.flash('info', 'Delete stock failed')
        req.flash('info_type', 'danger')
        res.redirect('/sm/stock')
      } else {
        DOItem.deleteMany({
          stock: stock._id
        }, function (error, doi) {
          if (error || doi == null) {
            console.log(error)
            req.flash('info', 'Delete DO item relation failed')
            req.flash('info_type', 'danger')
            res.redirect('/sm/stock')
          } else {
            StockTransaction.deleteMany({
              stock: stock._id
            }, function (error, stocks) {
              if (error || stocks == null) {
                console.log(error)
                req.flash('info', 'Delete stock transaction failed')
                req.flash('info_type', 'danger')
                res.redirect('/sm/stock')
              } else {
                req.flash('info', 'Delete stock success')
                req.flash('info_type', 'success')
                res.redirect('/sm/stock')
              }
            })
          }
        })
      }
    })
  } else {
    req.flash('info', 'Stock id required')
    req.flash('info_type', 'danger')
    res.redirect('/sm/stock')
  }
}


// GET API ho single stock
exports.single_stock = function (req, res, next) {
  Session.validate(req.session.key, 'sm_supervisor', function (error, user) {
    if (error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: error
      }
      return res.send(JSON.stringify(err))
    } else if (user.error) {
      var err = {
        status: 400,
        message: 'Session key incorrect or not found',
        innerMessage: user.message
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.stockId) {
        Stock.findOne({
            _id: req.params.stockId
          }, {
            '__v': 0
          }).populate('createdBy updatedBy')
          .exec(function (error, stock) {
            if (err) {
              var err = {
                status: 400,
                message: err
              }
              return res.send(JSON.stringify(err))
            }
            if (stock == null) {
              var err = {
                status: 400,
                message: 'Stock not found'
              }
              return res.send(JSON.stringify(err))
            } else {
              var ret = {
                status: 200,
                data: {
                  _id: stock._id,
                  initialStock: stock.initialStock,
                  notes: stock.notes
                }
              }
              return res.send(JSON.stringify(ret))
            }
          })
      } else {
        var err = {
          status: 400,
          message: 'Supplier id required'
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}


// POST sm update stock
exports.update_stock_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.stock_id && req.body.initial_stock && req.body.notes) {
    var transData = {
      quantity: req.body.initial_stock,
      updatedDate: Date.now(),
      updatedBy: user_data._id,
    }
    StockTransaction.findOneAndUpdate({
      stock: req.body.stock_id,
      type: 'initial'
    }, transData, function (err, trans) {
      if (err || trans == null) {
        console.log(err, trans)
        req.flash('info', 'Update stock transaction failed')
        req.flash('info_type', 'danger')
        res.redirect('/sm/stock')
      } else {
        StockTransaction.aggregate([{
            $match: {
              hoId: user_data.headOffice,
              stock: ObjectId(req.body.stock_id)
            }
          },
          {
            $group: {
              _id: "$type",
              sum: {
                $sum: "$quantity"
              }
            }
          }
        ], function (err, result) {
          if (err) {
            reject(err)
          } else {
            var currentStock = result.reduce(function (prev, cur) {
              return prev + cur.sum
            }, 0)
            var stockData = {
              initialStock: req.body.initial_stock,
              currentStock: currentStock,
              notes: req.body.notes,
              updatedDate: Date.now(),
              updatedBy: user_data._id,
            }
            Stock.findOneAndUpdate({
              _id: req.body.stock_id
            }, stockData, function (error, stock) {
              if (error || stock == null) {
                console.log(error)
                req.flash('info', 'Update stock failed')
                req.flash('info_type', 'danger')
                res.redirect('/sm/stock')
              } else {
                req.flash('info', 'Update stock success')
                req.flash('info_type', 'success')
                res.redirect('/sm/stock')
              }
            })
          }
        })
      }
    })
  } else {
    req.flash('info', 'All fields required')
    req.flash('info_type', 'danger')
    res.redirect('/sm/stock')
  }
}


//stock transaction
exports.stock_transaction = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    function (callback) {
      StockTransaction.find({
          hoId: user_data.headOffice
        }).populate({
          path: 'stock createdBy updatedBy',
          populate: {
            path: 'item',
            populate: {
              path: 'unit itemCategory'
            }
          }
        })
        .exec(function (err, trans) {
          if (err) return callback(err)
          locals.trans = trans
          callback()
        })
    },
    function (callback) {
      Item.find({
          hoId: user_data.headOffice
        }).populate({
          path: 'itemCategory unit stock',
          populate: {
            path: 'highestUnit'
          }
        })
        .exec(function (err, items) {
          if (err) return callback(err)
          locals.items = items
          callback()
        })
    },
    function (callback) {
      Unit.find({
          type: "lowest"
        }).populate('highestUnit')
        .exec(function (err, units) {
          if (err) return callback(err)
          locals.units = units
          callback()
        })
    }
  ], function (err) {
    if (err) return next(err)
    res.render('stock_transaction', {
      title: appName + " | " + "Stock History",
      user_data: user_data,
      trans: locals.trans,
      items: locals.items,
      units: locals.units,
      messages: messages,
      messages_type: messages_type
    })
  })
}

//receive items delivery order manual
exports.delivery_order_manual = function (req, res) {
  var code = req.body.do_code
  DeliveryOrder.findOne({
    doCode: code
  }, function (err, result) {
    if (err || result == null) {
      console.log(err)
      req.flash('info', 'Invalid Code')
      req.flash('info_type', 'danger')
      res.redirect('/sm/delivery_order')
    } else {
      res.redirect('/sm/receive_item/' + code)
    }
  })
}

//GET delivery_order_print
exports.delivery_order_print = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var locals = {}
  var messages = {}
  var messages_type = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
    messages_type = res.locals.info_type
  }
  async.parallel([
    function (callback) {
      DeliveryOrder.findOne({
          _id: req.params.id
        }).sort({
          createdDate: -1
        }).populate({
          path: 'headOffice supplier deliveryItem store recipientUser storeWarehouse storeCasheer createdBy updatedBy',
          populate: {
            path: 'item stockRef unit brand',
            populate: {
              path: 'itemCategory'
            }
          }
        }).sort({
          createdDate: -1
        })
        .exec(function (err, DOData) {
          if (err) return callback(err)
          locals.DOData = DOData
          // console.log(DOData[0].deliveryItem[0])
          callback()
        })
    },
  ], function (err) {
    if (err) return next(err)
    DeliveryOrder.find({
        headOffice: user_data.headOffice,
        isDifference: true
      }).populate({
        path: 'headOffice supplier deliveryItem store storeWarehouse storeCasheer createdBy updatedBy',
        populate: {
          path: 'item stockRef unit brand',
          populate: {
            path: 'itemCategory'
          }
        }
      }).sort({
        createdDate: -1
      })
      .exec(function (err, DOData) {
        if (err || DOData == null) {
          return console.log(err)
        } else {
          locals.DOData_alert = DOData
          res.render('delivery_order_print', {
            title: appName + " | " + "Delivery Order Print Details",
            user_data: user_data,
            lowest_unit: locals.lowest_unit,
            DOData: locals.DOData,
            DOData_alert: locals.DOData_alert,
            messages: messages,
            messages_type: messages_type,
          })
        }
      })
  })
}

//Spoil Function
exports.spoil_stock_process = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  Stock.findOne({
    _id: req.body.stock_id
  }, function (err, stock) {
    if (err || stock == null) {
      var err = {
        status: 400,
        message: err
      }
      return res.send(JSON.stringify(err))
    } else {
      if (req.body.quantity <= stock.currentStock) {
        var StockData = {
          "$inc": {
            currentStock: parseFloat(req.body.quantity) * -1
          }
        }
        Stock.findByIdAndUpdate(req.body.stock_id, StockData, function (err, stock) {
          if (err || stock == null) {
            console.log(err)
            req.flash('info', 'Failed to spoil item stock')
            req.flash('info_type', 'danger')
            res.redirect('/sm/stock')
          } else {
            var transData = {
              hoId: user_data.headOffice,
              stock: stock._id,
              quantity: parseFloat(req.body.quantity) * -1,
              type: 'out',
              description: 'Spoil Item : ' + req.body.reason,
              createdDate: Date.now(),
              createdBy: user_data._id,
            }
            StockTransaction.create(transData, function (error, trans) {
              if (error || trans == null) {
                req.flash('info', 'Failed to spoil item stock')
                req.flash('info_type', 'danger')
                res.redirect('/sm/stock')
              } else {
                req.flash('info', 'Spoile item successful')
                req.flash('info_type', 'success')
                res.redirect('/sm/stock')
              }
            })
          }
        })
      } else {
        req.flash('info', 'Spoil quantity can\'t exceed the current stock')
        req.flash('info_type', 'danger')
        res.redirect('/sm/stock')
      }
    }
  })
}

function random_password(){
  var pass = (Math.random().toString(36).substr(2)).toUpperCase()
  pass = crypto.createHash('md5').update(pass).digest('hex')
  return pass
}

function decodeBase64Image(dataString) 
        {
          var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
          var response = {};

          if (matches.length !== 3) 
          {
            return new Error('Invalid input string');
          }

          response.type = matches[1];
          response.data = new Buffer(matches[2], 'base64');

          return response;
        }