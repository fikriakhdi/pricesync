const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let Brand_schema = new Schema({
    code: String,
    name : String,
    headOffice:
    {
      type: Schema.Types.ObjectId,
      ref: 'headoffice'
    },
    stores: [{
      type: Schema.Types.ObjectId,
      ref: 'store'
    }],
    imageUrlBackground : String,
    imageUrlLogo : String,
    createdDate: Date,
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    },
    updatedDate: Date,
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    }
});

// Export the model
module.exports = mongoose.model('brand', Brand_schema );
