const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let Customer_schema = new Schema({
    companyId : String,
    brandId : String,
    unitKerja : String,
    custId : String,
    headOffice: {
      type: Schema.Types.ObjectId,
      ref: 'headoffice'
    },
    brand:
    {
      type: Schema.Types.ObjectId,
      ref: 'brand'
    },
    store:
    {
      type: Schema.Types.ObjectId,
      ref: 'store'
    },
    custName : String,
    custAddr1 : String,
    custAddr2: String,
    custTelp1: String,
    custTelp2: String,
    custEmail: String,
    custCounter: Number,
    custDisc: Number,
    custNotes: String,
    custRewards: String,
    custExp: Date,
    custBirth: Date,
    isMember: Boolean,
    custSpendStart: Date,
    custSpendValue: Number,
    custSpendLast: Date,
    custSpendValueLast: Number,
    createdDate: Date,
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    },
    updatedDate: Date,
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    }
});

// Export the model
module.exports = mongoose.model('customer', Customer_schema );
