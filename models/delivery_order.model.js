const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let DeliveryOrderSchema = new Schema({
  doCode: String,
  deliveryItem: [
    {
      type: Schema.Types.ObjectId,
      ref: 'doitem'
    }
  ],
  type: {
    type: String,
    enum: ['direct_store', 'indirect_store', 'direct_ho'],
  },
  store: {
    type: Schema.Types.ObjectId,
    ref: 'store'
  },
  headOffice: {
    type: Schema.Types.ObjectId,
    ref: 'headoffice'
  },
  supplier: {
    type: Schema.Types.ObjectId,
    ref: 'supplier'
  },
  recipientUser:  {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  paidStatus: {
    type: String,
    enum: ['unpaid', 'paid'],
  },
  isDifference: Boolean,
  imageUrl: String,
  deliveryDate: Date,
  receiptDate: Date,
  receiptTime: String,
  driverName: String,
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
})

// Export the model
module.exports = mongoose.model('deliveryorder', DeliveryOrderSchema );
