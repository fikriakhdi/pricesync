const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let DiscountScheme = new Schema({
    name : String,
    brand: {
        type: Schema.Types.ObjectId,
        ref: 'brand'
      },
    valueType: {
        type: String,
        enum: ['nominal', 'percent'],
      },
    value:Number,
    maxValue:Number,
    product: {
        type: Schema.Types.ObjectId,
        ref: 'product'
      },
    productcategory: {
        type: Schema.Types.ObjectId,
        ref: 'productcategory'
      },
    productgroup: {
        type: Schema.Types.ObjectId,
        ref: 'productgroup'
      },
    payment: {
      type: Schema.Types.ObjectId,
      ref: 'payment'
    },
    createdDate: Date,
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    },
    updatedDate: Date,
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    }
});

// Export the model
module.exports = mongoose.model('discount', DiscountScheme );
