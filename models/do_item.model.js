const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let DOItemSchema = new Schema({
  item: {
    type: Schema.Types.ObjectId,
    ref: 'item'
  },
  stock: {
    type: Schema.Types.ObjectId,
    ref: 'stock'
  },
  image: String,
  quantitySend: Number,
  quantityReceive: Number,
  quantityAdjustment: Number,
  quantityReturn: Number,
  notesReturn: String,
  adjustmentType: String,
  totalWeight: Number,
  unitPrice: Number,
  totalPrice: Number,
  unit: {
    type: Schema.Types.ObjectId,
    ref: 'unit'
  },
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
})

// Export the model
module.exports = mongoose.model('doitem', DOItemSchema );
