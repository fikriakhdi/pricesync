const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let HeadOfficeSchema = new Schema({
    name : String,
    companyId : String,
    token : String,
    brands: [
        {
          type: Schema.Types.ObjectId,
          ref: 'brand'
        }
    ],
    createdDate: Date,
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    },
    updatedDate: Date,
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    }
});

// Export the model
module.exports = mongoose.model('headoffice', HeadOfficeSchema );
