const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let ItemSchema = new Schema({
  name:  String,
  itemCategory: {
    type: Schema.Types.ObjectId,
    ref: 'itemcategory'
  },
  itemCodeNumber: String,
  hoId: String,
  unit: {
    type: Schema.Types.ObjectId,
    ref: 'unit'
  },
  // stockTransaction: [
  //   {
  //     type: Schema.Types.ObjectId,
  //     ref: 'stocktransaction'
  //   }
  // ],
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
})

// Export the model
module.exports = mongoose.model('item', ItemSchema );
