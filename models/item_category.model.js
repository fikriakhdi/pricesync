const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let ItemCategorySchema = new Schema({
  name:  String,
  prefix: {
    type:String
  },
  lastIndex: Number,
  hoId: String,
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
})

// Export the model
module.exports = mongoose.model('itemcategory', ItemCategorySchema );
