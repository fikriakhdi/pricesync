const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let PaymentScheme = new Schema({
    name : String,
    logo : String,
    brand: {
      type: Schema.Types.ObjectId,
      ref: 'brand'
    },
    type: {
        type: String,
        enum: ['debit', 'credit', 'digital', 'cash'],
      },
    createdDate: Date,
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    },
    updatedDate: Date,
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    }
});

// Export the model
module.exports = mongoose.model('payment', PaymentScheme );
