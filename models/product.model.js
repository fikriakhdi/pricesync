const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let ProductSchema = new Schema({
  productCode: {type: String, unique:true},   //prod_no
  name: String,                               //prod_desc
  price: Number,                              //prod_price
  printerId: String,                          //kd_printer
  productCategoryCode: String,                //prod_group
  imageProduct : String,
  description: String,                  
  PLUId: String,
  productGroup:{
    type: Schema.Types.ObjectId,
    ref: 'productgroup'
  },
  productCategory:{
    type: Schema.Types.ObjectId,
    ref: 'productcategory'
  },
  validStartDate: Date,
  brand:{
    type: Schema.Types.ObjectId,
    ref: 'brand'
  },
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
})

// Export the model
module.exports = mongoose.model('product', ProductSchema );
