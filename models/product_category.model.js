const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let ProductCategorySchema = new Schema({
    code: {type: String, unique:true},    //prod_group
    name: String,                         //group_desc  
    productGroupCode: String,             //group_jenis
		groupDisc: String,                    //group_disc
		groupParent: String,                  //group_parent
		groupList: String,                    //group_list
    groupMultidisc: String,               //group_multidisc       
    productGroup: { 
      type: Schema.Types.ObjectId,
      ref: 'productgroup'
    },
    brand: {
      type: Schema.Types.ObjectId,
      ref: 'brand'
    },    
    createdDate: Date,
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    },
    updatedDate: Date,
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    }
});

// Export the model
module.exports = mongoose.model('productcategory', ProductCategorySchema );
