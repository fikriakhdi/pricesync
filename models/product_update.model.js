const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let ProductUpdateSchema = new Schema({
  hoId: String,
  brand:{
    type: Schema.Types.ObjectId,
    ref: 'brand'
  },
  updateToPOSDate: Date,
  validDate: Date,
  productChanges: [{
    product: {
      type: Schema.Types.ObjectId,
      ref: 'product'
    },
    name:  String,
    description: String,    
    price: Number
  }],
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
})

// Export the model
module.exports = mongoose.model('productupdate', ProductUpdateSchema );
