const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let Revenue_schema = new Schema({
    date : Date,
    shift : Number,
    cash : Number,
    card: Number,
    debit: Number,
    other: Number,
    tax: Number,
    discount: Number,
    tax: Number,
    total : Number,
    brand:{
          type: Schema.Types.ObjectId,
          ref: 'brand'
    },
    store:{
          type: Schema.Types.ObjectId,
          ref: 'store'
    },
    createdDate: Date,
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    },
    updatedDate: Date,
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    }
});

// Export the model
module.exports = mongoose.model('revenue', Revenue_schema );
