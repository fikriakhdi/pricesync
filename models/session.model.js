const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

const User = require('./user.model')

let SessionSchema = new Schema({
    token: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
      },
    createdDate: Date,
})

SessionSchema.statics.validate = (token, rolePermitted, callback) => {
    Session.findOne({token: token}).populate("user").exec(function (err, session) {
        if (err || !session ) {
            return callback(null, {error:true,message:err})
        } else {
            var user = session.user
            if (user.role != rolePermitted) {
                return callback(null, {error:true,message:'Insufficient authority'})
            } else{
                return callback(null, user)
            }
        }
    })
};


// Export the model
var Session = mongoose.model('session', SessionSchema )
module.exports = Session
