const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let StockSchema = new Schema({
  item: {
    type: Schema.Types.ObjectId,
    ref: 'item'
  },
  ownerType: {
    type: String,
    enum: ['store', 'ho'],
  },
  headOffice: {
    type: Schema.Types.ObjectId,
    ref: 'headoffice'
  },
  store: {
    type: Schema.Types.ObjectId,
    ref: 'store'
  },
  initialStock : Number, 
  currentStock: Number,
  notes: String,
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
})

// Export the model
module.exports = mongoose.model('stock', StockSchema );
