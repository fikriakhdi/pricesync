const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let StockTransactionSchema = new Schema({
  quantity: Number,
  ROP: Number,
  hoId: String,
  doItemId:  {
    type: Schema.Types.ObjectId,
    ref: 'doitem'
  },
  description: String,
  type: {
    type: String,
    enum: ['in', 'out', 'initial'],
  },
  stock: {
    type: Schema.Types.ObjectId,
    ref: 'stock'
  },
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
})

// Export the model
module.exports = mongoose.model('stocktransaction', StockTransactionSchema );
