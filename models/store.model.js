const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let StoreSchema = new Schema({
  name : String,
  brand: {
      type: Schema.Types.ObjectId,
      ref: 'brand'
  },
  unitKerja: String, 
  outerJakarta: String,
  token: String,
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
});

// Export the model
module.exports = mongoose.model('store', StoreSchema );
