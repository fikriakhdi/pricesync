const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let SupplierSchema = new Schema({
    name : String,
    headOffice: {
      type: Schema.Types.ObjectId,
      ref: 'headoffice'
    },
    createdDate: Date,
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    },
    updatedDate: Date,
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    }
});

// Export the model
module.exports = mongoose.model('supplier', SupplierSchema );
