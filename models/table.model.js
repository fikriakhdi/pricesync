const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let TableSchema = new Schema({
    number : String,
    store: {
      type: Schema.Types.ObjectId,
      ref: 'store'
    },
    brand: {
      type: Schema.Types.ObjectId,
      ref: 'brand'
    },
    type: {
      type: String,
      enum: ['dinein', 'takeaway'],
    },
    status: {
        type: String,
        enum: ['booked', 'available', 'unavailable'],
    },
    createdDate: Date,
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    },
    updatedDate: Date,
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    }
});

// Export the model
module.exports = mongoose.model('table', TableSchema );
