const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let UnitSchema = new Schema({
  name: {
    type: String
  },
  type: {
    type: String,
    enum: ['lowest', 'highest'],
  },
  label: String,
  lowestUnit: {
    type: Schema.Types.ObjectId,
    ref: 'unit'
  },
  highestUnit: [
    {
      type: Schema.Types.ObjectId,
      ref: 'unit'
    },
  ],
  HTLConversion: Number,
  hoId: String,
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
})

// Export the model
module.exports = mongoose.model('unit', UnitSchema );
