const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let UserSchema = new Schema({
    name: String,
    email : {type: String, unique:true},
    password : String,
    role: String,
    expire: Date,
    headOffice: {
        type: Schema.Types.ObjectId,
        ref: 'headoffice'
    },
    brand: {
        type: Schema.Types.ObjectId,
        ref: 'brand'
    },
    store: {
        type: Schema.Types.ObjectId,
        ref: 'store'
    },
    imageProfile: String,
    createdDate: Date,
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    },
    updatedDate: Date,
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    }
});

UserSchema.statics.setPassword = function(password){
	this.password = crypto.createHash('md5').update(password).digest('hex');
};

UserSchema.statics.authenticate = (email, password, callback) => {
    password = crypto.createHash('md5').update(password).digest('hex');
    User.findOne({'email': email, 'password':password}).populate('brand store headOffice').exec( function (err, user) {
        if (err || !user ) {
            return callback(null, err)
        } else {
            return callback(null, user)
        }
    })
};

// Export the model
var User = mongoose.model('user', UserSchema)
module.exports = User
