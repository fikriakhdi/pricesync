// app.js
const express = require('express');
const bodyParser = require('body-parser');
const router = require('./routes/router');
const path = require('path');
const session = require('express-session');
var flash = require('connect-flash');
var cookieParser = require('cookie-parser');
var hbs = require('hbs');
var currencyFormatter = require('currency-formatter');
global.appName = "ISIPos Online";
global.billingAPI = "https://billing.isipos.online/";
global.secretCode = "054141060ddc85ce75febe112d4fe0ec";
//Helper
hbs.registerHelper('dateFormat', require('handlebars-dateformat'));

hbs.registerHelper('formatCurrency', function(value) {
    if(typeof value == 'undefined') return '0'
    var val = "" + value;
    return val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
});

//helper
hbs.registerHelper("inc", function(value, options){
    return parseInt(value) + 1;
});

hbs.registerHelper('ifEquals', function(arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
});

hbs.registerHelper('findDiff', function(obj) {
    var found = 0
    if(obj){
    obj.map((f,i)=>{
        var substract = Math.abs(f.quantitySend-f.quantityReceive)
        if(substract>0) found++
    })
    return (found >= 0) ? 'block' : 'none';
} else return 'Kosong';
});

hbs.registerHelper('notEquals', function(arg1, arg2, options) {
    return (arg1 != arg2) ? options.fn(this) : options.inverse(this);
});

hbs.registerHelper('ifDoEquals', function(arg1, arg2) {
    return (arg1 == arg2) ? true : false;
});

hbs.registerHelper('or', function(arg1, arg2, options) {
    return (arg1 || arg2) ? options.fn(this) : options.inverse(this);
});

hbs.registerHelper('convertRole', function(role) {
    if(role=="bm") return "Brand Manager"
    if(role=="sm_supervisor") return "Store Manager (Supervisor)"
    if(role=="sm_warehouse") return "Store Manager (Warehouse)"
    if(role=="admin") return "Admin"
    if(role=="ho") return "Head Office"
    if(role=="ho_staff") return "HO Staff"
    if(role=="cashier") return "Cashier"
    if(role=="waiter") return "Waiter"
});

hbs.registerHelper('convertDOType', function(type) {
    if(type=="direct_store") return "Direct Store"
    if(type=="indirect_store") return "Indirect Store"
    if(type=="direct_ho") return "Direct HO"
});

hbs.registerHelper('doCode', function(id) {
    return ("#DO"+id.toString().slice(-5)).toUpperCase()
});

hbs.registerHelper('minValue', function(arg1, arg2) {
    var val1 = (arg1?arg1:0)
    var val2 = (arg2?arg2:0)
    return val1-val2
});

hbs.registerHelper('substract', function(val1, val2) {
    return Math.abs(val1-val2)
});

hbs.registerHelper('moneyRegister', function(val){
  var result = currencyFormatter.format(val, { code: 'IDR' });
  return result
})

hbs.registerHelper('get_length', function (obj) {
    return obj.length;
});   

// initialize our express app
const app = express();

// Set up mongoose connection
const mongoose = require('mongoose');
let dev_db_url = 'mongodb://localhost/pricesync';
// let dev_db_url = 'mongodb://pricesync:guagokil805@ds151453.mlab.com:51453/pricesync';
const mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise; 
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//App Use
app.use(flash());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(session({
  cookieName: 'session',
  secret: "04cbe3a7258b1306dda99bdd5de3a574",
  duration: 86400000,
  activeDuration: 86400000,
  httpOnly: true,
  secure: true,
  ephemeral: true
}));
//routing
app.use('/', router);

app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views/partials');
app.use(express.static(__dirname));
app.use(cookieParser());

let port = 8080;
// app.get('*', function(req, res){
//   res.status(404).send('what???');
// });


app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});
