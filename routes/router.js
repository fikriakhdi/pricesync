const express = require('express')
const router = express.Router()
var multer  = require('multer')

// Require the controllers
const admin_controller = require('../controllers/admin.controller')
const ho_controller = require('../controllers/ho.controller')
const ho_staff_controller = require('../controllers/ho_staff.controller')
const bm_controller = require('../controllers/bm.controller')
const sm_controller = require('../controllers/sm.controller')
const common_controller = require('../controllers/common.controller')
const api_controller = require('../controllers/api.controller')
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './assets/image')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '.' + file.mimetype.split('/')[1])
  }
})
var upload = multer({ storage: storage })

// // a simple test url to check that all of our files are communicating correctly.
// router.get('/read/:email', common_controller.user_details);
// router.get('/read_brands', common_controller.read_brands);
// router.get('/read_stores', common_controller.read_stores);
// router.put('/update/:email', common_controller.user_update);
// router.post('/create', common_controller.user_create);
// router.post('/brand_create', common_controller.brand_create);
// router.post('/store_create', common_controller.store_create);
// router.get('/delete/:email', common_controller.user_delete);



//// ------------- COMMON VIEW & API --------------- ////
router.get('/', common_controller.main_page);
router.get('/signin/', common_controller.signin);
// router.get('/.well-known/pki-validation/4F1A4EE875E9C20337E6B45149778EAF.txt', common_controller.file);
router.get('/signin/:redirect', common_controller.signin);
router.post('/signin', common_controller.signin_process);
router.get('/forgot_password', common_controller.forgot_password);
router.post('/forgot_password', common_controller.forgot_password_process);
router.get('/forgot_password/reset_password/:email/:rstcode', common_controller.reset_password);
router.post('/forgot_password/reset_password/', common_controller.reset_password_process);
router.get('/signout', common_controller.signout);

router.post('/api/login', api_controller.login_process);
router.post('/api/login_pos', api_controller.login_process);
router.post('/api/forgot_password', api_controller.forgot_password_process);
router.post('/api/checkToken', api_controller.checkToken);
router.post('/api/edit_password', api_controller.edit_password);
router.post('/api/dashboard', api_controller.dashboard);
router.post('/api/my_profile', api_controller.my_profile);
router.post('/api/delivery_order', api_controller.delivery_order);
router.post('/api/create_do', api_controller.create_do);
router.post('/api/create_do_process', api_controller.create_do_process);
router.post('/api/restock', api_controller.restock);
router.post('/api/stock', api_controller.stock);
router.post('/api/create_product', api_controller.create_product);
router.get('/api/read_product', api_controller.read_product);
router.get('/api/read_brand', api_controller.read_brand);
router.get('/api/read_store', api_controller.read_store);
router.get('/api/read_user', api_controller.read_user);
router.post('/api/create_restock', api_controller.create_restock);
router.post('/api/create_restock_process', api_controller.create_restock_process);
router.post('/api/receive_do_manual', api_controller.receive_do_manual);
router.post('/api/receive_item_process', api_controller.receive_item_process)
router.post('/api/spoil_stock', api_controller.spoil_stock_process);
router.post('/api/return_doitem', api_controller.return_doitem_process);
router.post('/api/adjust_doitem', api_controller.adjust_doitem_process);
router.post('/api/notification', api_controller.notification);
router.post('/api/delivery_order_detail', api_controller.delivery_order_detail);
router.post('/api/resolve_difference', api_controller.resolve_difference);
router.post('/api/change_paid_status', api_controller.change_paid_status);
router.get('/api/read_product/:brandId', api_controller.read_product);
router.get('/api/read_product_category/:brandId', api_controller.read_product_category);
router.get('/api/read_payment/:brandId', api_controller.read_payment);
router.get('/api/read_table/:brandId/:storeId', api_controller.read_table);
router.get('/api/read_product_group/:brandId', api_controller.read_product_group);
router.get('/api/read_discount/:brandId', api_controller.read_discount);
router.get('/api/checkStore/:storeId', api_controller.checkStore);
router.get('/api/user', api_controller.user);
router.get('/api/brand', api_controller.brand);
router.get('/api/product/:brand', api_controller.product);
router.get('/api/product_category/:brand', api_controller.product_category);
router.get('/api/product_group/:brand', api_controller.product_group);
router.get('/api/store/:brand', api_controller.store);

router.post('/api/create_revenue', api_controller.create_revenue);
router.get('/api/product_update_checker/:brandcode/:unitkerja/:updatedate', api_controller.product_update_checker);
router.post('/api/push_product_list', api_controller.push_product_list);
router.post('/api/push_product_category', api_controller.push_product_category);


//// ------------- HEAD OFFICE VIEW & API --------------- ////
router.get('/ho/test', ho_controller.test_agregate);
router.get('/ho', ho_controller.dashboard);
router.get('/ho/profile', ho_controller.profile);
router.post('/ho/profile', ho_controller.change_password_process)

// router.post('/ho/add_unit', ho_controller.add_unit_process);
router.get('/ho/unit', ho_controller.unit);
router.post('/ho/add_unit', ho_controller.add_unit_process);
router.get('/ho/unit/:id', ho_controller.single_unit)
router.post('/ho/edit_unit', ho_controller.update_unit_process);
router.get('/ho/delete_unit/:id', ho_controller.delete_unit_process);

router.get('/ho/item_category', ho_controller.item_category);
router.post('/ho/create_item_category', ho_controller.create_item_category_process);
router.get('/ho/item_category/:categoryId', ho_controller.single_item_category)
router.post('/ho/edit_item_category', ho_controller.update_item_category_process);
router.get('/ho/delete_item_category/:categoryId', ho_controller.delete_item_category_process);

router.get('/ho/item', ho_controller.item);
router.post('/ho/create_item', ho_controller.create_item_process);
router.get('/ho/item/:itemId', ho_controller.single_item)
router.post('/ho/edit_item', ho_controller.update_item_process);
router.get('/ho/delete_item/:itemId', ho_controller.delete_item_process);

router.get('/ho/delivery_order', ho_controller.delivery_order);
router.get('/ho/create_delivery_order/:dotype', ho_controller.create_delivery_order);
router.post('/ho/create_delivery_order', upload.single('photo'), ho_controller.create_delivery_order_process);
router.get('/ho/invoice_delivery_order/:DOId', ho_controller.invoice_delivery_order);
router.get('/ho/delete_delivery_order/:DOId/:DOtype', ho_controller.delete_do_process);
router.post('/ho/adjust_doitem', ho_controller.adjust_doitem_process);

router.get('/ho/supplier', ho_controller.supplier);
router.post('/ho/create_supplier', ho_controller.create_supplier_process);
router.get('/ho/supplier/:supplierId', ho_controller.single_supplier)
router.post('/ho/edit_supplier/', ho_controller.update_supplier_process);
router.get('/ho/delete_supplier/:supplierId', ho_controller.delete_supplier_process);

router.get('/ho/stock', ho_controller.stock);
router.get('/ho/active_stock', ho_controller.active_stock);
router.get('/ho/receiving_goods', ho_controller.receiving_goods);
router.get('/ho/stock/:stockId', ho_controller.single_stock);
router.post('/ho/create_stock', ho_controller.create_stock_process);
router.post('/ho/edit_stock', ho_controller.update_stock_process);
router.get('/ho/delete_stock/:stockId', ho_controller.delete_stock_process);
router.get('/ho/stock_transaction', ho_controller.stock_transaction);

router.post('/ho/return_doitem', ho_controller.return_doitem_process);

router.get('/ho/customer', ho_controller.customer);
router.post('/ho/customer', ho_controller.create_customer_process);
router.get('/ho/delete_customer/:customerId', ho_controller.delete_customer_process);
router.post('/ho/update_customer', ho_controller.update_customer_process);
router.get('/ho/customer/:customerId', ho_controller.get_single_customer);

router.get('/ho/user', ho_controller.user);
router.post('/ho/create_user/user', ho_controller.create_user_process);
router.post('/ho/update_user/user', ho_controller.edit_user_process);
router.get('/ho/user/:userId', ho_controller.single_user);
router.get('/ho/delete_user/:userId/:redirect', ho_controller.delete_user_process);
router.get('/ho/delete_user/:userId', ho_controller.delete_user_process);

router.get('/ho/get_store_list/:brandId', ho_controller.get_store_list);
router.get('/ho/delivery_order_detail/:doId', ho_controller.delivery_order_detail);
router.get('/ho/notification', ho_controller.notification);
router.get('/ho/resolve_difference/:doId', ho_controller.resolve_difference);
router.get('/ho/restock_report/:type', ho_controller.restock_report);
router.get('/ho/restock_report/:type/:id/:dateFrom/:dateTo', ho_controller.restock_report_single);
router.get('/ho/restock_report_print/:type/:id/:dateFrom/:dateTo', ho_controller.restock_report_print);
router.get('/ho/do_report/:type', ho_controller.do_report);
router.get('/ho/do_report/:type/:id', ho_controller.do_report_single);
router.get('/ho/delivery_order_print/:id', ho_controller.delivery_order_print);
router.get('/ho/delivery_order_print/:showprice/:id', ho_controller.delivery_order_print);
router.post('/ho/spoil_stock/', ho_controller.spoil_stock_process);
router.post('/ho/changePaidStatus/:id', ho_controller.changePaidStatus);


//// ------------- CUSTOM - GANG KELINCI VIEW & API --------------- ////
router.get('/ho_staff', ho_staff_controller.dashboard);
router.get('/ho_staff/profile', ho_staff_controller.profile);
router.post('/ho_staff/profile', ho_staff_controller.change_password_process)

router.get('/ho_staff/unit', ho_staff_controller.unit);
router.post('/ho_staff/add_unit', ho_staff_controller.add_unit_process);
router.get('/ho_staff/unit/:id', ho_staff_controller.single_unit)
router.post('/ho_staff/edit_unit', ho_staff_controller.update_unit_process);
router.get('/ho_staff/delete_unit/:id', ho_staff_controller.delete_unit_process);

router.get('/ho_staff/item_category', ho_staff_controller.item_category);
router.post('/ho_staff/create_item_category', ho_staff_controller.create_item_category_process);
router.get('/ho_staff/item_category/:categoryId', ho_staff_controller.single_item_category)
router.post('/ho_staff/edit_item_category', ho_staff_controller.update_item_category_process);
router.get('/ho_staff/delete_item_category/:categoryId', ho_staff_controller.delete_item_category_process);

router.get('/ho_staff/item', ho_staff_controller.item);
router.post('/ho_staff/create_item', ho_staff_controller.create_item_process);
router.get('/ho_staff/item/:itemId', ho_staff_controller.single_item)
router.post('/ho_staff/edit_item', ho_staff_controller.update_item_process);
router.get('/ho_staff/delete_item/:itemId', ho_staff_controller.delete_item_process);

router.get('/ho_staff/delivery_order', ho_staff_controller.delivery_order);
router.get('/ho_staff/create_delivery_order/:dotype', ho_staff_controller.create_delivery_order);
router.post('/ho_staff/create_delivery_order', upload.single('photo'), ho_staff_controller.create_delivery_order_process);
router.get('/ho_staff/invoice_delivery_order/:DOId', ho_staff_controller.invoice_delivery_order);
router.get('/ho_staff/delete_delivery_order/:DOId/:DOtype', ho_staff_controller.delete_do_process);
router.post('/ho_staff/adjust_doitem', ho_staff_controller.adjust_doitem_process);

router.get('/ho_staff/supplier', ho_staff_controller.supplier);
router.post('/ho_staff/create_supplier', ho_staff_controller.create_supplier_process);
router.get('/ho_staff/supplier/:supplierId', ho_staff_controller.single_supplier)
router.post('/ho_staff/edit_supplier/', ho_staff_controller.update_supplier_process);
router.get('/ho_staff/delete_supplier/:supplierId', ho_staff_controller.delete_supplier_process);

router.get('/ho_staff/stock', ho_staff_controller.stock);
router.get('/ho_staff/active_stock', ho_staff_controller.active_stock);
router.get('/ho_staff/receiving_goods', ho_staff_controller.receiving_goods);
router.get('/ho_staff/stock/:stockId', ho_staff_controller.single_stock);
router.post('/ho_staff/create_stock', ho_staff_controller.create_stock_process);
router.post('/ho_staff/edit_stock', ho_staff_controller.update_stock_process);
router.get('/ho_staff/delete_stock/:stockId', ho_staff_controller.delete_stock_process);
router.get('/ho_staff/stock_transaction', ho_staff_controller.stock_transaction);

router.post('/ho_staff/return_doitem', ho_staff_controller.return_doitem_process);

router.get('/ho_staff/customer', ho_staff_controller.customer);
router.post('/ho_staff/customer', ho_staff_controller.create_customer_process);
router.get('/ho_staff/delete_customer/:customerId', ho_staff_controller.delete_customer_process);
router.post('/ho_staff/update_customer', ho_staff_controller.update_customer_process);
router.get('/ho_staff/customer/:customerId', ho_staff_controller.get_single_customer);

router.get('/ho_staff/user', ho_staff_controller.user);
router.post('/ho_staff/create_user/user', ho_staff_controller.create_user_process);
router.post('/ho_staff/update_user/user', ho_staff_controller.edit_user_process);
router.get('/ho_staff/user/:userId', ho_staff_controller.single_user);
router.get('/ho_staff/delete_user/:userId/:redirect', ho_staff_controller.delete_user_process);
router.get('/ho_staff/delete_user/:userId', ho_staff_controller.delete_user_process);

router.get('/ho_staff/get_store_list/:brandId', ho_staff_controller.get_store_list);
router.get('/ho_staff/delivery_order_detail/:doId', ho_staff_controller.delivery_order_detail);
router.get('/ho_staff/notification', ho_staff_controller.notification);
router.get('/ho_staff/resolve_difference/:doId', ho_staff_controller.resolve_difference);
router.get('/ho_staff/restock_report/:type', ho_staff_controller.restock_report);
router.get('/ho_staff/restock_report/:type/:id/:dateFrom/:dateTo', ho_staff_controller.restock_report_single);
router.get('/ho_staff/restock_report_print/:type/:id/:dateFrom/:dateTo', ho_staff_controller.restock_report_print);
router.get('/ho_staff/do_report/:type', ho_staff_controller.do_report);
router.get('/ho_staff/do_report/:type/:id', ho_staff_controller.do_report_single);
router.get('/ho_staff/delivery_order_print/:id', ho_staff_controller.delivery_order_print);
router.get('/ho_staff/delivery_order_print/:showprice/:id', ho_staff_controller.delivery_order_print);
router.post('/ho_staff/spoil_stock/', ho_staff_controller.spoil_stock_process);
router.post('/ho_staff/changePaidStatus/:id', ho_staff_controller.changePaidStatus);



//// ------------- BRAND MANAGER VIEW & API --------------- ////
router.get('/bm', bm_controller.dashboard);
router.get('/bm/profile', bm_controller.profile);
router.post('/bm/profile', bm_controller.change_password_process);
router.get('/bm/setting', bm_controller.setting);
router.post('/bm/update_logo',upload.single('image'), bm_controller.update_logo_process);
router.post('/bm/update_background',upload.single('image'), bm_controller.update_background_process);
// PRODUCT
router.get('/bm/product', bm_controller.product);
router.post('/bm/create_product',upload.single('image'), bm_controller.create_product_process);
router.post('/bm/edit_product',upload.single('image'), bm_controller.update_product_process);
router.get('/bm/product/:productId', bm_controller.single_product);
router.get('/bm/delete_product/:productId', bm_controller.delete_product_process);
// PRODUCT UPDATE
router.get('/bm/product_update', bm_controller.product_update);
router.get('/bm/create_product_update', bm_controller.create_product_update);
router.post('/bm/create_product_update', bm_controller.create_product_update_process);
router.get('/bm/edit_product_update/:id', bm_controller.update_product_update);
router.post('/bm/edit_product_update/:id', bm_controller.update_product_update_process);
router.get('/bm/get_single_product_update/:id', bm_controller.single_product_update);
router.get('/bm/delete_product_update/:id', bm_controller.delete_product_update_process);
// NEWS
router.get('/bm/news', bm_controller.news);
router.post('/bm/create_news', bm_controller.create_news_process);
router.get('/bm/delete_news/:newsId', bm_controller.delete_news_process);
router.get('/bm/news_list', bm_controller.news_list);
// TABLE
router.get('/bm/table', bm_controller.table);
router.post('/bm/table', bm_controller.table_create_process);
router.post('/bm/table_edit', bm_controller.table_edit_process);
router.get('/bm/table_delete/:id', bm_controller.table_delete_process);
router.get('/bm/get_single_table/:id', bm_controller.get_single_tables);
// PRODUCT GROUP
router.get('/bm/product_group', bm_controller.product_group);
router.post('/bm/product_group', bm_controller.product_group_create_process);
router.post('/bm/product_group_edit', bm_controller.product_group_edit_process);
router.get('/bm/product_group_delete/:id', bm_controller.product_group_delete_process);
router.get('/bm/get_single_product_group/:id', bm_controller.get_single_product_groups);
// PRODUCT CATEGORY
router.get('/bm/product_category', bm_controller.product_category);
router.post('/bm/product_category', bm_controller.product_category_create_process);
router.post('/bm/product_category_edit', bm_controller.product_category_edit_process);
router.get('/bm/product_category_delete/:id', bm_controller.product_category_delete_process);
router.get('/bm/get_single_product_category/:id', bm_controller.get_single_product_categorys);
//  PAYMENT
router.get('/bm/payment', bm_controller.payment);
router.post('/bm/payment', upload.single('logo'), bm_controller.payment_create_process);
router.post('/bm/payment_edit',  upload.single('logo'), bm_controller.payment_edit_process);
router.get('/bm/payment_delete/:id', bm_controller.payment_delete_process);
router.get('/bm/get_single_payment/:id', bm_controller.get_single_payments);
//  DISCOUNT
router.get('/bm/discount', bm_controller.discount);
router.post('/bm/discount', bm_controller.discount_create_process);
router.post('/bm/discount_edit',bm_controller.discount_edit_process);
router.get('/bm/discount_delete/:id', bm_controller.discount_delete_process);
router.get('/bm/get_single_discount/:id', bm_controller.get_single_discounts);
//  REVENUE
router.get('/bm/revenue', bm_controller.revenue);


//// ------------- STORE MANAGER VIEW & API --------------- ////
router.get('/sm', sm_controller.dashboard);
router.get('/sm/revenue', sm_controller.revenue);
router.get('/sm/profile', sm_controller.profile);
router.post('/sm/profile', sm_controller.change_password_process);
router.get('/sm/revenue_list', sm_controller.revenue_list);
router.post('/sm/create_revenue', sm_controller.create_revenue_process);
router.get('/sm/delete_revenue/:revenueId', sm_controller.delete_revenue_process);
router.get('/sm/edit_product/:productId', sm_controller.get_single_product);
router.post('/sm/edit_product', sm_controller.update_product_process);
router.post('/sm/delete_product/:productId', sm_controller.delete_product_process);
router.get('/sm/receive_item/:doId', sm_controller.receive_item);
router.post('/sm/receive_item/:doId', upload.array('images', 999),sm_controller.receive_item_process)
router.post('/sm/receive_items', sm_controller.delivery_order_manual);
router.get('/sm/delivery_order', sm_controller.delivery_order);
router.post('/sm/return_doitem', sm_controller.return_doitem_process);
router.get('/sm/stock', sm_controller.stock);
router.get('/sm/stock/:stockId', sm_controller.single_stock);
router.post('/sm/create_stock', sm_controller.create_stock_process);
router.post('/sm/edit_stock', sm_controller.update_stock_process);
router.get('/sm/delete_stock/:stockId', sm_controller.delete_stock_process);
router.get('/sm/stock_transaction', sm_controller.stock_transaction);
router.get('/sm/delivery_order_print/:id', sm_controller.delivery_order_print);
router.get('/ho_staff/delivery_order_print/:showprice/:id', ho_staff_controller.delivery_order_print);
router.post('/sm/spoil_stock/', sm_controller.spoil_stock_process);

//API
router.post('/sm/create/revenue', sm_controller.create_revenue);
router.post('/sm/update/revenue', sm_controller.update_revenue);
router.get('/sm/revenue/:revenueId', sm_controller.single_revenue);
router.get('/sm/delete/revenue/:revenueId', sm_controller.delete_revenue);

//// ------------- ADMIN MANAGER VIEW & API --------------- ////
router.get('/admin', admin_controller.dashboard);
router.post('/admin/forgot_password', common_controller.forgot_password);
router.get('/admin/reset_password/:email/:rstcode', common_controller.reset_password);
router.post('/admin/reset_password/', common_controller.reset_password_process);
router.get('/admin/get_store_list/:brandId', admin_controller.get_store_list);
router.post('/admin/create_user',upload.single('photo'), admin_controller.create_user_process);
router.post('/admin/create_user/:redirect',upload.single('photo'), admin_controller.create_user_process);
router.post('/admin/update_user',upload.single('photo') , admin_controller.edit_user_process);
router.post('/admin/update_user/:redirect',upload.single('photo'), admin_controller.edit_user_process);
router.get('/admin/profile', admin_controller.profile);
router.post('/admin/profile', admin_controller.change_password_process);
router.post('/admin/update_profile', admin_controller.update_profile);
router.get('/admin/product', admin_controller.product)

router.get('/admin/user', admin_controller.user);
router.post('/admin/create/user', admin_controller.create_user);
router.post('/admin/update/user', admin_controller.update_user);
router.get('/admin/user/:userId', admin_controller.single_user);
router.get('/admin/delete_user/:userId/:redirect', admin_controller.delete_user_process);
router.get('/admin/delete_user/:userId', admin_controller.delete_user_process);

router.get('/admin/brand', admin_controller.brand);
router.post('/admin/create_brand', admin_controller.create_brand_process);
router.post('/admin/update_brand', admin_controller.update_brand_process);
router.get('/admin/delete_brand/:brandId', admin_controller.delete_brand_process);
router.post('/admin/create_store', admin_controller.create_store_process);
router.post('/admin/update_store', admin_controller.update_store_process);
router.get('/admin/delete_store/:storeId', admin_controller.delete_store_process);
router.post('/admin/create/brand', admin_controller.create_brand);
router.post('/admin/update/brand', admin_controller.update_brand);
router.get('/admin/brand/:brandId', admin_controller.single_brand);
router.get('/admin/delete/brand/:brandId', admin_controller.delete_brand);
router.get('/admin/store', admin_controller.store_list);
router.post('/admin/create/store', admin_controller.create_store);
router.post('/admin/update/store', admin_controller.update_store);
router.get('/admin/store/:storeId', admin_controller.single_store);
router.get('/admin/delete/store/:storeId', admin_controller.delete_store);
router.get('/admin/revenue', admin_controller.revenue);
router.post('/admin/create_revenue', admin_controller.create_revenue_process);
router.get('/admin/delete_revenue/:revenueId', admin_controller.delete_revenue_process);
router.get('/admin/product_list', admin_controller.product_list);
router.post('/admin/create_product', admin_controller.create_product_process);
router.post('/admin/update/product', admin_controller.update_product);
router.get('/admin/product/:productId', admin_controller.get_single_product);
router.get('/admin/delete_product/:productId', admin_controller.delete_product_process);
router.post('/admin/edit_product', admin_controller.update_product_process)
router.get('/admin/news', admin_controller.news);
router.post('/admin/create_news', admin_controller.create_news_process);
router.get('/admin/delete_news/:newsId', admin_controller.delete_news_process);
router.post('/admin/create/news', admin_controller.create_news);
router.post('/admin/update/news', admin_controller.update_news);
router.get('/admin/news/:newsId', admin_controller.single_news);
router.get('/admin/delete/news/:newsId', admin_controller.delete_news);
router.get('/admin/revenue_report', admin_controller.revenue_report);
router.get('/admin/ho', admin_controller.ho);
router.get('/admin/ho/:hoID', admin_controller.get_single_ho);
router.post('/admin/create_ho', admin_controller.create_ho);
router.post('/admin/edit_ho', admin_controller.edit_ho_process);
router.get('/admin/delete_ho/:hoID', admin_controller.delete_ho_process);
// router.get('/admin/user_report', admin_controller.user_report);
// router.get('/admin/revenue_report', admin_controller.revenue_report);

//TOKEN
router.post('/verifyurl', admin_controller.verifyURL);





module.exports = router;
